import React ,{ useEffect ,useState} from 'react';
import {
  FlatList,
  StyleSheet,
  Image,
  StatusBar,
  SafeAreaView,
  Text,
  View,
  ScrollView,
  Alert,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  NativeModules,
} from 'react-native';
const window = Dimensions.get('window');
import Toast from 'react-native-simple-toast';
import Geolocation from '@react-native-community/geolocation';
const GLOBAL = require('./Global');
import { EventRegister } from 'react-native-event-listeners'
import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert';

import Orientation from 'react-native-orientation';
const {Agora} = NativeModules;
const {
 FPS30,
 AudioProfileDefault,
 AudioScenarioDefault,
 Host,
 Audience,
 Adaptative
} = Agora
import {textStyle, viewStyle} from '../style/Style';
import LinearGradient from 'react-native-linear-gradient';
import {imageStyle} from '../style/Style';
import store from '../redux/store';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend,FetchHome,Favorite,GetProfileApi,Addq} from '../backend/Api';
const AudioBroadcast = ({showFloatButton, onPress, onPressImage, onScroll,navigation}) => {
    const [username, setUsername] = useState([]);
      const [add, setAdd] = useState([]);
      const [check, setAcheck] = useState([]);
      const [profile, setProfile] = useState({});
      const [detail, setDetail] = useState('');
        const [show, setShow] = useState(false);
  const colorList = [
    '#4A90E2',
    '#9013FE',
    '#F5A623',
    '#8B572A',
    '#417505',
    '#BD10E0',
  ];
  const flatListData = [

  ];


const home = () =>{
    Geolocation.getCurrentPosition(info => {
      var e = JSON.stringify({
        latitude:GLOBAL.gllat,longitude:GLOBAL.gllong
      })

      var k = {latitude:GLOBAL.gllat,longitude:GLOBAL.gllong,broadcast_type:"one_to_one_audio"}

      console.log(k)

      FetchHome({latitude:info.coords.latitude,longitude:info.coords.longitude,broadcast_type:"one_to_one_audio"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 //alert(data.data.length)
                // alert(JSON.stringify(data))
                 setUsername(data.data)
       var d = []
                 for (var i = 0 ; i <data.data.length ; i++){

                  if (i  == 3 || i  == 9 || i  == 15  || i ==21 || i ==27 || i == 33 || i == 39 || i == 45 || i == 51 || i == 57 || i == 63 || i == 69 ||i == 75 || i == 81){
                     d.push(data.data[i].id)
                   }
                  //  d.push(data.data[i].id)

                   // if (i % 3 == 0){
                   //   if (i == 0){
                   //
                   //   }else {
                   //      d.push(data.data[i].id)
                   //   }


                  // }
                  }

                 setAcheck(d)

    }
                 //setAcheck
                 //alert(JSON.stringify(data.data[0]))
                //setUsername(data.data)






            else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })

}

const profile3 = () =>{
  Addq({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             //alert(JSON.stringify(data))
             setAdd(data.data)







           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}

const profile2 = () =>{
  GetProfileApi({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             //alert(JSON.stringify(data))
             setProfile(data.data)
             GLOBAL.mydata = data.data
             GLOBAL.wallet = data.data.wallet
        //  alert(GLOBAL.mydata.is_vip_subscribe)
          //   GLOBAL.wallet = data.data.wallet
          // this.setState({username:data.data})
          // this.setState({wallet:data.data.wallet})
          //wallet





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
  useEffect(() => {
    listener = EventRegister.addEventListener('refreh', (data) => {
home()
    })
    const unsubscribew =   navigation.addListener('focus', () => {
      home()
      profile2()
    })
    // alert("hh")
    home()

    profile2()
    profile3()

  //  alert(store.getState().token)

  }, []);

  const handleClose = () =>{
    setShow(false)
  }

  const call = (item) =>{

console.log(JSON.stringify(item))

  GLOBAL.host = item
if (item.is_online == "2"  ){
  if (item.busy_with_user_id ==  store.getState().user.id) {
    navigation.navigate("AudioCall", {
                                                uid: parseInt(store.getState().user.id),
                                                clientRole: Audience,
                                                channelName: item.bridge_id,
                                                onCancel: (message) => {

                  }})
  }
else {

  Toast.showWithGravity(`${item.name} is now busy in a Private Call, please try connecting after sometime`, Toast.LONG, Toast.CENTER);
  return
}

}

    if (item.is_online == "0"  ){
navigation.navigate("AudioViewProfile2")
      return
    }

    if (item.is_premium == "1"  ){
      GLOBAL.is_premium = "1"

      if (profile.is_vip_subscribe == "0"){
        Alert.alert('VIP','Please purchase a VIP subscription to enter this room.',
                                   [
                                       {text:"OK",onPress:()=>navigation.navigate('Vip')
                                       },
                                   ],
                                   {cancelable:false}
                               )

        return
      }

    }


    // if (this.state.username.is_vip_subscribe == 0){
    //   alert('Please become a VIP member to use this feature')
    //   return
    // }


    GLOBAL.host = item
    GLOBAL.bookingid = item.bridge_id
    if (item.is_premium == "0"){
    //  alert('hi')
      navigation.navigate("AudioCall", {
                                                  uid: parseInt(store.getState().user.id),
                                                  clientRole: Audience,
                                                  channelName: item.bridge_id,
                                                  onCancel: (message) => {

                    }})
    }
    if (item.is_premium == "1"){

      navigation.navigate("AudioCall", {
                                                  uid: parseInt(store.getState().user.id),
                                                  clientRole: Audience,
                                                  channelName: item.bridge_id,
                                                  onCancel: (message) => {

                    }})
    }
           
  }

  const edit = (item,index) =>{
  //  alert(JSON.stringify(item))
    setDetail(item)
    setShow(true)
  //  alert(JSON.stringify(item.description))
  }

  const  renderSeparator = ({leadingItem, section,index,trailingItem})=>{




    if (check.indexOf(leadingItem[0].id) !=  -1){
      var c = check.indexOf(leadingItem[0].id)
        return    <View>{add.length !=0 && (
          <Image source={{uri:add[c].imageUrl}}
                      style={imageStyle.bannerImage}
                    />
        )}

                  </View>
    }


    return <View />;
  };

  const editq = (item,index) =>{
  //  alert(item.id)
  //alert(JSON.stringify(item))
    Favorite({id:item.id.toString()})
           .then((data) => {
              //   toggleLoading(false);
   alert(JSON.stringify(data))
             if (data.status) {
               //alert(JSON.stringify(data))
            home()
              // alert(JSON.stringify(data))
              //setUsername(data.data)





             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  //  alert(JSON.stringify(item.description))
  }
  const onPressBack = () => navigation.goBack();
  const renderItem = ({item, index}) => (

      <TouchableOpacity  onPress ={() =>  call(item)}>


      <TouchableOpacity
      onPress ={() =>  call(item)}
           style={styles.flatItem}
           key={item.id}
           >
           <ImageBackground source={{uri:item.imageUrl}} style={styles.imageBackground}>
             <View style={styles.flatItemView}>


             </View>
             <View style={styles.flatItemView2}>
             {item.is_online == "1" && (
               <View style = {{backgroundColor:'#83f52c',width:10,height:10,borderRadius:10,marginTop:32,marginLeft:0}}>
               </View>
             )}
             {item.is_online == "0" && (
               <View style = {{backgroundColor:'red',width:10,height:10,borderRadius:10,marginTop:32,marginLeft:0}}>
               </View>
             )}
             {item.is_online == "2" && (
               <View style = {{backgroundColor:'blue',width:10,height:10,borderRadius:10,marginTop:32,marginLeft:0}}>
               </View>
             )}
               <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>

               {item.is_premium == "1" && (
                 <Image
                     source={require('../resources/vip.png')}
                   style={{width:20,height:20,resizeMode:'contain',position:'absolute',bottom:0,right:-5}}
                 />
               )}

             </View>
             {item.is_online == "1" && (
               <Text numberOfLines={1} style={{color:'white',fontFamily: 'DMSans-Bold',marginTop:-20,

               fontSize: 8,
               padding:5,

               color: '#FFFFFF',
               marginLeft: 5,}}>{item.description}</Text>
             )}

           </ImageBackground>
         </TouchableOpacity>




</TouchableOpacity>
  );
  return (
    <SafeAreaView style={styles.container}>

      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />

      <ImageBackground
          source={require('../resources/bg-l.png')}
        style={{width:'100%',height:'100%',resizeMode:'contain',marginTop:0}}
      >
<ScrollView>
      <FlatList
      numColumns = {3}
        data={username}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}




        />
        </ScrollView>






      <SCLAlert
       theme="info"
       show={show}
       title={detail.title}
       subtitle= {detail.description}
     >
       <SCLAlertButton theme="info" onPress={handleClose}>Okay</SCLAlertButton>
     </SCLAlert>

      </ImageBackground>



    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  gameIcon: {
    height: 72,
    width: 72,
    borderRadius: 36,
  },
  container: {
    flex: 1,
  },
  touchGame: {
    position: 'absolute',
    right: 6,
    bottom: 35,
    borderRadius: 36,
  },
  flatItem: {
    width:window.width/3 - 8.5,

    margin: 2.7,
    marginLeft:4,
    height: 105,
  },
  imageBackground: {
    flex: 1,
      overflow: 'hidden',
      justifyContent: 'space-between',
      borderRadius: 10,
  },
  flatItemView: {
  flexDirection: 'row-reverse',
  margin: 10,
},
  flatItemView2: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
    marginLeft:5,

  },
  itemName: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 15,
    color: '#FFFFFF',
    marginLeft: 5,
    marginTop:32,
  },
  levelView: {
    backgroundColor: '#4A90E2',
    paddingHorizontal: 5,
    borderRadius: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  levelText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  countImage: {
    height: 9,
    width: 9,
    resizeMode: 'contain',
  },
  countText: {
    fontFamily: 'PT Sans',
    fontWeight: '700',
    fontSize: 10,
    color: '#FFFFFF',
    marginLeft: 3,
  },
  countView: {
    flexDirection: 'row',
    paddingHorizontal: 3,
    borderRadius: 10,
    backgroundColor: '#ffffff4d',
    alignItems: 'center',
    borderColor: '#ffffff80',
    borderWidth: 0,
  },
  crownIcon2: {
    marginLeft: 'auto',
    height: 30,
    width: 30,
  },
  backTouch: {
    padding: 5,
  },
  coinstyle: {
   height: 10,
   width: 10,
   resizeMode: 'contain',
 },
 itemInfoTouch: {
   marginLeft: 'auto',
   padding: 12,

 },
 itemInfoIcon:{
   height: 30,
   width: 30,
   resizeMode:"contain",
   marginTop:12
 },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#43FA00',
  },
});

export default AudioBroadcast;
