import React ,{ useEffect ,useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
  View,
  Text,
  Linking,
  Share,
  ImageBackground,
  StatusBar,
} from 'react-native';
//GetProfileApi
const GLOBAL = require('./Global');
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi} from '../backend/Api';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import * as actions from '../redux/actions';
import store from '../redux/store';
const Login = ({navigation}) => {
      const [username, setUsername] = useState({});
      const optionList = [
        {
          name: 'Wallet',
          key: 'Wallet',
          source: require('../resources/wallet.png'),
        },
        {
          name: 'Vip',
          key: 'Vip',
          source: require('../resources/vip.png'),
        },


        {
          name: 'Contact Us',
          key: 'Support',
          source: require('../resources/contact.png'),
        },

        {
          name: 'Refer & Earn',
          key: 'Refer',
          source: require('../resources/referq.png'),
        },


        {
          name: 'Share ',
          key: 'share',
          source: require('../resources/share.png'),
        },
        {
          name: 'Rate us on Play Store ',
          key: 'rate',
          source: require('../resources/star.png'),
        },
        {
          name: 'Settings ',
          key: 'Setting',
          source: require('../resources/settings.png'),
        },


        {
          name: 'Sign Out',
          key: 'Logout',
          source: require('../resources/logout.png'),
        },
      ];

  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                // alert(JSON.stringify(data))
                setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);


  const _fancyShareMessage=()=>{

        var a = username.referral_code
      var commonHtml = `I want to recommend Cleo Live - mobile application.\n\nAt Cleo Live you can Play Live Games with Live Girls and Win lots of Money.\n\nLogin Every day and get Free Coins to Play.\n\nDownload Cleo Live from https://play.google.com/store/apps/details?id=com.cleolive&hl=en_IN&gl=US/\n\nWhile Registering use my referral code ${a} and get Free Coins`;


    Share.share({
            message:commonHtml
        },{
            tintColor:'green',
            dialogTitle:'Share this app via....'
        }
    );
}
  const call = (item) =>{
     if (item.key == "Logout"){
       actions.Logout({});
    navigation.reset({
                    index: 0,
                    routes: [{name: 'Introduction'}],
                  });

     }

     if (item.key == "Setting"){
    navigation.navigate("My")

     }
     if (item.key == "MyEarning"){
    navigation.navigate("MyEarning")

     }
     if (item.key == "Wallet"){
    navigation.navigate("Wallet")

     }
     if (item.key == "Vip"){
    navigation.navigate("Vip")

     }
     if (item.key == "Support"){
    navigation.navigate("Support")

     }
     if (item.key == "Refer"){
    navigation.navigate("Refer")

     }
     if (item.key == "daily"){
    navigation.navigate("Walletss")

     }

     if (item.key == "share"){
    _fancyShareMessage()

     }
     //_fancyShareMessage

     if (item.key == "rate"){
    Linking.openURL('https://play.google.com/store/apps/details?id=com.cleolive&hl=en_IN&gl=US')

     }

     //https://play.google.com/store/apps/details?id=com.mepluser
   }
  return (
    <SafeAreaView style={styles.container}>
      {/* <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      /> */}
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <ScrollView>
          <TouchableOpacity
            style={styles.editTouch}
            onPress={() => navigation.navigate('EditProfile')}>
            <Image
              source={require('../resources/edit2.png')}
              style={styles.editImage}
            />
          </TouchableOpacity>
          <View style={styles.profileView_1}>
            {/* <View style={styles.imageView}></View> */}

            {username.is_vip_subscribe == "1" && (

              <ImageBackground
                  source={require('../resources/crown3.png')}
                  style={{width: 60, height: 40,marginLeft:30,marginTop:-20,resizeMode:'contain',marginRight:5}}


              >

              </ImageBackground>
            )}
            <Image
               source={{uri:username.imageUrl}}

              style={styles.imageView}
            />

            <View style = {{marginLeft:22,marginTop:9,flexDirection:'row',justifyContent:'space-between'}}>

<View>

{username.is_vip_subscribe == "1" && (
  <View style  = {{flexDirection:'row'}}>
  <Image
      source={require('../resources/vip.png')}
      style={{width: 20, height: 20,marginLeft:2,marginTop:2,resizeMode:'contain',marginRight:5}}


  />
<Text style={styles.text_1}>{username.name}</Text>
  </View>
)}
{username.is_vip_subscribe == "0" && (
              <Text style={styles.text_1}>{username.name}</Text>
            )}
              <Text style={styles.text_2w}> ID:{store.getState().user.id}</Text>
              {username.is_vip_subscribe != "0" && (
              <Text style={styles.text_2w}>{username.vip_subscription_left} Left</Text>
            )}
              </View>
              <View style = {{flexDirection:'row',borderRadius:18,borderWidth:2,borderColor:'white',marginTop:4,width:70,height:30}}>
              <Image
                  source={require('../resources/coin.png')}
                  style={{width: 20, height: 20,marginLeft:2,marginTop:2,resizeMode:'contain'}}


              />
<Text style={styles.text_2w}> {username.wallet}</Text>

              </View>
            </View>
          </View>


          <View  style = {{marginTop:30}}>

          </View>

          {optionList.map((item) => (
            <TouchableOpacity
              style={styles.itemView}
              key={item.key}
              onPress={() => call(item)}>
              <Image source={item.source} style={styles.iconImage} />
              <Text style={styles.text_5}>{item.name}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </LinearGradient>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  linearGradient: {
    flex: 1,
  },
  editTouch: {
    marginTop: 40,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 20,
    padding: 10,
  },
  editImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  imageView: {
    height: 88,
    width: 88,
    borderRadius: 44,
    resizeMode: 'cover',
    borderWidth:3,
    borderColor:'white',
    marginLeft:18
  },
  profileView_1: {


    marginHorizontal: 15,
  },
  profileView_2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    marginTop: 10,
  },
  followingView: {
    marginHorizontal: 30,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 20,
    color: '#FFFFFF',
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 15,
    color: '#FFFFFF',
  },
  text_2w: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 10,
    color: '#FFFFFF',
    marginTop:6
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 25,
    lineHeight: 28,
    color: '#FFFFFF',
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 18,
    color: '#FFFFFF',
    marginHorizontal: 15,
    marginBottom: 5,
  },
  text_5: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#FFFFFF',
    marginTop:3
  },
  levelView: {
    backgroundColor: '#F5A623',
    paddingHorizontal: 10,
    borderRadius: 10,
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 10,
  },
  levelText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 15,
    color: '#FFFFFF',
  },
  coinImage: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
  },
  coinText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 15,
    color: '#FFFFFF',
    marginHorizontal: 5,
  },
  coinView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 3,
    borderRadius: 12.5,
  },
  coinView_2: {
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 12.5,
  },
  iconImage: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
  itemView: {
    flexDirection: 'row',
    marginHorizontal: 17,
    borderBottomWidth: 1,
    borderColor: '#ffffff33',
    paddingVertical: 7,
  },
});
export default Login;
