import React, {useRef, useState, useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
  Alert,
  Linking,
  Animated,
  Dimensions
} from 'react-native';
const window = Dimensions.get('window');
import SoundPlayer from 'react-native-sound-player';
import * as SoundHandler from '../utils/SoundHandler';
const GLOBAL = require('./Global');
import store from '../redux/store';
import * as actions from '../redux/actions';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import {hotGif} from './commonScreens';
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi,Today,Claim,Device,Unread,GameImage} from '../backend/Api';
import LinearGradient from 'react-native-linear-gradient';
import DeviceInfo from 'react-native-device-info';
import Geolocation from '@react-native-community/geolocation';
import Game from './game';
import moment from 'moment';
import VideoBroadcast from './VideoBroadcast';
import AudioBroadcast from './AudioBroadcast';
import { EventRegister } from 'react-native-event-listeners'
import LottieView from 'lottie-react-native';
import requestCameraAndAudioPermission from './permission';
import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
const Main = ({navigation}) => {
  const [state, setstate] = useState(1);
  const [visible, setvisible] = useState(false);
    const [visible1, setvisible1] = useState(false);
      const [visible2, setvisible2] = useState(false);
       const [number, setnumber] = useState('0');
  const [amount, setAmount] = useState('');
  const [title, settitle] = useState('');
  const [random, setrandom] = useState({});
  const [title1, settitle1] = useState('');
    const [call, setcall] = useState({});
      const [duration, setduration] = useState('0');
      const [callend, setcallesnd] = useState(false);
  const [notification, setNotifcation] = useState('');
  const [floating, setFloating] = useState(false);

  const [header, setHeader] = useState({
    isHeaderHidden: false,
  });
  const [username, setUsername] = useState({});
  const refView = useRef();
  const scrollY = new Animated.Value(0);
  const diffclamp = Animated.diffClamp(scrollY, 0, 50);
  const translateY = diffclamp.interpolate({
    inputRange: [0, 50],
    outputRange: [0, -15],
  });

  const callback = () =>{
    navigation.navigate('LiveBroadcast')
  }
  const unread = () =>{
    Unread({user_id:"1"})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {

  setnumber(data.data.sum ? data.data.sum :"0")





             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  }
  const videocallback =(data) =>{
    alert(JSON.stringify(data))
  }
  const videocallback1 =(data) =>{
    alert(JSON.stringify(data))
  }
  const callback2 = () =>{
    navigation.navigate('LiveBroadcast1')
  }
  const callback1 = (data) =>{
    Alert.alert('Rules','Which Language do you Prefer.',
                               [
                                   {text:"English",onPress:()=>data == "r" ? Linking.openURL('http://cleolive.com/Roulette_english'):Linking.openURL('http://cleolive.com/Dice_english')
                                   },
                                   {text:"Hindi",onPress:()=>data == "r" ? Linking.openURL('http://cleolive.com/Roulette_hindi'):Linking.openURL('http://cleolive.com/Dice_hindi')
                                   },
                               ],
                               {cancelable:false}
                           )

  //  navigation.navigate('Rule')
  }
  const claim = () =>{
    Claim({amount:amount,device_id: DeviceInfo.getDeviceId()})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {
                setvisible(false)
                profile()

              // alert(JSON.stringify(data))
              //setUsername(data.data)





             } else {
                setFloating(false)
  //  alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  }

const profile = () =>{

  Device({device_id: DeviceInfo.getDeviceId()})
         .then((data) => {
            //   toggleLoading(false);
            //  alert(JSON.stringify(data))

           if (data.status) {
             console.log(JSON.stringify(data))

            // alert(JSON.stringify(data))
            //setUsername(data.data)





           } else {
             Alert.alert('Blocked','Your device has been Blocked by admin',
                                      [
                                          {text:"OK",
                                          },
                                      ],
                                      {cancelable:false}
                                  )

             actions.Logout({});
          navigation.reset({
                          index: 0,
                          routes: [{name: 'Introduction'}],
                        });
  //alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });


  GetProfileApi({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
             setUsername(data.data)
             GLOBAL.name =data.data.name
            // alert(JSON.stringify(data.data.name))
            //setUsername(data.data)





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}

const profile1 = () =>{

  GameImage({device_id: DeviceInfo.getDeviceId()})
         .then((data) => {
            //   toggleLoading(false);
            //  alert(JSON.stringify(data))

           if (data.status) {
             console.log(JSON.stringify(data))

            // alert(JSON.stringify(data))
            //setUsername(data.data)





           } else {

  //alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });



}


const ok = () =>{
  setvisible1(false)
}

const hello = (info) =>{
  console.log(info)
  GLOBAL.gllat = info.coords.latitude
  GLOBAL.gllong = info.coords.longitude

}


accept = () =>{
  setvisible2(false)
  navigation.navigate("Wallet")
}

useEffect(() => {
  if (visible2 == true){
    SoundHandler.play()
    //SoundPlayer.playSoundFile('incoming', 'mp3')
  }else {
    SoundHandler.stop()
  }

},[visible2])
  useEffect(() => {
    requestCameraAndAudioPermission().then(_ => {
                console.log('requested!');
            })
      Geolocation.getCurrentPosition(info => hello(info));
    profile1()
  //  SoundPlayer.playSoundFile('incoming', 'mp3')

  //SoundHandler.play()

    const socket = io('http://51.79.250.86:3000', {
    transports: ['websocket']
  })

  socket.on("complete_call_data", msg => {

if (msg.status == true){
 setcall(msg)

 var now = moment(msg.end_time); //todays date
 var end = moment(msg.start_time); // another date
 var duration = now.diff(end,'seconds');

   console.log('pagal')

 setduration(duration.toString())
 //alert(duration)
 setcallesnd(true)


 //  this.setState({call:msg})
 // this.setState({callend:true})

 }
})


  socket.on("total_unread_messages", msg => {
  if (msg.status == true){
setnumber(msg.data.sum ? msg.data.sum :"0")
  }else {
    setnumber("0")
  }

  })
socket.emit('total_unread_messages',{
                                user_id:store.getState().user.id


             })

  socket.emit('complete_call_data',{
                                  user_id:store.getState().user.id


               })

  listener = EventRegister.addEventListener('notification', (data) => {

    if (data.data.hasOwnProperty('body')){
      setNotifcation(data.data.body)
      settitle(data.data.title)
      settitle1(data.data.image)
    }else {
      setNotifcation(data.message)
      settitle(data.title)
      if (data.hasOwnProperty("largeIconUrl")){
          settitle1(data.largeIconUrl)
      }else {
        settitle1(data.image)
      }

    }

setvisible1(true)



   })

  socket.on('connect', () => {

      console.log("socket connected")

      socket.on("user_blocked", msg => {
 //alert(JSON.stringify(msg))
   if (msg.status == true){
     Alert.alert('Deactivated','Your account have been deactivated by admin',
                              [
                                  {text:"OK",
                                  },
                              ],
                              {cancelable:false}
                          )

     actions.Logout({});
  navigation.reset({
                  index: 0,
                  routes: [{name: 'Introduction'}],
                });
   }else{
   //RtcEngine.setClientRole(Audience)
   }

   });



   socket.on("refresh_broadcasters", msg => {

  if (msg.status == true){
 EventRegister.emit('refreh', msg)
  }
  })

   socket.emit('refresh_broadcasters',{
                                   user_id:store.getState().user.id
                })


   socket.on("random_call", msg => {

if (msg.status == true){
  console.log(JSON.stringify(msg.data))
  setrandom(msg.data)
setvisible2(true)
}else {
  setvisible2(false)
}
})

   socket.emit('random_call',{
                                   user_id:store.getState().user.id
                })

      socket.emit('user_blocked',{
                                      user_id:store.getState().user.id
                   })


    })
  //device_id: DeviceInfo.getDeviceId()}


  socket.on("can_access_login_this_device", msg => {
    //alert(JSON.stringify(msg))
    if(msg.status == true){

    }else {
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Introduction'}],
                 });
    }

  })
  socket.emit('can_access_login_this_device',{
                                  device_id: DeviceInfo.getDeviceId(),
                                  user_id:store.getState().user.id

               })

  socket.emit('user_online',{
                                  token:store.getState().user.token,
                                  user_id:store.getState().user.id,

               })


    Today({user_id:"1",device_id: DeviceInfo.getDeviceId()})
           .then((data) => {
              //   toggleLoading(false);
//alert(JSON.stringify(data))

             if (data.status) {
               setvisible(true)
               setAmount(data.coins_claim)

              // alert(JSON.stringify(data))
              //setUsername(data.data)






             } else {
                setvisible(false)
  //  alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });


    const unsubscribew =   navigation.addListener('focus', () => {
      unread()

      profile()
    })
  //  console.log(store.getState().user_detail);
    // scrollY.addListener(({value}) => {
    // console.log(
    // refView.current.setNativeProps({
    //   height: 0,
    // });
    // );
    // console.log(refView.current.nativeProps);
    // });
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      {/* <Animated.View
        style={{
          transform: [{translateY: translateY}],
          // marginBottom: 150,
        }}> */}
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <View style={[styles.headerView_1]} ref={refView}>
          <View style={styles.headerIconCoin}>
            <Image
              style={styles.coinIcon}
              source={require('../resources/coin.png')}
            />
            <Text style={styles.coinText}>{username.wallet}</Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Search')}>
            <Image
              source={require('../resources/ic_right.png')}
              style={styles.headerIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('LeaderBoard')}>
          <View>
            <Image
              source={require('../resources/chat.png')}
              style={styles.headerIcon}
            />
            <View style = {{width:20,height:20,borderRadius:13,backgroundColor:'white',marginTop:-30,marginLeft:19}}>
             <Text style = {{color:'black',textAlign:'center',fontSize:8,marginTop:4}}>

             {number}
             </Text>
             </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.headerView_2}>

          <View style={[styles.optionView, {padding: 0}]}>
            {hotGif()}
            <TouchableOpacity
              style={[styles.optionView, state == 1 && styles.selectView]}
              onPress={() => setstate(1)}>
              <Text style={styles.text_1}>1-1</Text>
              <Text style={styles.text_1}>Video</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={[styles.optionView, state == 2 && styles.selectView]}
            onPress={() => setstate(2)}>
            <Text style={styles.text_1}>Play</Text>
            <Text style={styles.text_1}>Games</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.optionView, state == 3 && styles.selectView]}
            onPress={() => setstate(3)}>
            <Text style={styles.text_1}>1-1</Text>
            <Text style={styles.text_1}>Audio</Text>
          </TouchableOpacity>


        </View>
      </LinearGradient>
      {/* </Animated.View> */}
{state === 2 && <Game show="playGames" callback = {callback} callback1 = {callback1} callback2 = {callback2}/>}
{state === 1 && <VideoBroadcast show="playGames" callback = {videocallback} navigation = {navigation} />}
{state === 3 && <AudioBroadcast show="playGames" callback = {videocallback1} navigation = {navigation} />}



<Dialog dialogStyle = {{borderRadius: 26,backgroundColor:'transparent',marginTop:100}}
   visible={visible}
   style = {{backgroundColor:'red'}}


   onTouchOutside={() => {
     setvisible(false)

   }}
 >

   <DialogContent style = {{width:300,alignSelf:'center',backgroundColor:'transparent',borderRadius:20}} >

     <Image
         source={require('../resources/bonus.png')}

         style={{width: 280, height: 500,alignSelf:'center',resizeMode:'contain',marginLeft:0,marginTop:-26,borderRadius:32}}


     />


<Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:34,marginTop:-355}}>{amount} Coins credited to your wallet</Text>
<Text style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:-30}}>Check In Every Day and Get Free Coins</Text>

<Image
    source={require('../resources/img.png')}
    style={{width: 140, height: 100,marginLeft:0,marginTop:-20,alignSelf:'center',resizeMode:'contain'}}


/>
<TouchableOpacity onPress={() => claim()}>
<Image
    source={require('../resources/btn.png')}
    style={{width: 140, height: 60,marginLeft:0,marginTop:0,alignSelf:'center',resizeMode:'contain'}}


/>
</TouchableOpacity>





   </DialogContent>
 </Dialog>

 <Dialog dialogStyle = {{borderRadius: 26,backgroundColor:'transparent',marginTop:100}}
    visible={visible1}
    style = {{backgroundColor:'red'}}


    onTouchOutside={() => {
      setvisible1(false)

    }}
  >

    <DialogContent style = {{width:300,alignSelf:'center',backgroundColor:'transparent',borderRadius:20}} >

      <Image
          source={require('../resources/l-bg.png')}

          style={{width: 280, height: 500,alignSelf:'center',marginLeft:0,marginTop:-26,borderRadius:32}}


      />

      {title1 == "default.png" && (
        <Image
            source={require('../resources/logo.png')}

            style={{width: 80, height: 80,alignSelf:'center',resizeMode:'contain',marginLeft:0,marginTop:-440}}


        />
      )}

      {title1 != "default.png" && (
        <Image
            source={{uri:title1}}

            style={{width: 280, height: 200,alignSelf:'center',marginLeft:0,marginTop:-470}}


        />
      )}

  <Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:34,marginTop:1}}>{title}</Text>
  <Text style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:-26}}>{notification}</Text>





 <TouchableOpacity onPress={() => ok()}>
 <View style = {{backgroundColor:'black',width:100,borderRadius:12,height:40,alignSelf:'center'}}>
   <Text style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:9}}>OK</Text>
 </View>
 </TouchableOpacity>





    </DialogContent>
  </Dialog>



  <Dialog dialogStyle = {{borderRadius: 26,backgroundColor:'transparent',marginTop:100,height:400}}
     visible={visible2}
     style = {{backgroundColor:'red'}}


     onTouchOutside={() => {
       setvisible2(false)

     }}
   >

     <DialogContent style = {{width:310,alignSelf:'center',backgroundColor:'transparent',borderRadius:20,borderWidth:5,borderColor:"white",height:400}} >

       <ImageBackground
           source={{uri:random.imageUrl}}

           style={{width: 300, height: 396,alignSelf:'center',marginLeft:0,marginTop:0,borderRadius:32}}


       >

<View style = {{flexDirection:'row'}}>
       <Image
                    source={{uri:random.imageUrl}}
                    style={{width: 60, height: 60,borderRadius:30,marginLeft:10,marginTop:17}}


                />

<Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',marginTop:22}}>{random.name}</Text>
</View>
   <Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:34,marginTop:140}}>Video Call from {random.name}</Text>





<View style = {{flexDirection:'row',justifyContent:'space-between',width :290,alignSelf:'center',marginTop:10}}>
  <TouchableOpacity onPress={() => accept()}>
  <Image
               source={require('../resources/whatsapp.png')}
               style={{width: 60, height: 60,resizeMode:'contain'}}


           />
  </TouchableOpacity>


  <TouchableOpacity onPress={() => accept()}>
  <Image
               source={require('../resources/iconse.png')}
               style={{width: 60, height: 60,resizeMode:'contain'}}


           />
  </TouchableOpacity>

  <TouchableOpacity onPress={() => accept()}>
  <Image
               source={require('./btn_endcall.png')}
               style={{width: 60, height: 60,resizeMode:'contain'}}


           />
  </TouchableOpacity>


  </View>

  </ImageBackground>



     </DialogContent>
   </Dialog>

   <Dialog dialogStyle = {{borderRadius: 26,backgroundColor:'transparent',marginTop:100}}
      visible={callend}
      style = {{backgroundColor:'red'}}


      onTouchOutside={() => {
      setcallesnd(false)


      }}
    >

      <DialogContent style = {{width:300,alignSelf:'center',backgroundColor:'transparent',borderRadius:20}} >

        <Image
            source={require('../resources/l-bg.png')}

            style={{width: 280, height: 500,alignSelf:'center',marginLeft:0,marginTop:-26,borderRadius:32}}


        />


          <Image
              source={require('../resources/logo.png')}

              style={{width: 30, height: 30,alignSelf:'center',resizeMode:'contain',marginLeft:0,marginTop:-440}}


          />

<Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',marginTop:22}}>{call.call_type == "one_to_one_video" ? "Video" :"Audio"} Call completed Successfully</Text>


          <View style = {{flexDirection:'row',justifyContent:"space-between"}}>
          <Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginLeft:10,color:'white',marginTop:1}}>Duration</Text>
          <Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginRight:10,textAlign:'center',color:'white'}}>{duration} Seconds</Text>

          </View>

<View style = {{flexDirection:'row',justifyContent:"space-between"}}>
<Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginLeft:10,color:'white',marginTop:1}}>Total Cost</Text>
<Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginRight:10,textAlign:'center',color:'white'}}>🥇{call.txn_amount}</Text>

</View>
<View style = {{flexDirection:'row',justifyContent:"space-between"}}>
<Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginLeft:10,color:'white',marginTop:1}}>Updated Balance</Text>
<Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',marginRight:10,textAlign:'center',color:'white'}}>🥇{call.new_wallet}</Text>

</View>



   <TouchableOpacity onPress={() =>   setcallesnd(false)}>
   <View style = {{backgroundColor:'black',width:70,borderRadius:12,height:40,alignSelf:'center',marginTop:10}}>
     <Text style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:9}}>OK</Text>
   </View>
   </TouchableOpacity>





      </DialogContent>
    </Dialog>

    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  linearGradient: {
    paddingTop: 35,
    paddingBottom: 10,
    justifyContent: 'flex-end',
  },
  headerView_1: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingTop: 10,
    paddingHorizontal: 10,
    // backgroundColor: 'red',
  },
  headerView_2: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-evenly',
  },
  headerIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 5,
  },
  optionView: {
    alignItems: 'center',
    padding: 8,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 15,
    color: '#FFFFFF',
  },
  selectView: {
    borderRadius: 12,
    backgroundColor: '#ffffff26',
  },
  headerIconCoin: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginHorizontal: 10,
  },
  coinIcon: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  coinText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 15,
    color: '#FFFFFF',
  },
});
export default Main;
