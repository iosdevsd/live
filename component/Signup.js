import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,TextInput} from 'react-native';
import style from '../style/Style.js';
import {LoginOtpApi,SignUpApi} from '../backend/Api';
import ImagePicker from 'react-native-image-picker';
import * as actions from '../redux/actions';
const options = {
  title: 'Select Profile Picture',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
const Signup = ({navigation,route}) => {
const [mobile, setMobile] = useState('');
const [college, setCollege] = useState('');
const [image,setImage] = useState('');
const [state, setState] = useState({
  phone:route.params.mobile,

});



const picker = () =>{

        ImagePicker.showImagePicker(options, (response) => {


    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {

setImage(response.uri)


    }
  });

}

const loginButtonPress = async () =>{

  const data = new FormData();
               data.append('name',mobile);
               data.append('mobile',state.phone);
                 data.append('college',college);
                 data.append('device_id',"iOS");
                   data.append('device_type',"iOS");
                     data.append('device_token',"iOS");
                     data.append('model_name',"iOS");
                     data.append('image', {
                  uri: image,
                  type: 'image/jpg', // or photo.type
                  name: 'image.jpg'
              });

    SignUpApi(data)
         .then((data) => {
           console.log(JSON.stringify(data))
           if (data.status) {
             actions.Login(data.user);
             AsyncStorageSetUser(data.user);
             navigation.reset({
                             index: 0,
                             routes: [{name: 'Home'}],
                           });
          //   alert(data.otp)
          // navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})

           } else {
            // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
           }
         })
         .catch((error) => {
           console.log('error', error);
         });




}

  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/background_slide.png') }>
      <TouchableOpacity onPress={() => picker()} >
{image == "" && (
  <Image
               style={style.imageprofile}
               source={require('../resources/camera.png')}
           />
)}
{image != "" && (
  <Image
               style={style.imageprofile}
               source={{uri:image}}
           />
)}

               </TouchableOpacity>


               <Text style = {style.introtextss}>
              Name
               </Text>
               <TextInput placeholder="Enter Name" style={styles.textInput}
               onChangeText={(text) => setMobile(text)}
              value={mobile}
               ></TextInput>
               <Text style = {style.introtextss}>
              Enter College Name
               </Text>
               <TextInput placeholder="Enter College Name" style={styles.textInput}
               onChangeText={(text) => setCollege(text)}
              value={college}
               ></TextInput>

               <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                  <View>
                                                                  <Text style = {style.next}>
                                                                  SUBMIT
                                                                  </Text>

                                                                  </View>
                                                                  </TouchableOpacity>

      </ImageBackground>
    </View>
  );
};

export default Signup;
