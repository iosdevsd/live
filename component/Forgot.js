import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ScrollView,
  Platform,
  TextInput,
  Dimensions,
  StatusBar,
} from 'react-native';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
const window = Dimensions.get('window');
import DeviceInfo from 'react-native-device-info';
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin,Forgotq} from '../backend/Api';

import * as actions from '../redux/actions';
const Forgot = ({navigation}) => {
  const [state, setState] = useState({
  loading: false,



});
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const toggleLoading = (bol) => setState({...state, loading: bol});
  const [keyTypeNum, setKeyTypeNum] = useState(false);
  const onPress = () => {

    if (username == ""){
      Toast.showWithGravity('Please enter Email/Mobile', Toast.LONG, Toast.CENTER);
      return
    }
    toggleLoading(true);

    const json = {
  username: username,


};
console.log(JSON.stringify(json))

Forgotq(json)
       .then((data) => {
            toggleLoading(false);

         if (data.status) {
          // alert(JSON.stringify(data))
          navigation.navigate('ForgotOtp',{item:data.data})





         } else {
           console.log(JSON.stringify(data))
           if (data.signal == "email_not_exists" || data.signal == "phone_not_exists" ){
                        Alert.alert('USER NOT FOUND',`This ${username} is not registered at Cleo Live,  Kindly create a new account from Sign Up.`,
                                                 [
                                                     {text:"OK",
                                                     },
                                                 ],
                                                 {cancelable:false}
                                             )
                      }else {
                          Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
                      }

         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };

  const onPresss = () => {
navigation.navigate('Register')


  };
  const onForgot = () => {
navigation.navigate('Forgot')


  };
  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
      {state.loading && <Loader />}
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Forgot Password</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
              source = {require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <Text style={styles.text_1}>Enter your mobile number or email</Text>


      <View style = {{marginLeft:28,width:window.width -56,marginTop:-20}}>
    <TextField
        label='Email/Mobile'
        baseColor = '#acb1c0'
        tintColor = '#acb1c0'
        value = {username}
        onChangeText={(text) => setUsername(text)}


    />
    </View>

<View style = {{marginTop:12}}>
</View>

      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
          <Text style={styles.text_3}>Next</Text>
        </TouchableOpacity>
      </LinearGradient>


</ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 55,
    marginBottom: 42,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    height: 50,
    width: '90%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export default Forgot;
