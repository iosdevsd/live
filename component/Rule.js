import React ,{ useEffect ,useState} from 'react';
import {
  FlatList,
  StyleSheet,
  Image,
  StatusBar,
  SafeAreaView,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  NativeModules,
} from 'react-native';
import Toast from 'react-native-simple-toast';
const GLOBAL = require('./Global');

import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert';

import Orientation from 'react-native-orientation';
const {Agora} = NativeModules;
const {
 FPS30,
 AudioProfileDefault,
 AudioScenarioDefault,
 Host,
 Audience,
 Adaptative
} = Agora
import {textStyle, viewStyle} from '../style/Style';
const {width} = Dimensions.get('window')
import LinearGradient from 'react-native-linear-gradient';
import {imageStyle} from '../style/Style';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend,FetchHome,Favorite} from '../backend/Api';
const Rule = ({showFloatButton, onPress, onPressImage, onScroll,navigation}) => {
    const [username, setUsername] = useState({});
      const [detail, setDetail] = useState('');
        const [show, setShow] = useState(false);
  const colorList = [
    '#4A90E2',
    '#9013FE',
    '#F5A623',
    '#8B572A',
    '#417505',
    '#BD10E0',
  ];
  const flatListData = [
    {
      id: 1,
      level: 1,
      score: 1125,
      name: 'Shivani Singh',
      source: require('../resources/user/user1.jpg'),
    },
    {
      id: 2,
      level: 1,
      score: 508,
      name: 'Deepika',
      source: require('../resources/user/user5.jpg'),
    },
    {
      id: 3,
      level: 1,
      score: 1335,
      name: 'rabiya singh',
      source: require('../resources/user/user3.jpg'),
    },
    {
      id: 4,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../resources/user/user4.jpg'),
    },
    {
      id: 5,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../resources/user/user2.jpg'),
    },
    {
      id: 6,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../resources/user/user1.jpg'),
    },
    {
      id: 7,
      level: 1,
      score: 1335,
      name: 'rabiya singh',
      source: require('../resources/user/user3.jpg'),
    },
    {
      id: 8,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../resources/user/user4.jpg'),
    },
  ];


const home = () =>{
  FetchHome({latitude:"0",longitude:"0"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
             setUsername(data.data)
            // alert(JSON.stringify(data))
            //setUsername(data.data)





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
  home()
    })
  //  alert(store.getState().token)

  }, []);

  const handleClose = () =>{
    setShow(false)
  }

  const call = (item) =>{

    if (item.is_online == "0"  ){
        Toast.showWithGravity('This Live Dealer is not online at the moment. Please check the Room timings. Please also like this Live Dealer to get an alert when this room starts. ', Toast.LONG, Toast.CENTER);
        return
    }


    GLOBAL.host = item
    GLOBAL.bookingid = item.bridge_id
                navigation.navigate("LiveVideoCall", {
                                            uid: Math.floor(Math.random() * 100),
                                            clientRole: Audience,
                                            channelName: item.bridge_id,
                                            onCancel: (message) => {

              }})
  }

  const edit = (item,index) =>{
    setDetail(item.description)
    setShow(true)
  //  alert(JSON.stringify(item.description))
  }

  const editq = (item,index) =>{
  //  alert('hi')
  //alert(JSON.stringify(item))
    Favorite({host_id:item.id})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {
               //alert(JSON.stringify(data))
            home()
              // alert(JSON.stringify(data))
              //setUsername(data.data)





             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  //  alert(JSON.stringify(item.description))
  }
  const onPressBack = () => navigation.goBack();
  const renderItem = ({item, index}) => (
      <TouchableOpacity  onPress ={() =>  call(item)}>

      <ImageBackground source={require('../resources/boxq.png')} style={{width:'100%',height:146}}
      imageStyle = {{borderRadius:22}}>
    <View>

<View  style = {{flexDirection:'row'}}>
<Image source={{uri:item.imageUrl}} style={{width:'40%',height:132,borderTopLeftRadius:22,borderBottomLeftRadius:22,borderTopRightRadius:12,borderBottomRightRadius:12,marginTop:4,marginLeft:8}}/>
{item.is_online == "1" && (
  <View style = {{backgroundColor:'#83f52c',width:10,height:10,borderRadius:10,marginTop:10,marginLeft:-20}}>
  </View>
)}
{item.is_online != "1" && (
  <View style = {{backgroundColor:'red',width:10,height:10,borderRadius:10,marginTop:10,marginLeft:-20}}>
  </View>
)}


 <View style = {{width:'44%',marginTop:10}}>
 <View style = {{width:60,height:22,borderRadius:5,marginTop:10,marginLeft:20,backgroundColor:'#FFDF8F'}}>
 <Text style = {{color:'green',fontFamily:'DMSans-Bold',fontSize:11,textAlign:'center',marginTop:3}}>
 10 Coins

 </Text>
 </View>
 <Text style = {{color:'white',fontFamily:'DMSans-Bold',fontSize:13,marginTop:6,marginLeft:22}}>
{item.name}

 </Text>
 <View  style = {{marginLeft:22,flexDirection:'row',marginTop:6}}>
 <Image
     source={require('../resources/userq.png')}
   style={{width:9,height:9,resizeMode:'contain'}}
 />
 <Text style = {{color:'white',fontFamily:'DMSans-Regular',fontSize:9,marginTop:0,marginLeft:4}}>
{item.count_joiners} Players

 </Text>
 </View>
 <Text style = {{color:'white',fontFamily:'DMSans-Bold',fontSize:13,marginTop:6,marginLeft:22}}>
{item.title}

 </Text>
 </View>


 <View  style = {{marginTop:20,flexDirection:'row'}}>
{item.is_favourite  == "0" && (
  <TouchableOpacity

    onPress={() => editq(item,index)}>
  <Image
      source={require('../resources/heartqw.png')}
    style={{width:20,height:20,resizeMode:'contain'}}
  />
  </TouchableOpacity>
)}
{item.is_favourite  == "1" && (
  <TouchableOpacity

    onPress={() => editq(item,index)}>
  <Image
      source={require('../resources/heartq.png')}
    style={{width:20,height:20,resizeMode:'contain'}}
  />
  </TouchableOpacity>
)}


    <TouchableOpacity

      onPress={() => edit(item,index)}>
  <Image
      source={require('../resources/information-button.png')}
    style={{width:20,height:20,resizeMode:'contain',marginLeft:6}}
  />

  </TouchableOpacity>
 </View>




</View>






</View>
</ImageBackground>
</TouchableOpacity>
  );
  return (
    <SafeAreaView style={styles.container}>

      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />

      <ImageBackground
          source={require('../resources/bg-vip.png')}
        style={{width:'100%',height:'100%',resizeMode:'contain',marginTop:0}}
      >

      <ScrollView>
      <Image
          source={require('../resources/rules.png')}
        style={{width:width - 30 ,height:1000,resizeMode:'cover',marginTop:20,alignSelf:'center',marginTop:80}}
      />

      </ScrollView>

      <View style={{position:'absolute',top:30,right:10}}>
        <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
          <Image
              source={require('../resources/crossq.png')}
            style={{width:40,height:30,resizeMode:'contain',marginTop:20}}
          />
        </TouchableOpacity>



      </View>


      </ImageBackground>



    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  gameIcon: {
    height: 72,
    width: 72,
    borderRadius: 36,
  },
  container: {
    flex: 1,
  },
  touchGame: {
    position: 'absolute',
    right: 6,
    bottom: 35,
    borderRadius: 36,
  },
  flatItem: {
    flex: 1,
    margin: 4,
    height: 180,
  },
  imageBackground: {
    flex: 1,
    overflow: 'hidden',
    justifyContent: 'space-between',
    borderRadius: 10,
  },
  flatItemView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
  },
  flatItemView2: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  itemName: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 15,
    color: '#FFFFFF',
    marginLeft: 5,
  },
  levelView: {
    backgroundColor: '#4A90E2',
    paddingHorizontal: 7,
    borderRadius: 10,
    justifyContent: 'center',
  },
  levelText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  countImage: {
    height: 9,
    width: 9,
    resizeMode: 'contain',
  },
  countText: {
    fontFamily: 'PT Sans',
    fontWeight: '700',
    fontSize: 10,
    color: '#FFFFFF',
    marginLeft: 3,
  },
  countView: {
    flexDirection: 'row',
    paddingHorizontal: 3,
    borderRadius: 10,
    backgroundColor: '#ffffff4d',
    alignItems: 'center',
    borderColor: '#ffffff80',
    borderWidth: 0,
  },
  crownIcon2: {
    marginLeft: 'auto',
    height: 30,
    width: 30,
  },
  backTouch: {
    padding: 5,
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#43FA00',
  },
});

export default Rule;
