import React from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';

const LiveHouse = ({onPress}) => {
  const flatListData_2 = [
    {id: 1, house: 'House 1', source: require('../assets/l1.png')},
    {id: 2, house: 'House 2', source: require('../assets/l1.png')},
    {id: 3, house: 'House 3', source: require('../assets/l1.png')},
  ];
  const renderItem_2 = ({item}) => (
    <TouchableOpacity key={item.id} onPress={onPress}>
      <ImageBackground source={item.source} style={styles.itemView}>
        <View style={styles.titleView}>
          <Text style={styles.house}>{item.house}</Text>
          <Image
            source={require('../assets/infobutton.png')}
            style={styles.iconImage}
          />
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
  return (
    <>
      <FlatList
        data={flatListData_2}
        renderItem={renderItem_2}
        contentContainerStyle={{
          paddingBottom: 10,
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </>
  );
};

const styles = StyleSheet.create({
  itemView: {
    flex: 1,
    flexDirection: 'column-reverse',
    overflow: 'hidden',
    height: 242,
    margin: 10,
    marginBottom: 0,
    borderRadius: 20,
  },
  titleView: {
    padding: 12,
    backgroundColor: '#00000080',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  house: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 16,
    color: '#FFFFFF',
  },
  iconImage: {
    height: 17,
    width: 17,
  },
});
export default LiveHouse;
