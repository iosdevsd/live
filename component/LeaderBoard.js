import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Dimensions,
  FlatList
} from 'react-native';
const window = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import moment from 'moment';
const GLOBAL = require('./Global');
import store from '../redux/store';

import {ChatHistory,GetProfileApi} from '../backend/Api';
var array1 = [
  {
    title:"English",
    selected :''
  },
  {
    title:"Hindi",
    selected :''
  },
  {
    title:"Arabic",
    selected :''
  },


]
const LeaderBoard = ({navigation,route}) => {
const [lang, setLanguage] = useState([]);
  const [is_vip_subscribe,is_vip_subscribe1] = useState('0');





useEffect(() => {
  GetProfileApi({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
             is_vip_subscribe1(data.data.is_vip_subscribe)
          // alert(JSON.stringify(data.data.is_vip_subscribe))
        //    GLOBAL.wallet = data.data.wallet

        //  this.setState({username:data.data})
        //  this.setState({wallet:data.data.wallet})
          //wallet





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });

  const unsubscribew =   navigation.addListener('focus', () => {

  ChatHistory({user_id:store.getState().user.id})
         .then((data) => {



           if (data.status) {

setLanguage(data.data)
console.log(JSON.stringify(data))


           } else {
//  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
       })

//  alert(store.getState().token)

}, []);


const send = (item) =>{
GLOBAL.user_id = store.getState().user.id
GLOBAL.chatid = item.chatgid
if (store.getState().user.id == item.sender_id){
  GLOBAL.another = item.receiver_id
}else {
    GLOBAL.another = item.sender_id
}

if (item.sender_id ==  store.getState().user.id ){
  GLOBAL.nasme = item.receiver.name
}else {
  GLOBAL.nasme = item.sender.name
}

if (GLOBAL.user_id == GLOBAL.another){
  return
}
if (is_vip_subscribe == "0"){
  Alert.alert('VIP only','Please become a VIP member to use this feature',
                             [
                                 {text:"OK"},
                             ],
                             {cancelable:false}
                         )

  return
}
//GLOBAL.nasme = item.sender_id ==  store.getState().user.id ? item.receiver.name : item.sender.name
navigation.navigate("MyChat")

}

  const renderItemTv1=({item, index}) => {


      // alert(JSON.stringify(item))
  return(



    <TouchableOpacity
      onPress={() => send(item)}>
      <View style={{backgroundColor:item.unread_count == "0" ? "#F5F5F5":"#E5F2FA",color :'white', flex: 1,marginTop:5 ,borderRadius :9,width : window.width - 40 ,alignSelf:'center' , shadowColor: '#000',borderRadius:12,
                     }}>
                     <View style = {{flexDirection:'row'}}>
  {item.sender_id ==  store.getState().user.id && (
                     <Image       source={{uri:item.receiver.imageUrl}}
                                              style  = {{width:40, height:40,resizeMode:'stretch',margin:10,borderRadius:8
                                              }}

                                     />
                                   )}
                                   {item.sender_id !=  store.getState().user.id && (
                                                      <Image       source={{uri:item.sender.imageUrl}}
                                                                               style  = {{width:40, height:40,resizeMode:'stretch',margin:10,borderRadius:8
                                                                               }}

                                                                      />
                                                                    )}
                                     <View style = {{marginTop:4}}>
                  <View style = {{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={{fontFamily:"DMSans-Bold",color:'black',fontSize:14,marginLeft:6,marginTop:8,width:window.width - 190,lineHeight:20}}>
                              {item.sender_id ==  store.getState().user.id ? item.receiver.name : item.sender.name}

                                   </Text>
                                   <Text style={{fontFamily:'DMSans-Medium',fontSize:12,marginRight:6,marginTop:10,color:'black'}}>
{moment(item.created_at).format('DD/MM')} {moment(item.created_at).format('HH:MM')}


                 </Text>
                  </View>
                  <View style = {{flexDirection:'row'}}>
                     <Text style={{fontFamily:"DMSans-Medium",color:'grey',fontSize:14,marginLeft:6,marginTop:1,width:window.width - 140,lineHeight:20}}>
                     {item.message}

                     </Text>
                     <View style = {{width:20,height:20,borderRadius:13,backgroundColor:'#F00B51',marginTop:1}}>
                  <Text style = {{color:'white',textAlign:'center',fontSize:8,marginTop:4}}>

                  {item.unread_count}
                  </Text>
                  </View>
                     </View>
                  </View>

                     </View>

                     </View>

    </TouchableOpacity>



  )
  }


  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Chat History</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>

      </LinearGradient>

<FlatList  style={{width:'100%',marginTop:20,alignSelf:'center'}}
data={lang}
showsHorizontalScrollIndicator={false}
renderItem={renderItemTv1}
/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  calImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  calView: {
    position: 'absolute',
    right: 18,
    bottom: 8,
  },
  calTouch: {
    padding: 5,
  },
  bg1: {
    height: 120,
    width: 350,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 8,
    overflow: 'hidden',
  },
  bg2: {
    height: 120,
    width: 160,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 8,
    overflow: 'hidden',
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
  view_1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image_1: {
    height: 32,
    width: 32,
    resizeMode: 'contain',
  },
  coin_text_1: {
    marginLeft: 5,
    fontFamily: 'DMSans-Bold',
    fontWeight: '900',
    fontSize: 35,
    color: '#FFFFFF',
  },
  view_2: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '900',
    fontSize: 35,
    color: '#FFFFFF',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
});

export default LeaderBoard;
