import React, {Component} from 'react';
import { StyleSheet, Text, View, Button,Image, Dimensions,Alert } from 'react-native';
import Backend from "./Backend.js";
import { GiftedChat, InputToolbar,Send } from 'react-native-gifted-chat'
const GLOBAL = require('./Global');
import {LoginOtpApi,SignUpApi,RegisterVerify,EditDetail,GetProfileApi} from '../backend/Api';
import store from '../redux/store';
import moment from 'moment';
const window = Dimensions.get('window');
import { EventRegister } from 'react-native-event-listeners';
type Props = {};
export default class Chat extends Component<Props> {
    state = {
        messages: [],
        name:'',
        username:{}
    };




    renderInputToolbar (props) {
        //Add the extra styles via containerStyle
        return <InputToolbar {...props}

                             textInputStyle={{ color: "black" }}
                             containerStyle={{backgroundColor:'white',marginLeft:10,borderRadius:20,borderWidth:0,color:'black',marginBottom:0,marginRight:10}} />
    }

    componentDidMount () {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {

               //alert(JSON.stringify(data))
            //    GLOBAL.wallet = data.data.wallet
              this.setState({username:data.data})
            //  this.setState({wallet:data.data.wallet})
              //wallet





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    }




    renderBubble(props) {

        return (
            <View style = {{backgroundColor:'rgba(0,0,0,0.6)',borderRadius:12,marginBottom:6,borderColor:'#979797',borderWidth:1,flexDirection:'row'}}>


                <Text style={{color:'#7BAAED',fontFamily:"Nunito-Bold",fontSize:22,margin:4,marginLeft:8,marginBottom:1}}>{props.currentMessage.user.name} :</Text>


                <Text style={{color:'white',fontFamily:"Nunito-Bold",fontSize:22,margin:4,marginTop:1}}>{props.currentMessage.text}</Text>

            </View>
        );
    }
    componentWillMount() {

    }


    renderMessages = (msg) => {
    //  alert(JSON.stringify(msg.user._id))

let message = msg.currentMessage
var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';

console.log("single message", message)
var a = "rgba(0,0,0,0.8)"
if(message.vip == "0"){
a = "rgba(0,0,0,0.8)"
}else {
  if (message.text == "Left the Live"){
    a = "#55bb33"
  }else {
    a = "#F00B51"
  }

}
return (
  <View style = {{backgroundColor:a,borderRadius:12,marginBottom:6,borderColor:'#979797',borderWidth:0,marginLeft:6,width:window.width - 126}}>

      <View style = {{flexDirection:'row'}}>


                <Text style={{color:'white',fontFamily:'Nunito-Regular',fontSize:12,margin:4,marginLeft:8,padding:1,lineHeight:18,marginBottom:1}}>{message.user.name}: </Text>

  <Text style={{color:'white',fontFamily:'Nunito-Regular',fontSize:12,margin:4,marginLeft:-2,padding:1,lineHeight:18,marginTop:4}}>{message.text} {message.wallet? `🥇${message.wallet}`:""} </Text>




      </View>
      <Text style={{color:'white',fontFamily:'Nunito-Regular',fontSize:9,margin:4,marginLeft:window.width - 180,padding:1,lineHeight:18,marginTop:-10}}>{moment(message.createdAt).format("hh:mm:a")} </Text>


  </View>
)}
renderInputToolbar (props) {
    //Add the extra styles via containerStyle

}
    render() {


        return (
            <GiftedChat
                renderUsernameOnMessage = {false}
                messages={this.state.messages}

                renderBubble={this.renderBubble}
                onSend={message => {
                  // alert(JSON.stringify(this.state.username))
                  if (this.state.username.is_vip_subscribe == 0){
                    Alert.alert('VIP only','Please become a VIP member to use this feature',
                                               [
                                                   {text:"OK"},
                                               ],
                                               {cancelable:false}
                                           )

                    return
                  }

     Backend.sendMessage(message);
  }}


                 renderMessage={(message) => this.renderMessages(message)}





                user={{
                    _id: GLOBAL.user_id,
                    name:  GLOBAL.name

                }}
            />
        );
    }


    componentDidMount() {



      Backend.loadMessages(message => {

        if (message.user_id == store.getState().user.id){
          if (message.length == 0){


          }else {
            this.setState(previousState => {


                return {
                    messages: GiftedChat.append(previousState.messages, message)
                };
            });


          }
        }


        });
    }
    componentWillUnmount() {
        Backend.closeChat();
    }
}
