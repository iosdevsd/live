import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  StatusBar,
  Platform,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend} from '../backend/Api';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import {API_otp, API_signIn, otpGen} from '../backend/Api';
import DeviceInfo from 'react-native-device-info';
import * as actions from '../redux/actions';

const ForgotOtp = ({navigation, route}) => {
  const [states, setStates] = useState({
  loading: false,



});
  const [state, setState] = useState({
    otp: '',
    userOtp: '',
  });
    const toggleLoading = (bol) => setStates({...states, loading: bol});
  const submitHandler = async () => {
    if (state.userOtp == ""){
        Toast.showWithGravity('Please enter Verification Code', Toast.LONG, Toast.CENTER);
    }
    else if (route.params.item.otp != state.userOtp){
              Toast.showWithGravity('Sorry, verification code is incorrect. Please check and try again', Toast.LONG, Toast.CENTER);
      }
      else {
        navigation.navigate('Reset',{item:route.params.item})
      }


    }


  const onResetOtp = () => {
//alert(JSON.stringify(route.params.item.phone))
      toggleLoading(true);

var a = ""
      if (route.params.item.email == ""){
      a = route.params.item.phone
    }else {
        a = route.params.item.email
    }



      const json = {
    username: a,

  };
  console.log(JSON.stringify(json))

  Resend(json)
         .then((data) => {
              toggleLoading(false);

           if (data.status) {
            // alert(JSON.stringify(data))

  var a = `Verification Code sent to ${route.params.item.email == "" ? route.params.item.phone:route.params.item.email }`

  Toast.showWithGravity(a, Toast.LONG, Toast.CENTER);

           } else {
      Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });

  };
  const onPressBack = () => navigation.goBack();
  useEffect(() => {
    console.log(state);
  }, [state]);
  return (
    <SafeAreaView style={styles.container}>
      {states.loading && <Loader />}
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>OTP</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
                source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      {route.params.item.email == "" && (
              <Text style={styles.text_1}>Verification code has been sent to {route.params.item.phone}</Text>
      )}
      {route.params.item.email != "" && (
              <Text style={styles.text_1}>Verification code has been sent to {route.params.item.email}</Text>
      )}

      <Text style={styles.text_2}>

      </Text>

      <OTPInputView
        style={styles.OTPInputView}
        pinCount={4}
        code={state.userOtp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
        onCodeChanged={(text) => setState({...state, userOtp: text})}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={(code) => {
          console.log(`Code is ${code}, you are good to go!`);
        }}
      />
      <TouchableOpacity onPress={onResetOtp}>
        <Text style={styles.text_4}>Resend Verification Code</Text>
      </TouchableOpacity>

      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={submitHandler}>
          <Text style={styles.text_3}>Submit</Text>
        </TouchableOpacity>
      </LinearGradient>
      <Text style={{position:'absolute',bottom:10,textAlign:'center',margin:10,fontFamily:"DMSans-Regular"}}>Kindly check your SPAM or JUNK folder also for the mail with verification Code </Text>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 19,
    lineHeight: 30,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 55,
    marginBottom: 10,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 19,
    color: '#E90B52',
    marginHorizontal: 30,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 15,
    color: '#EA0B52',
    textAlign: 'center',
  },

  OTPInputView: {
    height: 150,
    width: '80%',
    alignSelf: 'center',
  },

  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },

  underlineStyleBase: {
    color: '#344356',
    fontFamily: 'HK Grotesk',
    fontWeight: '700',
    fontSize: 25,
    width: 65,
    height: 65,
    borderWidth: 1,
    borderRadius: 15,
  },

  underlineStyleHighLighted: {
    borderColor: '#cdcdcd',
  },
});
export default ForgotOtp;
