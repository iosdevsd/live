import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import RadialGradient from 'react-native-radial-gradient';
import {imageStyle} from '../style/style';
const AudioChat = ({navigation}) => {
  const [state, setstate] = useState(0);

  const flatListData = [
    {
      id: 1,
      level: 1,
      score: 1125,
      name: 'Shivani Singh',
      source: require('../assets/user/user1.jpg'),
    },
    {
      id: 2,
      level: 1,
      score: 508,
      name: 'Deepika',
      source: require('../assets/user/user5.jpg'),
    },
    {
      id: 3,
      level: 1,
      score: 1335,
      name: 'rabiya singh',
      source: require('../assets/user/user3.jpg'),
    },
    {
      id: 4,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user4.jpg'),
    },
    {
      id: 5,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user5.jpg'),
    },
    {
      id: 6,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user4.jpg'),
    },
    {
      id: 7,
      level: 1,
      score: 1125,
      name: 'Shivani Singh',
      source: require('../assets/user/user1.jpg'),
    },
    {
      id: 8,
      level: 1,
      score: 508,
      name: 'Deepika',
      source: require('../assets/user/user5.jpg'),
    },
    {
      id: 9,
      level: 1,
      score: 1335,
      name: 'rabiya singh',
      source: require('../assets/user/user3.jpg'),
    },
    {
      id: 10,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user4.jpg'),
    },
    {
      id: 11,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user5.jpg'),
    },
    {
      id: 12,
      level: 1,
      score: 7555,
      name: 'kirti sanon',
      source: require('../assets/user/user4.jpg'),
    },
  ];
  const flatListData_2 = [
    {
      id: 1,
      title: 'File Name',
      subTitle:
        'lorem lpsum is simply dummy text of the printing and typesetting industry',
      author: 'Rabiya Rajput',
      duration: '05:00 min',
      coin: '25/min',
      source: require('../assets/te1.png'),
    },
    {
      id: 2,
      title: 'File Name',
      subTitle:
        'lorem lpsum is simply dummy text of the printing and typesetting industry',
      author: 'Rabiya Rajput',
      duration: '05:00 min',
      coin: '25/min',

      source: require('../assets/te.png'),
    },
    {
      id: 3,
      title: 'File Name',
      subTitle:
        'lorem lpsum is simply dummy text of the printing and typesetting industry',
      author: 'Rabiya Rajput',
      duration: '05:00 min',
      coin: '25/min',

      source: require('../assets/te1.png'),
    },
  ];
  const renderItem_2 = ({item}) => (
    <TouchableOpacity key={item.key} style={styles_2.renderView}>
      <Image source={item.source} style={styles_2.renderView_1} />
      <View style={styles_2.renderView_2}>
        <Text style={styles_2.title}>{item.title}</Text>
        <Text style={styles_2.subTitle}>{item.subTitle}</Text>
        <Text style={styles_2.author}>{`by ${item.author}`}</Text>
        <View style={styles_2.renderView_3}>
          <Text style={styles_2.duration}>{item.duration}</Text>
          <Image
            source={require('../assets/coin.png')}
            style={styles_2.coinimage}
          />
          <Text style={styles_2.coin}>{item.coin}</Text>
          <Image
            source={require('../assets/play-button.png')}
            style={styles_2.playbutton}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.flatItem}
      key={item.id}
      onPress={onProfilePress}>
      <ImageBackground source={item.source} style={styles.imageBackground}>
        <View style={styles.flatItemView}>
          <LinearGradient
            colors={['#EB8B84', '#8379F9']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.levelView}>
            <Image
              source={require('../assets/coin.png')}
              style={styles.coinstyle}
            />
            <Text style={styles.levelText}>{` ${item.level} /min`}</Text>
          </LinearGradient>
        </View>
        <View style={styles.flatItemView2}>
          <View style={styles.dot} />
          <Text style={styles.itemName}>{item.name}</Text>
          {state === 1 && (
            <TouchableOpacity style={styles.itemInfoTouch}>
              <Image
                source={require('../assets/infobutton.png')}
                style={styles.itemInfoIcon}
              />
            </TouchableOpacity>
          )}
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );

  const onProfilePress = () => {
    navigation.navigate('AudioDetails', {
      audioType: state === 0 ? 'OneCall' : 'PartyCall',
    });
  };
  return (
    <>
      <View style={styles.selectView}>
        <TouchableOpacity onPress={() => setstate(0)}>
          <RadialGradient
            colors={
              state === 0 ? ['#3499FF', '#3A3985'] : ['#6ee2f580', '#6454f080']
            }
            stops={[0, 1]}
            center={[57.5, 17.5]}
            radius={180}
            style={[styles.linearGradient]}>
            <Text style={styles.selectText}>1-1 Audio</Text>
          </RadialGradient>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setstate(1)}>
          <RadialGradient
            colors={
              state === 1 ? ['#3499FF', '#3A3985'] : ['#6ee2f580', '#6454f080']
            }
            stops={[0, 1]}
            center={[57.5, 17.5]}
            radius={180}
            style={[styles.linearGradient]}>
            <Text style={styles.selectText}>Party Audio</Text>
          </RadialGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setstate(2)}>
          <RadialGradient
            colors={
              state === 2 ? ['#3499FF', '#3A3985'] : ['#6ee2f580', '#6454f080']
            }
            stops={[0, 1]}
            center={[57.5, 17.5]}
            radius={180}
            style={[styles.linearGradient]}>
            <Text style={styles.selectText}>Experiences</Text>
          </RadialGradient>
        </TouchableOpacity>
      </View>

      {[0, 1].includes(state) && (
        <FlatList
          columnWrapperStyle={{
            justifyContent: 'space-evenly',
          }}
          contentContainerStyle={{
            paddingBottom: 20,
            margin: 6,
          }}
          data={flatListData}
          horizontal={false}
          numColumns={3}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          ItemSeparatorComponent={({leadingItem}) => (
            <>
              {leadingItem[0].id === 4 && (
                <Image
                  source={require('../assets/ads.png')}
                  style={imageStyle.bannerImage}
                />
              )}
            </>
          )}
        />
      )}
      {state === 2 && (
        <FlatList
          data={flatListData_2}
          renderItem={renderItem_2}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  selectView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    paddingHorizontal: 10,
  },
  linearGradient: {
    width: 105,
    height: 30,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  selectText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 13,
    color: '#FFFFFF',
  },
  flatItem: {
    flex: 1,
    height: 125,
    margin: 4,
  },
  imageBackground: {
    flex: 1,
    overflow: 'hidden',
    justifyContent: 'space-between',
    borderRadius: 10,
  },
  flatItemView: {
    flexDirection: 'row-reverse',
    margin: 10,
  },
  flatItemView2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemName: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 13,
    color: '#FFFFFF',
    marginLeft: 5,
    marginVertical: 10,
  },
  itemInfoTouch: {
    marginLeft: 'auto',
    padding: 12,
  },
  itemInfoIcon: {
    height: 11,
    width: 11,
    resizeMode: 'contain',
  },
  levelView: {
    backgroundColor: '#4A90E2',
    paddingHorizontal: 5,
    borderRadius: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  levelText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 11,
    color: '#FFFFFF',
  },

  crownIcon2: {
    marginLeft: 'auto',
    height: 30,
    width: 30,
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#43FA00',
    marginVertical: 10,
    marginLeft: 10,
  },
  coinstyle: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
  },
});

const styles_2 = StyleSheet.create({
  renderView: {
    flex: 1,
    flexDirection: 'row',
    height: 100,
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
    marginBottom: 0,
    overflow: 'hidden',
    elevation: 2,
  },
  renderView_1: {
    height: 100,
    width: 100,
  },
  renderView_2: {
    flex: 1,
    paddingHorizontal: 8,
    paddingBottom: 4,
    // backgroundColor: 'green',
  },
  title: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 15,
    color: '#000000',
  },
  subTitle: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 11,
    color: '#000000',
    lineHeight: 16,
    flex: 1,
    overflow: 'scroll',
  },
  renderView_3: {
    flexDirection: 'row',
    marginTop: 'auto',
    marginBottom: 0,
    alignItems: 'center',
  },
  author: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 10,
    color: '#E70A52',
    fontStyle: 'italic',
  },
  duration: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 12,
    color: '#000000',
  },
  coin: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 12,
    color: '#EA0B52',
  },
  coinimage: {
    marginLeft: 15,
    marginRight: 2,
    height: 13,
    width: 13,
    resizeMode: 'contain',
  },
  playbutton: {
    height: 20,
    width: 20,
    marginLeft: 'auto',
    marginRight: 5,
  },
});
export default AudioChat;
