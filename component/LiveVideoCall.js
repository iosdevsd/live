import React, {Component, PureComponent} from 'react'
import {
    StyleSheet, Text, View, TouchableOpacity,
    Dimensions, Modal, NativeModules, Image,Alert,FlatList,ScrollView,ImageBackground,
} from 'react-native'
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import LiveChat from './LiveChat';
import {Stopwatch} from "react-native-stopwatch-timer";
import CountDown from 'react-native-countdown-component';
import RBSheet from "react-native-raw-bottom-sheet";
import Game from './Games.js';
import DigitalClock from './DigitalClock.js';
import Backend from "./Backend.js";
var randomString = require('random-string');
import store from '../redux/store';
import {Surface, ActivityIndicator} from 'react-native-paper';
import Orientation from 'react-native-orientation';
import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
import LinearGradient from 'react-native-linear-gradient';
import {RtcEngine, AgoraView} from 'react-native-agora'
import {LoginOtpApi,SignInApi,Explore,FetchHomeWallet,PujaStart,GetProfileApi,EndLive} from '../backend/Api';
import {APPID} from './settingss'
import { EventRegister } from 'react-native-event-listeners'
const {Agora} = NativeModules
console.log(Agora)
const options = {
    container: {
        backgroundColor: 'transparent',
    },
    text: {
        fontSize: 12,
        color: '#fff',
    }
};
if (!Agora) {
    throw new Error("Agora load failed in react-native, please check ur compiler environments")
}

const {
    FPS30,
    AudioProfileDefault,
    AudioScenarioDefault,
    Host,
    Audience,
    Adaptative
} = Agora

const BtnEndCall = () => require('./btn_endcall.png')
const BtnMute = () => require('./btn_mute.png')
const BtnSwitchCamera = () => require('./btn_switch_camera.png')
const IconMuted = () => require('./icon_muted.png')
var bg = ["1","1","1","1","1","1","1","1","1","1"]
const {width} = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F4F4F4'
    },
    linearGradient: {
      marginHorizontal: 25,
      borderRadius: 25,
      marginTop: 46,
    },
    touchable: {
      flexDirection: 'row',
      padding: 10,
      borderRadius: 25,
      justifyContent: 'center',
    },
    text_3: {
      fontFamily: 'DMSans-Bold',
      fontWeight: '700',
      fontSize: 18,
      color: '#FFFFFF',
    },
    absView: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'space-between',
    },
    videoView: {
        padding: 5,
        flexWrap: 'wrap',
        flexDirection: 'row',
        zIndex: 100
    },
    localView: {
        flex: 1,

    },
    duration: {
      position: 'absolute',
      top: 130,
      left: 0,
      right: 0,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
  },
  absView: {
      position: 'absolute',
      top: 70,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'space-between',
  },
  absViews: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      flex:1,
      height:80,
      justifyContent: 'space-between',
  },
    remoteView: {
        width: (width - 40) / 3,
        height: (width - 40) / 3,
        margin: 5
    },
    bottomView: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
})

class OperateButton extends PureComponent {
    render() {
        const {onPress, source, style, imgStyle = {width: 40, height: 40,resizeMode:'contain',marginTop:6}} = this.props
        return (
            <TouchableOpacity
                style={style}
                onPress={onPress}
                activeOpacity={.7}
            >
                <Image
                    style={imgStyle}
                    source={source}
                />
            </TouchableOpacity>
        )
    }
}

type Props = {
    channelProfile: Number,
    channelName: String,
    clientRole: Number,
    onCancel: Function,
    uid: Number,
}

class LiveVideoCall extends Component<Props> {
  constructor(props) {
   super(props);
    this.state = {
        peerIds: [],
        screen: Dimensions.get('window'),
        joinSucceed: false,
        isMute: false,
        color:'white',
        hideButton: false,
        visible: false,
        ucount:'0',
        username :{},
        wining:false,
        check :false,
        finalq:false,
        fina :'0',
        checkvip:'0',
        selectedUid: undefined,
        animating: true,
        price :'10',
        rbopen:false,
        isMutes:false,
        wining2 :'0',
        roundwining:'',
        connectionState: 'connecting',
        stopwatchStart: false,
        stopwatchReset: false,
        number:'',
        array :[],
        last :[],
        roundbet :'0',
        wallet :'0',

        coin :[
          {
            coin:'10',
            selected:'',
          },
          {
            coin:'20',
            selected:'',
          },
          {
            coin:'30',
            selected:'',
          },
          {
            coin:'40',
            selected:'',
          },
          {
            coin:'50',
            selected:'',
          },
          {
            coin:'60',
            selected:'',
          },
          {
            coin:'70',
            selected:'',
          },

          {
            coin:'80',
            selected:'',
          },
          {
            coin:'90',
            selected:'',
          },
          {
            coin:'100',
            selected:'',
          },

        ],
        remaing:'0',
        width: Dimensions.get('window').width,
   height: Dimensions.get('window').height,
    }
 this.onLayout = this.onLayout.bind(this);
}

onLayout(e) {
  alert(Dimensions.get('window').width)
  this.setState({
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  });
}

getOrientation(){
    if (this.state.screen.width > this.state.screen.height) {
      return 'LANDSCAPE';
    }else {
      return 'PORTRAIT';
    }
  }

  getStyle(){
    if (this.getOrientation() === 'LANDSCAPE') {
      return landscapeStyles;
    } else {
      return portraitStyles;
    }
  }
  onLayout(){
    this.setState({screen: Dimensions.get('window')});
  }

  qwe =() =>{
    const socket = io('http://51.79.250.86:3000', {
    transports: ['websocket']
  })

    socket.on("round_winnings", msg => {
  //alert(JSON.stringify(msg))
  if (msg.status == true){

    var a = msg.count.total_winnings ? msg.count.total_winnings : "0"
  this.setState({fina:msg.count.total_winnings ? msg.count.total_winnings : "0"})

  if (a != "0"){
    this.setState({finalq:true})
this.checkexist(`Won ${msg.count.total_winnings} Coins`)
    setTimeout(()=>{
                  this.setState({finalq:false})
                },6000);
  }

  //this.RBSheet.close()
  //RtcEngine.setClientRole(Host)
  }else{
  //RtcEngine.setClientRole(Audience)
  }

  });

    socket.emit('round_winnings',{
                                    bridge_id:GLOBAL.bookingid,
                                    token:store.getState().user.token,
                                    user_id:store.getState().user.id,
                                    event_id :GLOBAL.event_id

                 })
  }

  weer = () =>{
    //roundbet
    this.setState({roundbet:'0'})
    const socket = io('http://51.79.250.86:3000', {
    transports: ['websocket']
  })

    socket.on("count_user_winnings", msg => {
    //  alert(JSON.stringify(msg.count))
      if(msg.status == true){
        this.setState({wining2:msg.count.total_winnings ? msg.count.total_winnings : "0"})
      }

    })

  socket.emit('count_user_winnings',{
                                  bridge_id:GLOBAL.bookingid,
                                  user_id:store.getState().user.id,


               })
  }
  capture = () =>{
    GetProfileApi({user_id:"1"})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {
               console.log(JSON.stringify(data))
             //alert(JSON.stringify(data))
              GLOBAL.wallet = data.data.wallet
            this.setState({username:data.data})
            this.setState({wallet:data.data.wallet})
            //wallet
            this.setState({checkvip:data.data.is_vip_subscribe})
            if (data.data.is_vip_subscribe == "0"){

              RtcEngine.setClientRole(Audience)
              RtcEngine.enableLocalAudio(false)
            }else {
              RtcEngine.setClientRole(Host)
              RtcEngine.enableLocalAudio(true)
            }




             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  }
  toggleStopwatch = () => {
     this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
 };
 resetStopwatch() {
     this.setState({stopwatchStart: false, stopwatchReset: true});
 }
 getFormattedTime(time) {
     this.currentTime = time;
 }
    componentWillMount () {

       this.setState({stopwatchStart:true})
    //  alert(JSON.stringify(GLOBAL.host))
this.capture()
this.checkexist('Joined the Live 👋')
      this.listener = EventRegister.addEventListener('wallet', (data) => {
           this.setState({
              wallet: data.balan,
           })
           this.setState({
              roundbet: data.amount,
           })
           //roundbet
       })


       this.listenerr = EventRegister.addEventListener('hide', (data) => {
         this.setState({
            rbopen: false,
         })
            //rbopen
            //roundbet
        })
  //  alert(Dimensions.get('window').width)

var dd = []
      for (var i = 0 ; i< 37 ; i++){
        var dict = {
          number :i,
          selected :""
        }
        dd.push(dict)
      }
    //  alert(JSON.stringify(dd))
      this.setState({array:dd})


        // const options = {
        //     appid: 'ef38b64215ed49d2acc3c6d8e20439f4',
        //     channelProfile: 1,
        //     videoProfile: 40,
        //     clientRole: 1,
        //     swapWidthAndHeight: true
        // };
        // RtcEngine.init(options);


        const config = {
          appid: APPID,
          channelProfile: this.props.channelProfile,
          clientRole: this.props.clientRole,
          videoEncoderConfig: {
            width: 1920,
            height: 1080,
              bitrate: 6500,
              frameRate: FPS30,
              orientationMode: Adaptative,
          },
          swapWidthAndHeight:true,
          audioProfile: AudioProfileDefault,
          audioScenario: AudioScenarioDefault
        }
        console.log("[CONFIG]", JSON.stringify(config));
        console.log("[CONFIG.encoderConfig", config.videoEncoderConfig);
        RtcEngine.on('videoSizeChanged', (data) => {
            console.log("[RtcEngine] videoSizeChanged ", data)
        })
        RtcEngine.on('remoteVideoStateChanged', (data) => {
            console.log('[RtcEngine] `remoteVideoStateChanged`', data);
        })
        RtcEngine.on('userJoined', (data) => {
        //  alert(JSON.stringify(data))
            console.log('[RtcEngine] onUserJoined', data);
            const {peerIds} = this.state;
            if (peerIds.indexOf(data.uid) === -1) {
              this.setState({stopwatchStart:true})
              this.setState({connectionState:'connected'})
                this.setState({
                    peerIds: [...peerIds, data.uid]
                })
            }
        })
        RtcEngine.on('userOffline', (data) => {
            console.log('[RtcEngine] onUserOffline', data);
            this.setState({
                peerIds: this.state.peerIds.filter(uid => uid !== data.uid)
            })
            console.log('peerIds', this.state.peerIds, 'data.uid ', data.uid)
        })
        RtcEngine.on('joinChannelSuccess', (data) => {
            console.log('[RtcEngine] onJoinChannelSuccess', data);
            RtcEngine.startPreview().then(_ => {
                this.setState({
                    joinSucceed: true,
                    animating: false
                })
            })
        })
        RtcEngine.on('audioVolumeIndication', (data) => {
            console.log('[RtcEngine] onAudioVolumeIndication', data);
        })
        RtcEngine.on('clientRoleChanged', (data) => {
            console.log("[RtcEngine] onClientRoleChanged", data);
        })
        RtcEngine.on('videoSizeChanged', (data) => {
            console.log("[RtcEngine] videoSizeChanged", data);
        })
        RtcEngine.on('error', (data) => {
            console.log('[RtcEngine] onError', data);
            if (data.error === 17) {
                RtcEngine.leaveChannel().then(_ => {
                    this.setState({
                        joinSucceed: false
                    })
                    const { state, goBack } = this.props.navigation;
                    this.props.onCancel(data);
                    goBack();
                })
            }
        })
        RtcEngine.init(config);
    }

    toggleStopwatch = () => {
       this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
   };
   resetStopwatch() {
       this.setState({stopwatchStart: false, stopwatchReset: true});
   }
   getFormattedTime(time) {
      // this.currentTime = time;
   }

   onPressd = () =>{
     var k = ""
     for (var i = 0 ; i < this.state.array.length; i++){
       if (this.state.array[i].selected == "Y"){
         const socket = io('http://51.79.250.86:3000', {
         transports: ['websocket']
       })



  var c = {

                                  winning_numbers:this.state.array[i].number,
                                  user_id:store.getState().user.id,
                                  bridge_id:GLOBAL.bookingid
               }

               console.log(c)

       // socket.emit('submit_winning_number',{
       //
       //                                 winning_numbers:this.state.array[i].number,
       //                                 user_id:store.getState().user.id,
       //                                 bridge_id:GLOBAL.bookingid,
       //                                 token:store.getState().token
       //              })

       }
     }


     //GLOBAL
   }


   navigateToScreen1 = () => {
this.checkexist('Left the Live')
this.props.navigation.goBack()




   }

   onstart = () =>{

     const socket = io('http://51.79.250.86:3000', {
     transports: ['websocket']
   })
   socket.on("start_game", msg => {

if (msg.status == true){
//RtcEngine.setClientRole(Host)
}else{
//RtcEngine.setClientRole(Audience)
}

});
   socket.emit('start_game',{

                                   user_id:store.getState().user.id,
                                   token:store.getState().token,
                                   bridge_id:GLOBAL.bookingid
                })
   }



   check = (item,index) =>{


   var b = this.state.vip[index]
   if (b.is_mute == "0"){
     b.is_mute = "1"
   }else {
     b.is_mute = "0"
   }
   this.state.vip[index] = b
   this.setState({vip:this.state.vip})
   const socket = io('http://51.79.250.86:3000', {
   transports: ['websocket']
   })
   socket.emit('toggle_mute_user',{
                                     id:item.id,
                                   user_id:store.getState().user.id,
                                   bridge_id:GLOBAL.bookingid,
                                   token:store.getState().token
                })
   }



    componentDidMount () {
     Orientation.lockToLandscape();
    //  alert(window.width)
      const socket = io('http://51.79.250.86:3000', {
      transports: ['websocket']
    })

  this.weer()

  socket.on("is_mute", msg => {

  if (msg.status == true){
    if (this.state.checkvip == "1"){
      RtcEngine.enableLocalAudio(true)
    }

  //RtcEngine.setClientRole(Host)
  }else{
    if (this.state.checkvip == "1"){
        RtcEngine.enableLocalAudio(false)
    }
  //RtcEngine.setClientRole(Audience)
  }

  });
  socket.emit('is_mute',{
                               bridge_id:GLOBAL.bookingid,
                               user_id:store.getState().user.id



            })

  socket.on("host_blocked", msg => {
  //alert(JSON.stringify(msg))
  if (msg.status == true){
  this.props.navigation.goBack()
  //RtcEngine.setClientRole(Host)
  }else{
  //RtcEngine.setClientRole(Audience)
  }

  });
  socket.emit('host_blocked',{
                               bridge_id:GLOBAL.bookingid,
                               host_id:GLOBAL.host.id



            })




    socket.on("end_broadcast_user", msg => {
//alert(JSON.stringify(msg))
 if (msg.status == true){
   this.props.navigation.goBack()
 //RtcEngine.setClientRole(Host)
 }else{
 //RtcEngine.setClientRole(Audience)
 }

 });
 socket.emit('end_broadcast_user',{
                                 bridge_id:GLOBAL.bookingid,



              })


              socket.on("winning_numbers_list", msg => {

                    this.setState({last:msg.data})
                 // this.props.navigation.goBack()
                //RtcEngine.setClientRole(Host)


           });
           socket.emit('winning_numbers_list',{
                                           bridge_id:GLOBAL.bookingid,



                        })


    socket.on("submit_winning_number", msg => {

if (msg.status == true){
  //this.RBSheet.close()
//RtcEngine.setClientRole(Host)
}else{
//RtcEngine.setClientRole(Audience)
}

});

socket.on("join_broadcast", msg => {

if (msg.status == true){

  //ucount
  this.setState({ucount:msg.data.data.length})
//this.RBSheet.close()
//RtcEngine.setClientRole(Host)
}else{
//RtcEngine.setClientRole(Audience)
}

});

socket.on("leave_broadcast", msg => {

if (msg.status == true){
    this.setState({ucount:msg.data.data.length})
//this.RBSheet.close()
//RtcEngine.setClientRole(Host)
}else{
//RtcEngine.setClientRole(Audience)
}

});

socket.on("live_game_status", msg => {

if (msg.status == true){
  var a = msg.data.remaining_seconds
GLOBAL.event_id = msg.data.id
  this.setState({remaing:a})

  this.setState({wining:false})




//RtcEngine.setClientRole(Host)
}else{
  this.setState({remaing:'0'})
//RtcEngine.setClientRole(Audience)
}

});

socket.emit('live_game_status',{
                                bridge_id:GLOBAL.bookingid,
                                token:store.getState().user.token,
                                user_id:store.getState().user.id,
                                host_id :GLOBAL.host.id

             })

             socket.emit('join_broadcast',{
                                             bridge_id:GLOBAL.bookingid,
                                             token:store.getState().user.token,
                                             user_id:store.getState().user.id,
                                             broadcast_id :GLOBAL.host.id

                          })

             socket.on("declare_winner", msg => {

             if (msg.status == true){
               this.capture ()
               GLOBAL.coin = "10"

this.weer()
this.qwe()





this.setState({number:msg.winning_number})
               this.setState({wining:true})
               setTimeout(()=>{
                             this.setState({wining:false})
                           },15000);
               this.setState({last:msg.last_wins_numbers})
               //last
             //RtcEngine.setClientRole(Host)
             }else{
               this.setState({remaing:'0'})
             //RtcEngine.setClientRole(Audience)
             }

             });

             socket.emit('declare_winner',{
                                             bridge_id:GLOBAL.bookingid,
                                             token:store.getState().user.token,
                                             user_id:store.getState().user.id,
                                             host_id :GLOBAL.host.id

                          })

        if (GLOBAL.user_id != "0"){
      // this.getlog()
      }

        RtcEngine.getSdkVersion((version) => {
            console.log('[RtcEngine] getSdkVersion', version);
        })

        console.log('[joinChannel] ' + this.props.channelName);
        RtcEngine.joinChannel(this.props.channelName, this.props.uid)
            .then(result => {
                /**
                 * ADD the code snippet after join channel success.
                 */
            });
        RtcEngine.enableAudioVolumeIndication(500, 3,true)

    }

    shouldComponentUpdate(nextProps) {
        return nextProps.navigation.isFocused();
    }
    checkexist = (item)=>{
      if (typeof store.getState().user.id != "undefined"){
        var k = ""

     k =   item


     var x = randomString({
         length: 20,
         numeric: true,
         letters: true,
         special: false,
         exclude: ['a', 'b']
     });

     var array = [];
     var users = {
         _id: store.getState().user.id,
         name: GLOBAL.mydata.name ,
     }
     var today = new Date();
     /* today.setDate(today.getDate() - 30);
     var timestamp = new Date(today).toISOString(); */
     var timestamp = today.toISOString();
     var dict = {
         text:k,
         user: users,
         createdAt: timestamp,
         vip:GLOBAL.mydata.is_vip_subscribe,
         _id: x,
         wallet:GLOBAL.wallet


         // etc.
     };
     array.push(dict)
     //Backend.load()

     Backend.sendMessage12(array)
      }
    }

    componentWillUnmount () {


      socket.emit('leave_broadcast',{
                                      bridge_id:GLOBAL.bookingid,
                                      token:store.getState().user.token,
                                      user_id:store.getState().user.id,
                                      broadcast_id :GLOBAL.host.id

                   })
Orientation.lockToPortrait()
        if (this.state.joinSucceed) {
            RtcEngine.leaveChannel().then(res => {
                RtcEngine.destroy()
            }).catch(err => {
                RtcEngine.destroy()
                console.log("leave channel failed", err);
            })
        } else {
            RtcEngine.destroy()
        }
    }

    handleCancel = () => {



        const { goBack } = this.props.navigation;
        RtcEngine.leaveChannel().then(_ => {
            this.setState({
                joinSucceed: false
            })

        }).catch(err => {
            console.log("[agora]: err", err)
        })


      //   const url = 'http://139.59.76.223/shaktipeeth/api/force_booking_done_complete_online'
      // fetch(url, {
      // method: 'POST',
      // headers: {
      //  'HTTP_X_API_KEY': 'ShaktipeethAUTH@##@17$',
      //  'Content-Type': 'application/json',
      // },
      // body: JSON.stringify({
      //  booking_id: GLOBAL.booking_id,
      //  from:"priest"
      //
      // }),
      // }).then((response) => response.json())
      // .then((responseJson) => {
      //
      //
      // if (responseJson.status == true) {
      //   // this.props
      //   //     .navigation
      //   //     .dispatch(StackActions.reset({
      //   //         index: 0,
      //   //         actions: [
      //   //             NavigationActions.navigate({
      //   //                 routeName: 'DrawerNavigator',
      //   //                 params: { someParams: 'parameters goes here...' },
      //   //             }),
      //   //         ],
      //   //     }))
      // } else {
      // }
      // })
      // .catch((error) => {
      //
      // console.error(error);
      // });
    }

    switchCamera = () => {
        RtcEngine.switchCamera();
    }

    toggleAllRemoteAudioStreams = () => {
        this.setState({
            isMute: !this.state.isMute
        }, () => {
            RtcEngine.muteAllRemoteAudioStreams(this.state.isMute).then(_ => {
                /**
                 * ADD the code snippet after muteAllRemoteAudioStreams success.
                 */
            })
        })
    }

    toggleAllRemoteAudioStreamss = () => {
        this.setState({
            isMutes: !this.state.isMutes
        }, () => {
            RtcEngine.setEnableSpeakerphone(this.state.isMutes).then(_ => {
                /**
                 * ADD the code snippet after muteAllRemoteAudioStreams success.
                 */
            })
        })
    }

    toggleHideButtons = () => {
        this.setState({
            hideButton: !this.state.hideButton
        })
    }


evening = (item,index) =>{
  // for (var i = 0 ; i<this.state.evening.length ; i++){
  //   var ee = this.state.evening[i]
  //   ee.isSelected = ""
  //   this.state.evening[i] = ee
  //   this.setState({evening:this.state.evening})
  // }

this.setState({isChecked:false})
  for (var i = 0 ; i<this.state.coin.length ; i++){
    var ee = this.state.coin[i]
    ee.selected = ""
    this.state.coin[i] = ee
    this.setState({coin:this.state.coin})
  }



  var k = this.state.coin[index]
  if (k.selected == ""){
    k.selected = "Y"
GLOBAL.coin = k.coin


  }else{
    k.selected = ""

  }
  this.state.coin[index] = k
  this.setState({coin:this.state.coin})
}


renderItemTimeSlotes=({item, index}) => {

  var a = "red"
const beasts = [1, 3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36];

  if (parseInt(item.winning_numbers)  == 0){
    a = "green"
  }
  else if (parseInt(item.winning_numbers) == 1  || parseInt(item.winning_numbers) == 3  || parseInt(item.winning_numbers) == 5 || parseInt(item.winning_numbers) == 7 || parseInt(item.winning_numbers) == 9 || parseInt(item.winning_numbers) == 14
|| parseInt(item.winning_numbers) == 12 || parseInt(item.winning_numbers) == 16 || parseInt(item.winning_numbers) == 18 || parseInt(item.winning_numbers) == 19
|| parseInt(item.winning_numbers) == 21 || parseInt(item.winning_numbers) == 23 || parseInt(item.winning_numbers) == 25
|| parseInt(item.winning_numbers) == 27 || parseInt(item.winning_numbers) == 30 || parseInt(item.winning_numbers) == 32
|| parseInt(item.winning_numbers) == 34 || parseInt(item.winning_numbers) == 36){
    a = "red"
  }else {
    a = "black"
  }
    // alert(JSON.stringify(item))
return(


    <View style={{flexDirection:'column',width:40,margin:0}}>



<View style={{backgroundColor:a,height:32}}>

<Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',textAlign:'center',marginTop:10}}>{item.winning_numbers}</Text>
</View>





    </View>





)
}
    renderItemTimeSlote=({item, index}) => {
        // alert(JSON.stringify(item))
    return(

    <TouchableOpacity onPress= {()=>this.evening(item,index)}>
        <View style={{flexDirection:'column',width:32,margin:1}}>

        <View style={{flexDirection:'row',justifyContent:'center',alignSelf:'center',marginTop:2,flex:1}}>
    {item.selected == "" && (
    <View style={{flex:1,borderRadius:3,backgroundColor:'#73005C',flexDirection:'row',height:20}}>
    <Image
        source={require('../resources/cr.png')}
        style={{width: 9, height: 9,marginLeft:2,marginTop:4,resizeMode:'contain'}}

    />
    <Text style={{fontSize:8,fontFamily:'DMSans-Regular',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',marginLeft:0,marginTop:2,textAlign:'right',width:16}}>{item.coin}</Text>
    </View>
    )}
    {item.selected != "" && (
      <View style={{flex:1,borderRadius:3,backgroundColor:'green',flexDirection:'row',height:20}}>
      <Image
          source={require('../resources/cr.png')}
          style={{width: 9, height: 9,marginLeft:2,marginTop:4,resizeMode:'contain'}}

      />
      <Text style={{fontSize:8,fontFamily:'DMSans-Regular',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',marginLeft:0,marginTop:2,textAlign:'right',width:16}}>{item.coin}</Text>
      </View>
    )}


        </View>
        </View>
        </TouchableOpacity>




    )
    }

    cde = () =>{
      EventRegister.emit('reset', "1")
      this.capture()
      this.setState({
          roundbet: '0'
      })

    }
    onPressVideo = (uid) => {
        this.setState({
            selectedUid: uid
        }, () => {
            this.setState({
                visible: true
            })
        })
    }
//setEnableSpeakerphone
    toolBar = ({hideButton, isMute}) => {
        if (!hideButton) {
            return (
                <View>
                    <View style={styles.bottomView}>



                        <OperateButton
                            onPress={this.toggleAllRemoteAudioStreams}
                            source={isMute ? IconMuted() : BtnMute()}
                        />

                        <OperateButton
                            onPress={this.switchCamera}
                            source={BtnSwitchCamera()}
                        />
                    </View>
                </View>)
        }
    }

    agoraPeerViews = ({visible, peerIds}) => {
        return (visible ?
            <View style={styles.videoView} /> :
            <View style={styles.videoView}>{
                peerIds.map((uid, key) => (
                    <TouchableOpacity
                        activeOpacity={1}

                        key={key}>
                        {/*               <Text>uid: {uid}</Text>*/}
                        <AgoraView
                            mode={1}
                            style={styles.remoteView}
                            zOrderMediaOverlay={true}
                            showLocalVideo={true}
                        />
                    </TouchableOpacity>
                ))
            }</View>)
    }

    cdew = () =>{
      if (this.state.username.is_vip_subscribe == 0){
        Alert.alert('VIP only','Please become a VIP member to use this feature',
                                   [
                                       {text:"OK"},
                                   ],
                                   {cancelable:false}
                               )

        return
      }
      //rbopen
this.setState({rbopen:true})
      //this.RBSheet.open()
    }

    selectedView = ({visible}) => {
        return (
            <Modal
                visible={visible}
                presentationStyle={'fullScreen'}
                animationType={'slide'}
                onRequestClose={() => {}}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 1}}
                    onPress={() => this.setState({
                        visible: false
                    })} >
                    <AgoraView
                        mode={1}
                        style={{flex: 1}}
                        zOrderMediaOverlay={true}
                        remoteUid={this.state.selectedUid}
                    />
                </TouchableOpacity>
            </Modal>)
    }

    vty = () =>{
      this.setState({rbopen:false})
    }

    render () {

      var a = "red"
      const beasts = [1, 3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36];

      if (parseInt(this.state.number)  == 0){
        a = "green"
      }
      else if (parseInt(this.state.number) == 1  || parseInt(this.state.number) == 3  || parseInt(this.state.number) == 5 || parseInt(this.state.number) == 7 || parseInt(this.state.number) == 9 || parseInt(this.state.number) == 14
      || parseInt(this.state.number) == 12 || parseInt(this.state.number) == 16 || parseInt(this.state.number) == 18 || parseInt(this.state.number) == 19
      || parseInt(this.state.number) == 21 || parseInt(this.state.number) == 23 || parseInt(this.state.number) == 25
      || parseInt(this.state.number) == 27 || parseInt(this.state.number) == 30 || parseInt(this.state.number) == 32
      || parseInt(this.state.number) == 34 || parseInt(this.state.number) == 36){
        a = "red"
      }else {
        a = "black"
      }
    // alert(this.state.peerIds)
        // if (!this.state.joinSucceed) {
        //     return (
        //         <View style={{flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center'}}>
        //             <ActivityIndicator animating={this.state.animating} />
        //         </View>
        //     )
        // }



        return (
            <Surface
                activeOpacity={1}
                onPress={this.toggleHideButtons}
                style={styles.container}
                onLayout={this.onLayout}
            >

               <AgoraView style={styles.localView} remoteUid={parseInt(GLOBAL.host.id)}showLocalVideo={false} mode={1} />




                <View style = {styles.absViews}>


                <View style = {{flexDirection:'row',justifyContent:'space-between',marginTop:30}}>
  <View style = {{backgroundColor:'grey',borderRadius:16}}>
                <View style = {{flexDirection:'row'}}>
                <Image   source={{uri:GLOBAL.host.imageUrl}}
                         style  = {{width:30, height:30,borderRadius:15,borderWidth:2,borderColor:'white',marginLeft:5
                       }}/>

                <Text style={{fontFamily:'DMSans-Bold',fontSize:16,marginTop:2,color:'white',textAlign:'left',marginLeft:-32}}>
           {GLOBAL.host.name}

         </Text>



                </View>
                <View style = {{flexDirection:'row',marginLeft:37,marginTop:-10}}>
                <Image       source={require('../resources/clock.png')}
                         style  = {{width:10, height:10,marginLeft:5,marginTop:4,marginRight:4
                       }}/>

     <DigitalClock />
  <Image       source={require('../resources/userq.png')}
           style  = {{width:10, height:10,marginLeft:5,marginTop:4,marginRight:4
         }}/>
  <Text style={{fontFamily:'DMSans-Bold',fontSize:12,marginTop:2,color:'white',textAlign:'left',marginRight:10}}>
{this.state.ucount}
  </Text>



                </View>
  </View>
  <TouchableOpacity style = {{width: 30,zIndex:5, height: 30,marginRight:12}}
               activeOpacity ={0.99} onPress= {()=>this.navigateToScreen1()}>
  <Image
               source={require('../resources/cross.png')}
               style={{width: 30, height: 30,resizeMode:'contain'}}


           />
  </TouchableOpacity>

                </View>



</View>
{this.state.remaing == "0" && (

<View style = {{position:'absolute',bottom:50,left:15,borderWidth:2,borderColor:'#F00B51'}}>
 <AgoraView style={{width:200,height:200,zIndex:1}} remoteUid={`100${GLOBAL.host.id}100`}showLocalVideo={false} mode={1} />

</View>
)}


{this.state.remaing != "0" && (

<View style = {{position:'absolute',left:0,width:'100%',backgroundColor:'rgba(0,0,0,0.4)',top:0,height:40}}>
<Text style={{fontFamily:'DMSans-Bold',fontSize:16,marginTop:12,color:'white',textAlign:'center'}}>
  Place Your Bets

</Text>
</View>
)}

{this.state.wining == true && (

<View style = {{position:'absolute',left:0,width:'100%',top:0,height:40}}>
<Text style={{fontFamily:'DMSans-Bold',fontSize:16,marginTop:12,color:'white',textAlign:'center'}}>
  Winning Number :
<Text style={{fontFamily:'DMSans-Bold',fontSize:16,marginTop:13,marginLeft:3,color:a}}>
 &nbsp;{this.state.number}

</Text>

</Text>

</View>
)}

{this.state.finalq == true && (
  <View style ={{position:'absolute',top:40,width:300,height:200,alignSelf:'center'}}>
 <ImageBackground
     source={require('../resources/win-pop.png')}
     imageStyle ={{resizeMode:'contain'}}
     style={{width: 300, height: 200,marginLeft:0,marginTop:4,resizeMode:'contain',alignSelf:'center'}}


 >

<Text style = {{alignSelf:'center',fontFamily:'DMSans-Bold',fontSize:14,color:'white',marginTop:120}}>

You have won {this.state.fina} Coins.


</Text>

 </ImageBackground>

 </View>
)}


{this.state.remaing != "0" && (
  <View style = {{position:'absolute',left:0,width:60,top:70,height:300}}>
  <FlatList  style={{width:'100%'}}
              data={this.state.coin}

              showsVerticalScrollIndicator={false}
              keyExtractor={this._keyExtractor}
              renderItem={this.renderItemTimeSlote}
              extraData = {this.state}
           />
  </View>
)}



<View style = {{position:'absolute',right:0,width:50,top:70,height:'64%'}}>
<FlatList  style={{width:'100%'}}
            data={this.state.last}

            showsVerticalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            renderItem={this.renderItemTimeSlotes}
            extraData = {this.state}
         />
</View>




  <View style = {{position:'absolute',left:0,width:'100%',bottom:0,height:50}}>
  <LinearGradient
    colors={['#73005C', '#F00B51']}
    start={{x: 0, y: 0}}
    end={{x: 1, y: 1}}
    style={{height:50,width:'100%',borderTopLeftRadius:42,borderTopRightRadius:42}}>
 <View style = {{flexDirection:'row'}}>
 {GLOBAL.is_premium == "1" && (
      <Image       source={require('../resources/vip.png')}
               style  = {{width:30, height:30,marginLeft:25,marginTop:9,marginRight:4
             }}/>
    )}
    <View style = {{marginLeft:10,marginTop:8,marginLeft:8}}>
    <Text style={{fontFamily:'DMSans-Bold',fontSize:16,color:'white'}}>
       Balance

    </Text>


    <View style = {{flexDirection:'row',marginLeft:0}}>
    <Image
        source={require('../resources/cr.png')}
        style={{width: 20, height: 20,marginLeft:8,resizeMode:'contain'}}


    />
    <Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',marginLeft:7,marginTop:3}}>{this.state.wallet}</Text>
    </View>
    </View>
    <TouchableOpacity
              activeOpacity ={0.99} onPress= {()=>this.cdew()}>
    <Image
        source={require('../resources/chatq.png')}
        style={{width: 20, height: 20,marginLeft:20,marginTop:20,marginLeft:20,resizeMode:'contain'}}


    />
    </TouchableOpacity>
    </View>

    {this.state.remaing != "0" && (
      <View style = {{alignSelf:'center',marginTop:-36,flexDirection:'row'}}>
        <Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',marginTop:10}}>Remaining Time:</Text>
        <View style = {{marginTop:1,flexDirection:'row'}}>
        <CountDown
               until={parseInt(this.state.remaing)}
               onFinish={() => this.setState({check:false})}
               digitStyle={{backgroundColor: 'transparent'}}
           digitTxtStyle={{color: 'white'}}
           size = {11}
           timeToShow={[ 'S']}
           timeLabels={{s: 'sec'}}
             />

               <Text style={{fontSize:12,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'justify',lineHeight:14,color:'white',marginLeft:2,marginTop:8}}>sec</Text>
               </View>

             </View>

    )}


    <View style = {{alignSelf:'flex-end',marginRight:30}}>




      <View style ={{flexDirection:'row',marginTop:-32,alignSelf:'center'}}>
      <View style ={{flexDirection:'row'}}>
      {this.state.remaing != "0" && (
      <TouchableOpacity
            activeOpacity ={0.99} onPress= {()=>this.cde()}>
      <Image
          source={require('../resources/reset.png')}
          style={{width: 20, height: 20,marginLeft:0,marginRight:12,resizeMode:'contain'}}


      />
      </TouchableOpacity>
    )}

      <Image
          source={require('../resources/cr.png')}
          style={{width: 20, height: 20,marginLeft:8,resizeMode:'contain'}}


      />
      <Text style={{fontSize:14,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',lineHeight:14,color:'white',marginTop:3,marginLeft:4}}>{this.state.roundbet}</Text>

      </View>
      <View style ={{flexDirection:'row'}}>
      <Image
          source={require('../resources/crown.png')}
          style={{width: 20, height: 20,marginLeft:8,resizeMode:'contain'}}


      />
      <Text style={{fontSize:14,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',lineHeight:14,color:'white',marginTop:3,marginLeft:4}}>{this.state.wining2}</Text>

      </View>
      </View>

    </View>








    </LinearGradient>
  </View>






{this.state.remaing != "0" && (
  <View style = {{position:'absolute',left:30,bottom:40,top:52,width:'94%',backgroundColor:'transparent'}}>

  <Game/>
  </View>
)}


{this.state.rbopen == true && (
  <View style = {{position:'absolute',left:30,bottom:60,width:'94%',backgroundColor:'transparent',height:'70%'}}>

  <LiveChat  />
  </View>
)}


<RBSheet
ref={ref => {
            this.RBSheet = ref;
          }}
closeOnDragDown={true}
height={window.height







}
openDuration={250}
customStyles={{
container: {
justifyContent: "center",
alignItems: "center"
}
}}
>
<LiveChat />
</RBSheet>


            </Surface>
        )
    }
}

export default function AgoraRTCViewContainer(props) {
  const  navigation  = props.route.params
  //alert(JSON.stringify(props.route.params))

    const channelProfile = navigation.channelProfile
    const clientRole = navigation.clientRole
    const channelName = navigation.channelName
    const uid = navigation.uid
    const onCancel = navigation.onCancel

    return (<LiveVideoCall
        channelProfile={channelProfile}
        channelName={channelName}
        clientRole={clientRole}
        uid={uid}
        onCancel={onCancel}
        {...props}
    ></LiveVideoCall>)
}
