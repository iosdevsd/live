import React from 'react';
import {Image, StyleSheet, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const simpleHeader = (title) => (
  <LinearGradient
    colors={['#73005C', '#F00B51']}
    start={{x: 0, y: 0}}
    end={{x: 1, y: 1}}
    style={styles.header}>
    <Text style={styles.title}>{title}</Text>
  </LinearGradient>
);
export const hotGif = () => (
  <LinearGradient
    colors={['#FAD961', '#F76B1C']}
    start={{x: 0, y: 0}}
    end={{x: 0, y: 1}}
    style={styles.hotGifView}>
    <Text style={styles.hotGifText}>Hot</Text>
    <Image source={require('../resources/fire.gif')} style={styles.hotGif} />
  </LinearGradient>
);
const styles = StyleSheet.create({
  title: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 18,
    color: '#FFFFFF',
  },
  header: {
    flex: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: 88,
    padding: 11,
  },
  hotGifText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
    paddingLeft: 10,
  },
  hotGifView: {
    borderRadius: 9,
    paddingHorizontal: 7,
    paddingVertical: 1,
  },
  hotGif: {
    height: 25,
    width: 17,
    resizeMode: 'contain',
    position: 'absolute',
    top: -10,
    left: -3,
  },
});
