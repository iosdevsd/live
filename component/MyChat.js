import React, {Component} from 'react';
import { StyleSheet, Text, View, Button,SafeAreaView,StatusBarLight,Dimensions,Image,Animated,TouchableOpacity,Alert,ImageBackground,Clipboard,PermissionsAndroid,Platform } from 'react-native';
import Backend from "./Backend.js";
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-controls';
import { GiftedChat,Time,Send,InputToolbar } from "react-native-gifted-chat";
import ImagePicker from 'react-native-image-picker';
import Orientation from 'react-native-orientation';
import Bubble from "react-native-gifted-chat/lib/Bubble";
import ActionSheet from 'react-native-actionsheet';
import Lightbox from 'react-native-lightbox-v2';
import { createThumbnail } from "react-native-create-thumbnail"
import {LoginOtpApi,SignInApi,Explore,FetchHomeWallet,PujaStart,GetProfileApi,SendMessage,Unsseen} from '../backend/Api';
import { EventRegister } from 'react-native-event-listeners';
import CountDown from 'react-native-countdown-component';
import {Stopwatch} from "react-native-stopwatch-timer";
import store from '../redux/store';
var randomString = require('random-string');
import convertToProxyURL from 'react-native-video-cache';
import Sound from 'react-native-sound';
import {AudioPlayer} from 'react-native-simple-audio-player';
import AudioRecord from 'react-native-audio-record';
import io from 'socket.io-client';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
const options2 = {
    container: {
        backgroundColor: 'transparent',
    },
    text: {
        fontSize: 12,
        color: '#fff',
    }
};
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
var randomString = require('random-string');
const GLOBAL = require('./Global');
const options3 = {
  title: 'Select video',
   mediaType: 'video',
  path:'video',
  quality: 1
};
const options = {
    title: 'Select Document',
    maxWidth:300,
    maxHeight:500,

    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
const window = Dimensions.get('window');
type Props = {};

export default class MyChat extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Chat Consulation',
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  constructor(props) {
    super(props);

    this.state = {
        modalVisible: false,
        recognized: '',
        started: '',
        text :'',
        mystatus:false,
        results: [],
        messages: [],
        texts:'',
        audioFile: '',
recording: false,
loaded: false,
paused: true,
stopwatchStart: false,
stopwatchReset: false,

    };
  }

  // renderBubble(props) {
  //
  //     return (
  //         <View>
  //             <Text style={{color:'black'}}>{props.currentMessage.user.name}</Text>
  //         </View>
  //     );
  // }

  showActionSheet= ()=>{
this.showActionSheet24()

  }

  showActionSheet24 = () => {
   this.ActionSheet.show()
 }

  renderActions=() =>{
        return(
            <TouchableOpacity onPress={()=>this.showActionSheet()}>
                <Image style={{width:18, height:18, resizeMode:'contain', marginLeft:9, marginBottom:16}}
                       source={require('./attachement.png')}/>
            </TouchableOpacity>
        )
    }

    uploadVideo = (image,uri) => {


            const url =
                  'http://51.79.250.86/roulette/api/upload_media';
                const data = new FormData();
                data.append('image', {
                  uri: uri ,
                  type: 'video/mp4', // or photo.type
                  name: 'ccdcd.mp4',
                });
                fetch(url, {
                  method: 'post',
                  body: data,
                  headers: {
                    'Content-Type': 'multipart/form-data',
                  },
                })
                  .then(response => response.json())
                               .then((responseJson) => {
                                   //       this.hideLoading()

                              var x = randomString({
                                            length: 20,
                                            numeric: true,
                                            letters: true,
                                            special: false,
                                            exclude: ['a', 'b']
                                        });

                                        var array = [];
      var users = {
        _id: store.getState().user.id,
          name: GLOBAL.mydata.name
                                        }
                                        var today = new Date();
                                        /* today.setDate(today.getDate() - 30);
                                        var timestamp = new Date(today).toISOString(); */
                                        var timestamp = today.toISOString();
                                        var dict = {
                                            text: 'Video',
                                            user: users,
                                            createdAt: timestamp,
                                            _id: x,
                                            image: '',
                                            image1: image,
                                            video:responseJson.media,
                                            userdelete:'',
                                            broaddelete:'',
                           // etc.
                                        };
                                        array.push(dict)
      var c = {receiver_id:GLOBAL.host.id,sender_id:GLOBAL.user_id,chatgid:GLOBAL.chatid,message:"",have_media:"1"}
      console.log(JSON.stringify(c))
      const socket = io('http://51.79.250.86:3000', {
        transports: ['websocket']
      })
      socket.emit('send_message',c)

                         
                                        Backend.chatendMessage(array)

                               }

                             );
    }
    uploadtoo = () =>{

         const options2 = {
     title: 'Select video',
      mediaType: 'video',
     path:'video',
     quality: 1
   };


               ImagePicker.showImagePicker(options2, (response) => {
                     console.log('Response = ', response);

                     if (response.didCancel) {
                         console.log('User cancelled image picker');
                     } else if (response.error) {
                         console.log('ImagePicker Error: ', response.error);
                     } else if (response.customButton) {
                         console.log('User tapped custom button: ', response.customButton);
                     } else {
                         const source = { uri: response.uri };
         createThumbnail({
           url: response.uri,
           timeStamp: 10000,
         })
           .then(response => {
                   const url =
                         'http://51.79.250.86/roulette/api/upload_media';
                       const data = new FormData();

                       // you can append anyone.
                       data.append('image', {
                         uri: response.path,
                         type: 'image/jpeg', // or photo.type
                          name: 'image.png'
                       });
                       fetch(url, {
                         method: 'post',
                         body: data,
                         headers: {
                           'Content-Type': 'multipart/form-data',
                         },
                       })
                         .then(response => response.json())
                                      .then((responseJson) => {
                                          //       this.hideLoading()


             if (responseJson.status == true){
               var image = responseJson.media
               this.uploadVideo(responseJson.media,source.uri)
             }

             console.log("harami" + JSON.stringify(responseJson))





                                      }

                                    );

           })
           .catch(err => console.log({ err }))












                     }
                 });
       }

  componentWillMount() {
    if (Platform.OS === 'android') {
   try {
     const grants =  PermissionsAndroid.requestMultiple([
       PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
       PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
       PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
     ]);

     console.log('write external stroage', grants);

     if (
       grants['android.permission.WRITE_EXTERNAL_STORAGE'] ===
         PermissionsAndroid.RESULTS.GRANTED &&
       grants['android.permission.READ_EXTERNAL_STORAGE'] ===
         PermissionsAndroid.RESULTS.GRANTED &&
       grants['android.permission.RECORD_AUDIO'] ===
         PermissionsAndroid.RESULTS.GRANTED
     ) {
       console.log('permissions granted');
     } else {
       console.log('All required permissions not granted');
       return;
     }
   } catch (err) {
     console.warn(err);
     return;
   }
 }
 const options = {
   sampleRate: 16000,
   channels: 1,
   bitsPerSample: 16,
   wavFile: 'test.wav'
 };

 AudioRecord.init(options);

    // const url = GLOBAL.BASE_URL + 'chat_read';

    // fetch(url, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     user_id: GLOBAL.user_id,
    //     chat_group_id: GLOBAL.bookingid,
    //   }),
    // })
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     if (responseJson.status == true) {
    //     } else {
    //     }
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  }

  videoError = (error) =>{
    alert(JSON.stringify(error))
  }

  uploadto = () =>{
            ImagePicker.showImagePicker(options, (response) => {
                  console.log('Response = ', response);
                  if (response.didCancel) {
                      console.log('User cancelled image picker');
                  } else if (response.error) {
                      console.log('ImagePicker Error: ', response.error);
                  } else if (response.customButton) {
                      console.log('User tapped custom button: ', response.customButton);
                  } else {
                      const source = { uri: response.uri };
                      const url = 'http://51.79.250.86/roulette/api/upload_media'
                      const data = new FormData();
               
                      data.append('image', {
                          uri:response.uri,
                          type: 'image/jpeg', // or photo.type
                          name: 'image.png'
                      });
                      fetch(url, {
                          method: 'post',
                          body: data,
                          headers: {
                              'Content-Type': 'multipart/form-data',
                          }

                      }).then((response) => response.json())
                          .then((responseJson) => {

        console.log(JSON.stringify(responseJson.path))



                             var x = randomString({
                                            length: 20,
                                            numeric: true,
                                            letters: true,
                                            special: false,
                                            exclude: ['a', 'b']
                                        });

                                        var array = [];
                                        var users = {
        _id: store.getState().user.id,
            name: GLOBAL.mydata.name
                                        }
                                        var today = new Date();
                                        /* today.setDate(today.getDate() - 30);
                                        var timestamp = new Date(today).toISOString(); */
                                        var timestamp = today.toISOString();
                                        var dict = {
                                            text: 'Attachment',
                                            user: users,
                                            createdAt: timestamp,
                                            _id: x,
                                            image: responseJson.media,
      userdelete:'',
      broaddelete:'',
      video:'',
      image1:''


                                            // etc.
                                        };
                                        array.push(dict)
                                        //Backend.load()

      console.log(responseJson.images)
      var c = {receiver_id:GLOBAL.host.id,sender_id:GLOBAL.user_id,chatgid:GLOBAL.chatid,message:"",have_media:"1"}
      console.log(JSON.stringify(c))
      const socket = io('http://51.79.250.86:3000', {
        transports: ['websocket']
      })
      socket.emit('send_message',c)
                                        Backend.chatendMessage(array)

                          }

                        );




                      // You can also display the image using data:
                      // const source = { uri: 'data:image/jpeg;base64,' + response.data };


                  }
              });
    }

  renderChatVideo = (props) =>{
    const { currentMessage } = props;
       return (
         <View style={{ padding: 0 }}>
         <View style = {{width:200,height:170}}>
         <VideoPlayer  style = {{backgroundColor:"red"}}
         disableBack = {true}
         resizeMode="cover"
         paused={true}
         source={{uri: "https://vjs.zencdn.net/v/oceans.mp4"}}   // Can be a URL or a local file.
               />
               </View>
         </View>
       )
  }
  renderBubble = (props,index) => {
         var a = false;
         if (props.currentMessage.status == true){
         a = true;
         }else{
             a = false;
         }
         //
         // if (props.currentMessage.user_id != GLOBAL.user_id ){
         //
         // }
         return (

                 <View style={{paddingRight: 12}}>




                     <Bubble {...props}

                     textProps={{
       style: {

         fontFamily:'Nunito-Regular',
         color:'white'

       },
     }}
                     wrapperStyle={{
                                             left: {
                                                 backgroundColor: '#87CEEB',
                                                 color:'white'
                                             },
                                             right: {
                                                 backgroundColor: '#171717',
                                                 color:'white'
                                             }
                                         }} />
                     {props.currentMessage.user_id != GLOBAL.user_id  &&  (
                         <View>

                         </View>
                     )}

                     {props.currentMessage.user_id == GLOBAL.user_id  &&  (
                         <View>


                         </View>
                     )}






                 </View>

         )
     }




  login = () => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Landing',
            params: {someParams: 'parameters goes here...'},
          }),
        ],
      }),
    );
  };

  renderChatFooter = () => {
    if (this.state.texts != '') {
      return (
        <Text style={{fontSize: 14, margin: 10}}> {this.state.texts}</Text>
      );
    }

    // if (this.state.isTyping) {
    //   if (this.typingTimeoutTimer == null) {
    //     this.startTimer();
    //   }
    //   return <TypingIndicator />;
    // }
    return null;
  };
  fieldView = (title, value) => (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        paddingVertical: 5,
      }}>
      <View style={{flex: 0.5, paddingHorizontal: 5}}>
        <Text>{title}</Text>
      </View>
      <View style={{flex: 0.5, paddingHorizontal: 5}}>
        <Text>{value}</Text>
      </View>
    </View>
  );

  renderInputToolbar (props) {
          //Add the extra styles via containerStyle
          return <InputToolbar {...props}

                               textInputStyle={{ color: "black" }}
                               containerStyle={{backgroundColor:'white',marginLeft:10,borderRadius:20,borderWidth:0,color:'black',marginBottom:0,marginRight:10}} />
      }


clear = () =>{
  Backend.updateDeleteStatus()
  this.setState(previousState =>
    ({ messages: []}))
}
      onDelete(messageIdToDelete,message) {

        Backend.updateStatus(messageIdToDelete,message)
  this.setState(previousState =>
    ({ messages: previousState.messages.filter(message => message._id !== messageIdToDelete) }))
}





    onLongPress = (context, message)=> {
    console.log(context, message);
    const options = ['copy','Delete Message', 'Cancel'];
    const cancelButtonIndex = options.length - 1;
    context.actionSheet().showActionSheetWithOptions({
        options,
        cancelButtonIndex
    }, (buttonIndex) => {
        switch (buttonIndex) {
            case 0:
                Clipboard.setString(message.text);
                break;
            case 1:
            this.onDelete(message._id,message)
            //alert(JSON.stringify(this.state.messages))

            // this.setState(previousState =>
            //   ({ messages: previousState.messages.filter(message => message.id !== message._id) }))
              //  this.onDelete(message.id) //pass the function here
                break;
        }
    });
}


stopActionSheet1 = async () => {
   if (!this.state.recording) return;
   console.log('stop record');
   this.setState({recording:false})
   let audioFile = await AudioRecord.stop();
 //  console.log('audioFile', audioFile);
var d = `file://${audioFile}`
const url =
     'http://51.79.250.86/roulette/api/upload_media';
   const data = new FormData();
   data.append('image', {
     uri: d,
     type: 'audio/wav', // or photo.type
     name: 'recording.wav',
   });
   fetch(url, {
     method: 'post',
     body: data,
     headers: {
       'Content-Type': 'multipart/form-data',
     },
   })
     .then(response => response.json())
     .then(responseJson => {

if (responseJson.status == true){
 var x = randomString({
                                      length: 20,
                                      numeric: true,
                                      letters: true,
                                      special: false,
                                      exclude: ['a', 'b']
                                  });

                                  var array = [];
var users = {
  _id: store.getState().user.id,
      name: GLOBAL.mydata.name
                                  }
                                  var today = new Date();
                                  /* today.setDate(today.getDate() - 30);
                                  var timestamp = new Date(today).toISOString(); */
                                  var timestamp = today.toISOString();
                                  var dict = {
                                      text: '',
                                      user: users,
                                      createdAt: timestamp,
                                      _id: x,
                                      image: "",
                                      video:responseJson.media,
                                      userdelete:'',
                                      broaddelete:''

                                  };
                                  array.push(dict)
                                  //Backend.load()
var c = {receiver_id:GLOBAL.host.id,sender_id:GLOBAL.user_id,chatgid:GLOBAL.chatid,message:"",have_media:"1"}
console.log(JSON.stringify(c))
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
socket.emit('send_message',c)

                                  Backend.chatendMessage(array)



}
       //       this.hideLoading()

       //alert(responseJson.path)

   // You can also display the image using data:
   // const source = { uri: 'data:image/jpeg;base64,' + response.data };
 })

   }
   showActionSheet1 = () =>{
       this.setState({ audioFile: '', recording: true, loaded: false });
  this.setState({stopwatchStart:true})

    const options = {
         sampleRate: 16000,
         channels: 1,
         bitsPerSample: 16,
         wavFile: 'test.wav'
       };

       AudioRecord.init(options)
    AudioRecord.start()
}
  renderSend = (props) =>{
    if (!props.text.trim()) { // text box empty
         return (
           <TouchableOpacity
           onPressOut={ () => this.stopActionSheet1()}
        onLongPress={ () => this.showActionSheet1()}
            >
               <Image style={{width:22, height:22, resizeMode:'contain', marginLeft:9, marginBottom:12}}
                      source={require('./voice.png')}/>
           </TouchableOpacity>
                                   )
                                 }
       return (
           <Send
               {...props}
           >
               <View style={{backgroundColor:'transparent'}}>
                   <Image source={require('../resources/send.png')}
                   style = {{height:38,width:38,resizeMode:'contain',backgroundColor:'transparent',marginRight:2,marginBottom:2}}/>
               </View>
           </Send>
       );
   }
   toggleStopwatch = () => {
         this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
     };
     resetStopwatch() {
         this.setState({stopwatchStart: false, stopwatchReset: true});
     }
     getFormattedTime(time) {
         this.currentTime = time;
     }

   renderChatVideo1 = (props) =>{
      console.log(props.currentMessage.video)
      var color = "#171717"
         if (props.currentMessage.user_id == store.getState().user.id){
           color =  "#171717"
         }else {
             color =  "#87CEFA"
         }
    const { currentMessage } = props;
       return (
         <View style={{ padding: 0 }}>
         {props.currentMessage.video.split('.').pop() == "wav"  && (
           <View style = {{width:250,height:40,backgroundColor:color,borderRadius:13}}>
           {props.currentMessage.video.split('.').pop() == "wav" && (
             <AudioPlayer
                 url={currentMessage.video}
               />
           )}

                 </View>
         )}

         {props.currentMessage.video.split('.').pop() != "wav"  && (
           <Lightbox
         renderHeader={close => (
           <TouchableOpacity onPress={close}>
             <Text style={styles.closeButton}>Close</Text>
           </TouchableOpacity>
         )}
              renderContent={(item ) =>
                <View style={{ width: window.width, height: window.height - 200 }}>
                <VideoPlayer  style = {{backgroundColor:"black"}}
            disableBack = {true}
            resizeMode="contain"

            source={{uri: convertToProxyURL(props.currentMessage.video)}}   // Can be a URL or a local file.
                  />
                </View>}

         >
          <View>
          <Image   source={{uri:props.currentMessage.image1}}
                   style  = {{width:200, height:200,borderRadius:15,borderWidth:2,borderColor:'white'
                 }}/>
                 <View style = {{backgroundColor:'rgba(0,0,0,0.4)',marginTop:-200,height:200,borderRadius:22}}>
                 <Image
                     source={require('./play.png')}
                     style={{width: 22, height: 22,marginTop:80,marginLeft:80,resizeMode:'contain'}}


                 />
                 </View>
          </View>
          </Lightbox>
         )}

         </View>
       )
  }

   renderTime = (props) => {
 return (
   <Time
   {...props}
     timeTextStyle={{
       left: {
         color: 'grey',
       },
       right: {
         color: 'grey',
       },
     }}
   />
 );
};
  render() {


    return (
      <ImageBackground source={require('../resources/chat-bg.png')}
style = {{flex:1,backgroundColor:'#fcfcfe',width:window.width,marginBottom:8}}>



      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>

        <Text style={textStyle.simpleHeaderTitle}>{GLOBAL.nasme}</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => this.props.navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
<Text  onPress={() => this.clear() }style={{position:'absolute',top:55,right:12,fontWeight:'bold',color:"white"}}>Clear All</Text>
      </LinearGradient>



             <GiftedChat
             renderActions={this.renderActions}
                    onLongPress={this.onLongPress}
                     extraData={this.state}
                    showUserAvatar = {false}
                     messages={this.state.messages}
 renderMessageVideo={this.renderChatVideo1}
             renderChatFooter={this.renderChatFooter}
             renderSend = {this.renderSend}
             renderInputToolbar={this.renderInputToolbar}
                     onSend={message => {

   const regex = /\d/;
   message[0].text = message[0].text.replace(/\d+/g, "XXX")
message[0].text = message[0].text.replace(new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?"), "XXX")


var c = {receiver_id:GLOBAL.host.id,sender_id:GLOBAL.user_id,chatgid:GLOBAL.chatid,message:message[0].text,have_media:"0"}
console.log(JSON.stringify(c))
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
socket.emit('send_message',c)
Backend.chatendMessage(message);

                     }}
                     renderBubble={this.renderBubble}
                     renderTime = {this.renderTime}
                     renderAvatar = {null}
                     onInputTextChanged = {text =>{


                         // alert(text)

                     }

                     }
                     user={{
                         _id: store.getState().user.id,
                          name: GLOBAL.mydata.name
                     }}
                 />


                 <ActionSheet
    ref={o => this.ActionSheet = o}
    title={'Select'}
    options={['Select Image', 'Select Video', 'cancel']}
    cancelButtonIndex={2}
    destructiveButtonIndex={1}
    onPress={(index) => {
      if (index == 0){
        this.uploadto()
      }
      if (index == 1){
        this.uploadtoo()
      }

    }}
  />

  {this.state.recording == true && (
       <View style = {{position:'absolute',bottom:0,width:window.width - 70 ,height:45,backgroundColor:'black',fle
     :'row',justifyContent:'space-between'}}>

  <View style =  {{margin:12}}>
     <Stopwatch
         laps
         start={this.state.stopwatchStart}
         reset={this.state.stopwatchReset}
         options={options2}
         getTime={this.getFormattedTime}
     />

     </View>

       </View>
   )}

             </ImageBackground>


    );
  }
  componentDidMount() {
    Unsseen({user_id:GLOBAL.user_id,chatgid:GLOBAL.chatid})
           .then((data) => {
             if (data.status) {
               console.log(JSON.stringify(data))

             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
    const socket = io('http://51.79.250.86:3000', {
      transports: ['websocket']
    })
          var c = {host_id:GLOBAL.host.id,user_id:GLOBAL.user_id,chatgid:GLOBAL.chatid}
    socket.emit('user_join_chat',c)
                       
        // Orientation.lockToLandscape();
  //  alert('hi')


    //  GLOBAL.mystatus = "Online";



      // Backend.updateMessage(message => {
      //     alert(JSON.stringify(message))
      //
      //
      // })


      Backend.loadMessages1(message => {


if (message.userdelete == ""){


  if (message.length == 0){


  }else {
    this.setState(previousState => {


        return {
            messages: GiftedChat.append(previousState.messages, message)
        };
    });
}

  }








      });


     ;


      // Backend.updateMessage(message => {
      //     alert(JSON.stringify(message))
      //
      //
      // })





  }




  componentWillUnmount() {
    const socket = io('http://51.79.250.86:3000', {
      transports: ['websocket']
    })
    var c = {chatgid:GLOBAL.chatid}
    socket.emit('user_leave_chat',c)
    Backend.closeChat();
  }
}
const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#001739',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  slide1: {
    marginLeft: 50,

    width: window.width - 50,
    height: 300,
    resizeMode: 'contain',
    marginTop: window.height / 2 - 200,
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});
