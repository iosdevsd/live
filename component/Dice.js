import React, {Component} from 'react';
import {Platform, StyleSheet,ScrollView, Text, View,FlatList,ImageBackground,ActivityIndicator,StatusBar,Image,TouchableOpacity ,Alert,Container,Linking ,TextInput , Dimensions} from 'react-native';
const windowW= Dimensions.get('window').width
const windowH = Dimensions.get('window').height
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { EventRegister } from 'react-native-event-listeners'
const window = Dimensions.get('window');
import store from '../redux/store';
const GLOBAL = require('./Global');
const green = '#1F7316';
const red = '#C82222';
const black = '#030A02';
import {LoginOtpApi,SignInApi,Explore,FetchHomeWallet,PujaStart,GetProfileApi,EndLive,Bet1} from '../backend/Api';

import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
//import IndicatorCustom from './IndicatorCustom.js';




type Props = {};
export default class Dice extends Component<Props> {

    static navigationOptions = ({ navigation }) => {
        return {
           header: () => null,
        }
    }




    constructor(props){
        super(props)
        const { navigation } = this.props;
        this.state = {
            name: '',
            response:'',
            check :0,


            array : [
            {
              index:'small',
              amount:'0',
              position:0,
              selected:'',
              bet_type:"small_big",
            },
            {
              index:'odd',
              amount:'0',
              position:1,
              selected:'',
              bet_type:"odd_even",
            },
            {
              index:'even',
              amount:'0',
              position:2,
              selected:'',
              bet_type:"odd_even",
            },
            {
              index:'big',
              amount:'0',
              position:3,
              selected:'',
              bet_type:"small_big",
            },
            {
              index:'1,1',
              amount:'0',
              position:4,
              selected:'',
              bet_type:"double",

            },
            {
              index:'2,2',
              amount:'0',
              position:5,
              selected:'',
              bet_type:"double",
            },
            {
              index:'3,3',
              amount:'0',
              position:6,
              selected:'',
              bet_type:"double",
            },
            {
              index:'4,4',
              amount:'0',
              position:7,
              selected:'',
              bet_type:"double",
            },
            {
              index:'5,5',
              amount:'0',
              position:8,
              selected:'',
              bet_type:"double",
            },
            {
              index:'6,6',
              amount:'0',
              position:9,
              selected:'',
              bet_type:"double",
            },
            {index:'1,1,1',
            amount:'0',
            position:10,
            selected:'',
            bet_type:"specific_triple",

          },
          {index:'2,2,2',
          amount:'0',
          position:11,
          selected:'',
          bet_type:"specific_triple",

        },
        {index:'3,3,3',
        amount:'0',
        position:12,
        selected:'',
        bet_type:"specific_triple",

      },
      {index:'4,4,4',
      amount:'0',
      position:13,
      selected:'',
      bet_type:"specific_triple",

    },
    {index:'5,5,5',
    amount:'0',
    position:14,
    selected:'',
    bet_type:"specific_triple",

  },
  {index:'6,6,6',
  amount:'0',
  position:15,
  selected:'',
  bet_type:"specific_triple",

},
{index:'any_triple',
amount:'0',
position:16,
selected:'',
bet_type:"any_triple",

},
{index:'4',
amount:'0',
position:17,
selected:'',
bet_type:"total",

},
{index:'5',
amount:'0',
position:18,
selected:'',
bet_type:"total",

},
{index:'6',
amount:'0',
position:19,
selected:'',
bet_type:"total",

},
{index:'7',
amount:'0',
position:20,
selected:'',
bet_type:"total",

},
{index:'8',
amount:'0',
position:21,
selected:'',
bet_type:"total",

},
{index:'9',
amount:'0',
position:22,
selected:'',
bet_type:"total",

},
{index:'10',
amount:'0',
position:23,
selected:'',
bet_type:"total",

},
{index:'11',
amount:'0',
position:24,
selected:'',
bet_type:"total",

},
{index:'12',
amount:'0',
position:25,
selected:'',
bet_type:"total",

},
{index:'13',
amount:'0',
position:26,
selected:'',
bet_type:"total",

},
{index:'14',
amount:'0',
position:27,
selected:'',
bet_type:"total",

},
{index:'15',
amount:'0',
position:28,
selected:'',
bet_type:"total",

},
{index:'16',
amount:'0',
position:29,
selected:'',
bet_type:"total",

},
{index:'17',
amount:'0',
position:30,
selected:'',
bet_type:"total",

},
{index:'1,2',
amount:'0',
position:31,
selected:'',
bet_type:"combination",

},

{index:'1,3',
amount:'0',
position:32,
selected:'',
bet_type:"combination",

},
{index:'1,4',
amount:'0',
position:33,
selected:'',
bet_type:"combination",

},
{index:'1,5',
amount:'0',
position:34,
selected:'',
bet_type:"combination",

},
{index:'1,6',
amount:'0',
position:35,
selected:'',
bet_type:"combination",

},
{index:'2,3',
amount:'0',
position:36,
selected:'',
bet_type:"combination",

},
{index:'2,4',
amount:'0',
position:37,
selected:'',
bet_type:"combination",

},
{index:'2,5',
amount:'0',
position:38,
selected:'',
bet_type:"combination",

},
{index:'2,6',
amount:'0',
position:39,
selected:'',
bet_type:"combination",

},
{index:'3,4',
amount:'0',
position:40,
selected:'',
bet_type:"combination",

},
{index:'3,5',
amount:'0',
position:41,
selected:'',
bet_type:"combination",

},
{index:'3,6',
amount:'0',
position:42,
selected:'',
bet_type:"combination",

},
{index:'4,5',
amount:'0',
position:43,
selected:'',
bet_type:"combination",

},
{index:'4,6',
amount:'0',
position:44,
selected:'',
bet_type:"combination",

},
{index:'5,6',
amount:'0',
position:45,
selected:'',
bet_type:"combination",

},
{index:'1',
amount:'0',
position:46,
selected:'',
bet_type:"single",


},
{index:'2',
amount:'0',
position:47,
selected:'',
bet_type:"single",

},
{index:'3',
amount:'0',
position:48,
selected:'',
bet_type:"single",

},
{index:'4',
amount:'0',
position:49,
selected:'',
bet_type:"single",

},
{index:'5',
amount:'0',
position:50,
selected:'',
bet_type:"single",

},
{index:'6',
amount:'0',
position:51,
selected:'',
bet_type:"single",

},









          ],
        }
    }





    showLoading() {
        this.setState({loading: true})
    }

    hideLoading() {
        this.setState({loading: false})
    }



    componentDidMount(){

      this.listener = EventRegister.addEventListener('reset', (data) => {
          this.refresh()
           //roundbet
       })


    const socket = io('http://51.79.250.86:3000', {
    transports: ['websocket']
  })


    socket.on("place_your_bet", msg => {
    //  alert(JSON.stringify(msg))
 if (msg.status == true){
   this.submit()
//   this.props.navigation.goBack()
 //RtcEngine.setClientRole(Host)
 }else{
 //RtcEngine.setClientRole(Audience)
 }

 });

    socket.emit('place_your_bet',{

                                    bridge_id:GLOBAL.bookingid,
                                    token:store.getState().user.token,
                                    user_id:store.getState().user.id
                 })

//        this.props.navigation.addListener('willFocus',this._handleStateChange);


    }


    refresh = () =>{
      for (var i = 0 ; i < this.state.array.length ; i ++)
      {
var a = this.state.array[i]
a.selected = ""
a.amount = "0"
this.state.array[i] = a
      }
this.setState({array:this.state.array})

    }


    submit = () =>{
         var bet = [];

         for (var i = 0 ; i < this.state.array.length ; i ++)
         {
           if (this.state.array[i].selected == "Y")
           {




             var dict =  {
             "bet_type" : this.state.array[i].bet_type,
             "bet_numbers": this.state.array[i].index,
             "bet_amount" : this.state.array[i].amount


         }
bet.push(dict)


        }
             }



var cd = {host_id:GLOBAL.host.id,event_id:GLOBAL.host.event_id,bridge_id:GLOBAL.bookingid,bets:JSON.stringify(bet)}


console.log(JSON.stringify(cd))
             Bet1({host_id:GLOBAL.host.id,event_id:GLOBAL.event_id,bridge_id:GLOBAL.bookingid,bets:JSON.stringify(bet)})
                    .then((data) => {
                       //   toggleLoading(false);

                      if (data.status) {
                        this.refresh()

                       // alert(JSON.stringify(data))

                     //wallet





                      } else {
             alert(JSON.stringify(data.message))
                      }
                    })
                    .catch((error) => {
                      //toggleLoading(false);
                      console.log('error', error);
                    });

             var c = {

             "host_id":"1",
             "event_id":"1",
             "bridge_id":"2",
             "bets":JSON.stringify(bet)
           }
            console.log(JSON.stringify(c))
           }









    mode = (item,index) =>{



      //this.state.array.selected




      var amou = 0
            for (var i = 0 ; i< this.state.array.length ; i++ ){
              if (this.state.array[i].selected == "Y"){
              //  alert(this.state.array[i].amount)
                amou = amou + parseInt(this.state.array[i].amount)
              }
            }
amou = amou + parseInt(GLOBAL.coin)

            var calc = parseInt(GLOBAL.wallet)

            console.log(calc)
            console.log(amou)

            if (calc < amou){
              Alert.alert('LOW BALANCE',`You have insufficient coins – Please recharge and then try.`,
                                       [
                                           {text:"OK",
                                           },
                                       ],
                                       {cancelable:false}
                                   )
              return
            }


            var balan = calc - amou

            var ff = {
              balan:balan.toString(),
              amount:amou
            }

            EventRegister.emit('wallet', ff)

      var k = this.state.array[index]
      k.amount = parseInt(k.amount) + parseInt(GLOBAL.coin)
      k.selected = "Y"

      this.state.array[index] = k
      this.setState({array:this.state.array})



      //alert(index)
    }


    render() {
      var yeah = this.state.response
        // if(this.state.loading){
        //     return(
        //       <IndicatorCustom/>
        //     )
        // }
        return (

        <ScrollView style={{flex:1, flexDirection:'column',backgroundColor:'transparent'}}>



<View style = {{backgroundColor:'#d19650',width:window.width - 60 ,alignSelf:'center',height:470,borderRadius:22}}>


<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('small',0)}>
  <View style = {{height:77,backgroundColor:'#d19650',width:window.width/4.8,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#e4dfdb',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   Small
  </Text>

  {this.state.array[0].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-17}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[0].amount}
         </Text>

       </ImageBackground>
  )}
  <Text style = {{color:'#a06320',fontFamily:'DMSans-Bold',fontSize:22,marginTop:2}}>
   4-10
  </Text>

  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('odd',1)}>
  <View style = {{height:77,backgroundColor:'#d19650',width:window.width/4.8,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#e4dfdb',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   Odd
  </Text>


  {this.state.array[1].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-17}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[1].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('even',2)}>
  <View style = {{height:77,backgroundColor:'#d19650',width:window.width/4.8,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#e4dfdb',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   Even
  </Text>

  {this.state.array[2].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-17}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[2].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('big',3)}>
  <View style = {{height:77,backgroundColor:'#d19650',width:window.width/4.8,marginTop:0,marginLeft:0,alignItems:'center',borderTopRightRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7',borderColor:'#dad5c7'}}>

  <Text style = {{color:'#e4dfdb',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   Big
  </Text>

  {this.state.array[3].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-17}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[3].amount}
         </Text>

       </ImageBackground>
  )}
  <Text style = {{color:'#a06320',fontFamily:'DMSans-Bold',fontSize:22,marginTop:2}}>
   11-17
  </Text>

  </View>
  </TouchableOpacity>




</View>
<View style = {{backgroundColor:"white",width:130,borderRadius:22,height:15,marginTop:-50,alignSelf:'center'}}>
<Text style = {{color:'#a06320',fontFamily:'DMSans-Regular',fontSize:10,marginTop:2,textAlign:'center'}}>
PAYOUT 0.9:1
</Text>
</View>
<View style = {{backgroundColor:"white",width:130,borderRadius:22,height:15,marginTop:3,alignSelf:'center',marginBottom:6}}>
<Text style = {{color:'#a06320',fontFamily:'DMSans-Regular',fontSize:10,marginTop:2,textAlign:'center'}}>
LOOSE IF ANY TRIPLE
</Text>
</View>
<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('1,1',4)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice1.png')}>


        </ImageBackground>

  {this.state.array[4].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[4].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('2,2',5)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice2.png')}>


        </ImageBackground>

  {this.state.array[5].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[5].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('3,3',6)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice3.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice3.png')}>


        </ImageBackground>

  {this.state.array[6].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[6].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('4,4',7)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice4.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice4.png')}>


        </ImageBackground>

  {this.state.array[7].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[7].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('5,5',8)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice5.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice5.png')}>


        </ImageBackground>

  {this.state.array[8].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[8].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('6,6',9)}>
  <View style = {{height:70,backgroundColor:'#d19650',width:window.width/7.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopRightRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice6.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>

  {this.state.array[9].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-37}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[9].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>




</View>




<View style = {{flexDirection:'row'}}>
<View>
  <TouchableOpacity onPress={()=>this.mode('1,1,1',10)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice1.png')}>


        </ImageBackground>
        <ImageBackground
          style={{width:25,height:25,marginTop:6,marginLeft:2}}
          imageStyle={styles.image}
          resizeMode="cover"
             source={require('../resources/dice1.png')}>


           </ImageBackground>
           </View>

  {this.state.array[10].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[10].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <View style = {{flexDirection:'row'}}>
    <TouchableOpacity onPress={()=>this.mode('2,2,2',11)}>
    <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <View style = {{flexDirection:'row'}}>
    <ImageBackground
      style={{width:25,height:25,marginTop:6,marginLeft:2}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/dice2.png')}>


       </ImageBackground>
       <ImageBackground
         style={{width:25,height:25,marginTop:6,marginLeft:2}}
         imageStyle={styles.image}
         resizeMode="cover"
            source={require('../resources/dice2.png')}>


          </ImageBackground>
          <ImageBackground
            style={{width:25,height:25,marginTop:6,marginLeft:2}}
            imageStyle={styles.image}
            resizeMode="cover"
               source={require('../resources/dice2.png')}>


             </ImageBackground>
             </View>

    {this.state.array[11].selected == "Y" && (
      <ImageBackground
        style={{width:25,height:25,marginTop:-27}}
        imageStyle={styles.image}
        resizeMode="cover"
           source={require('../resources/cr.png')}>
           <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
            {this.state.array[11].amount}
           </Text>

         </ImageBackground>
    )}


    </View>
    </TouchableOpacity>

  </View>

  <View style = {{flexDirection:'row'}}>
    <TouchableOpacity onPress={()=>this.mode('3,3,3',12)}>
    <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <View style = {{flexDirection:'row'}}>
    <ImageBackground
      style={{width:25,height:25,marginTop:6,marginLeft:2}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/dice3.png')}>


       </ImageBackground>
       <ImageBackground
         style={{width:25,height:25,marginTop:6,marginLeft:2}}
         imageStyle={styles.image}
         resizeMode="cover"
            source={require('../resources/dice3.png')}>


          </ImageBackground>
          <ImageBackground
            style={{width:25,height:25,marginTop:6,marginLeft:2}}
            imageStyle={styles.image}
            resizeMode="cover"
               source={require('../resources/dice3.png')}>


             </ImageBackground>
             </View>

    {this.state.array[12].selected == "Y" && (
      <ImageBackground
        style={{width:25,height:25,marginTop:-27}}
        imageStyle={styles.image}
        resizeMode="cover"
           source={require('../resources/cr.png')}>
           <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
            {this.state.array[12].amount}
           </Text>

         </ImageBackground>
    )}


    </View>
    </TouchableOpacity>

  </View>

</View>

<View>
  <TouchableOpacity onPress={()=>this.mode('any_triple',16)}>
  <View style = {{height:105,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#e4dfdb',fontFamily:'DMSans-Bold',fontSize:18,marginTop:32}}>
   Any Triple
  </Text>

  {this.state.array[16].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[16].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>





</View>

<View>
  <TouchableOpacity onPress={()=>this.mode('4,4,4',13)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice4.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice4.png')}>


        </ImageBackground>
        <ImageBackground
          style={{width:25,height:25,marginTop:6,marginLeft:2}}
          imageStyle={styles.image}
          resizeMode="cover"
             source={require('../resources/dice4.png')}>


           </ImageBackground>
           </View>

  {this.state.array[13].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[13].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <View style = {{flexDirection:'row'}}>
    <TouchableOpacity onPress={()=>this.mode('5,5,5',15)}>
    <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <View style = {{flexDirection:'row'}}>
    <ImageBackground
      style={{width:25,height:25,marginTop:6}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/dice5.png')}>


       </ImageBackground>
       <ImageBackground
         style={{width:25,height:25,marginTop:6,marginLeft:2}}
         imageStyle={styles.image}
         resizeMode="cover"
            source={require('../resources/dice5.png')}>


          </ImageBackground>
          <ImageBackground
            style={{width:25,height:25,marginTop:6,marginLeft:2}}
            imageStyle={styles.image}
            resizeMode="cover"
               source={require('../resources/dice5.png')}>


             </ImageBackground>
             </View>

    {this.state.array[14].selected == "Y" && (
      <ImageBackground
        style={{width:25,height:25,marginTop:-27}}
        imageStyle={styles.image}
        resizeMode="cover"
           source={require('../resources/cr.png')}>
           <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
            {this.state.array[14].amount}
           </Text>

         </ImageBackground>
    )}


    </View>
    </TouchableOpacity>

  </View>

  <View style = {{flexDirection:'row'}}>
    <TouchableOpacity onPress={()=>this.mode('6,6,6',15)}>
    <View style = {{height:35,backgroundColor:'#d19650',width:window.width/3.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:12,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <View style = {{flexDirection:'row'}}>
    <ImageBackground
      style={{width:25,height:25,marginTop:6}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/dice6.png')}>


       </ImageBackground>
       <ImageBackground
         style={{width:25,height:25,marginTop:6,marginLeft:2}}
         imageStyle={styles.image}
         resizeMode="cover"
            source={require('../resources/dice6.png')}>


          </ImageBackground>
          <ImageBackground
            style={{width:25,height:25,marginTop:6,marginLeft:2}}
            imageStyle={styles.image}
            resizeMode="cover"
               source={require('../resources/dice6.png')}>


             </ImageBackground>
             </View>

    {this.state.array[15].selected == "Y" && (
      <ImageBackground
        style={{width:25,height:25,marginTop:-27}}
        imageStyle={styles.image}
        resizeMode="cover"
           source={require('../resources/cr.png')}>
           <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
            {this.state.array[15].amount}
           </Text>

         </ImageBackground>
    )}


    </View>
    </TouchableOpacity>

  </View>

</View>
</View>



<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('4',17)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   4
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   50:1
  </Text>

  {this.state.array[17].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[17].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('5',18)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   5
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   20:1
  </Text>

  {this.state.array[18].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[18].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('6',19)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   6
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   15:1
  </Text>

  {this.state.array[19].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[19].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('7',20)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   7
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   12:1
  </Text>

  {this.state.array[20].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[20].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('8',21)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   8
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   8:1
  </Text>

  {this.state.array[21].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[21].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('9',22)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   9
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   6:1
  </Text>

  {this.state.array[22].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[22].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('10',23)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   10
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   6:1
  </Text>

  {this.state.array[23].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[23].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>







</View>

<View style = {{flexDirection:'row'}}>
<TouchableOpacity onPress={()=>this.mode('11',24)}>
<View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

<Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
 11
</Text>
<Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
 6:1
</Text>

{this.state.array[24].selected == "Y" && (
  <ImageBackground
    style={{width:25,height:25,marginTop:-27}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/cr.png')}>
       <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
        {this.state.array[24].amount}
       </Text>

     </ImageBackground>
)}


</View>
</TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('12',25)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   12
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   6:1
  </Text>

  {this.state.array[25].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[25].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('13',26)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   13
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   8:1
  </Text>

  {this.state.array[26].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[26].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('14',27)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   14
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   12:1
  </Text>

  {this.state.array[27].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[27].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('15',28)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   15
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   15:1
  </Text>

  {this.state.array[28].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[28].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('16',29)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   16
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   20:1
  </Text>

  {this.state.array[29].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[29].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('17',30)}>
  <View style = {{height:40,backgroundColor:'#d19650',width:window.width/8.4,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>

  <Text style = {{color:'#a06420',fontFamily:'DMSans-Bold',fontSize:18,marginTop:1}}>
   17
  </Text>
  <Text style = {{color:'black',fontFamily:'DMSans-Bold',fontSize:12,marginTop:0}}>
   50:1
  </Text>

  {this.state.array[30].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[30].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>







</View>

<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('1,2',31)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice2.png')}>


        </ImageBackground>
        </View>

  {this.state.array[31].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[31].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('1,3',32)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice3.png')}>


        </ImageBackground>
        </View>

  {this.state.array[32].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[32].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('1,4',33)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice4.png')}>


        </ImageBackground>
        </View>

  {this.state.array[33].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[33].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('1,5',34)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice5.png')}>


        </ImageBackground>
        </View>

  {this.state.array[34].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[34].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('1,6',35)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>
        </View>

  {this.state.array[35].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[35].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>



</View>



<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('2,3',36)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice3.png')}>


        </ImageBackground>
        </View>

  {this.state.array[36].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[36].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('2,4',37)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice4.png')}>


        </ImageBackground>
        </View>

  {this.state.array[37].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[37].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('2,5',38)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice5.png')}>


        </ImageBackground>
        </View>

  {this.state.array[38].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[38].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('2,6',39)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>
        </View>

  {this.state.array[39].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[39].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('3,4',40)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice3.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice4.png')}>


        </ImageBackground>
        </View>

  {this.state.array[40].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[40].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>



</View>



<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('3,5',41)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice3.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice5.png')}>


        </ImageBackground>
        </View>

  {this.state.array[41].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[41].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('3,6',42)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice3.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>
        </View>

  {this.state.array[42].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[42].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('4,5',43)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice4.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice5.png')}>


        </ImageBackground>
        </View>

  {this.state.array[43].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[43].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('4,6',44)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice4.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>
        </View>

  {this.state.array[44].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[44].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('5,6',45)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice5.png')}>


     </ImageBackground>
     <ImageBackground
       style={{width:25,height:25,marginTop:6,marginLeft:2}}
       imageStyle={styles.image}
       resizeMode="cover"
          source={require('../resources/dice6.png')}>


        </ImageBackground>
        </View>

  {this.state.array[45].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[45].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>



</View>



<View style = {{flexDirection:'row'}}>
  <TouchableOpacity onPress={()=>this.mode('1',46)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/7.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice1.png')}>


     </ImageBackground>

        </View>

  {this.state.array[46].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[46].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('2',47)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/7.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice2.png')}>


     </ImageBackground>

        </View>

  {this.state.array[47].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[47].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('3',48)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/7.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice3.png')}>


     </ImageBackground>

        </View>

  {this.state.array[48].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[48].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=>this.mode('4',49)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/7.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice4.png')}>


     </ImageBackground>

        </View>

  {this.state.array[49].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[49].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>
  <TouchableOpacity onPress={()=>this.mode('5',50)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/6.0,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice5.png')}>


     </ImageBackground>

        </View>

  {this.state.array[50].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[50].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>


  <TouchableOpacity onPress={()=>this.mode('6',51)}>
  <View style = {{height:35,backgroundColor:'#d19650',width:window.width/7.5,marginTop:0,marginLeft:0,alignItems:'center',borderTopLeftRadius:0,borderBottomLeftRadius:0,borderWidth:1.2,borderColor:'#dad5c7'}}>
<View style = {{flexDirection:'row'}}>
  <ImageBackground
    style={{width:25,height:25,marginTop:6}}
    imageStyle={styles.image}
    resizeMode="cover"
       source={require('../resources/dice6.png')}>


     </ImageBackground>

        </View>

  {this.state.array[51].selected == "Y" && (
    <ImageBackground
      style={{width:25,height:25,marginTop:-27}}
      imageStyle={styles.image}
      resizeMode="cover"
         source={require('../resources/cr.png')}>
         <Text style = {{color:'white',fontFamily:'Nunito-Bold',fontSize:10,marginTop:5,textAlign:'center'}}>
          {this.state.array[51].amount}
         </Text>

       </ImageBackground>
  )}


  </View>
  </TouchableOpacity>



</View>



</View>







          </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },

});
