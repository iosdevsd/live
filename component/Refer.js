import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Clipboard,
  Share
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi} from '../backend/Api';
import {textStyle, viewStyle} from '../style/Style';
import * as actions from '../redux/actions';
import {settingList} from '../backend/Config';



const Refer = ({navigation}) => {
  const [username, setUsername] = useState({});
  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 setUsername(data.data)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);

  const copyToClipboard = () => {
   Clipboard.setString(username.referral_code)
 }

const  fancyShareMessage=()=>{

    var a = username.referral_code
    var commonHtml = `I want to recommend Cleo Live - mobile application.\n\nAt Cleo Live you can Play Live Games with Live Girls and Win lots of Money.\n\nLogin Every day and get Free Coins to Play.\n\nDownload Cleo Live from https://play.google.com/store/apps/details?id=com.cleolive&hl=en_IN&gl=US.\n\nWhile Registering use my referral code ${a} and get Free Coins`;

    Share.share({
            message:commonHtml
        },{
            tintColor:'green',
            dialogTitle:'Share this Referral Code via....'
        }
    );
}


  const onPressHandler = (key) => {
    switch (key) {
      case 'logout':
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Introduction'}],
                 });
        break;
      default:
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Refer & Earn</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>

      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <ScrollView>
        <Image
          source={require('../resources/coin.png')}
          style={{marginTop:30,width:100,height:70,alignSelf:'center',resizeMode:'contain'}}
        />

        <Text style = {{color:'white',margin:20,textAlign:'center',fontFamily:"DMSans-Bold",fontSize:28}}>
        Refer your friends and earn Coins
</Text>

<Text style = {{color:'white',margin:20,textAlign:'center',fontFamily:"DMSans-Regular",fontSize:14,marginTop:3}}>
Invite your friends to join Cleo Live and get {username.referral_price} coins.
</Text>
<Text style = {{color:'white',margin:20,textAlign:'center',fontFamily:"DMSans-Regular",fontSize:14,marginTop:-20}}>
Your friend also gets {username.referral_price} coins
</Text>

<View style = {{width:350,height:50,flexDirection:'row',borderRadius:20,borderColor:'white',borderWidth:3,alignSelf:'center',marginTop:5,justifyContent:'space-between'}}>
<Text style = {{color:'white',fontFamily:"DMSans-Bold",fontSize:20,marginLeft:12,marginTop:7}}>
{username.referral_code}
</Text>
<TouchableOpacity style = {{height:50}}

  onPress={() => copyToClipboard()}>
<View style = {{backgroundColor:'white',borderRadius:16,width:100,height:44}}>
<Text style = {{color:'#8C025A',fontFamily:"DMSans-Bold",fontSize:20,marginTop:9,marginLeft:20}}>
COPY
</Text>
</View>

</TouchableOpacity>
</View>

<Text style = {{color:'white',margin:20,textAlign:'center',fontFamily:"DMSans-Regular",fontSize:14}}>
Ask your Friends to add your referral code while purchasing Coins - For every purchase made by your friend you get incentive coins for 5%.
</Text>

<TouchableOpacity

  onPress={() => fancyShareMessage()}>
<View style = {{width:350,height:50,borderRadius:20,backgroundColor:'white',borderWidth:0,alignSelf:'center',marginTop:10,justifyContent:'space-between'}}>



<Text style = {{color:'#8C025A',fontFamily:"DMSans-Bold",fontSize:20,textAlign:'center',marginTop:10}}>
INVITE NOW
</Text>


</View>
</TouchableOpacity>
<View style = {{height:100}}>

</View>


        </ScrollView>


        </LinearGradient>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  appVersion: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
    alignSelf: 'center',
    opacity: 0.7,
    marginTop: 47,
  },
  view: {
    flexDirection: 'row',
    borderBottomColor: '#F5F5F5',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  image: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 12,
  },
  text: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
  },
  linearGradient: {
    flex: 1,
  },
});

export default Refer;
