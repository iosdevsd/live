import React ,{ useEffect ,useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Alert,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import Toast from 'react-native-simple-toast';
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi,FetchVip,CreateVip} from '../backend/Api';

const Vip = ({navigation}) => {
  const [day, setDay] = useState(0);
  const [selectedId, setSelectedId] = useState(true);
  const [coin, setCoin] = useState(0);
    const [plan, setPlan] = useState(0);
    const [username, setUsername] = useState({});
      const [data, setData] = useState([]);

  const flatListData = [
    {
      id: 1,
      title: 'Unlimited Messages',
      source: require('../resources/comment.png'),
    },


    {
      id: 3,
      title: 'Access to all VIP Rooms',
      source: require('../resources/lock.png'),
    },





    {
      id: 6,
      title: 'Avtar Frame',
      source: require('../resources/frame.png'),
    },



  ];

  const renderItem = ({item}) => (
    <View key={item.key} style={styles.flatView}>
      <Image source={item.source} style={styles.flatImage} />
      <Text style={styles.flatTitle}>{item.title}</Text>
    </View>
  );

const calcy = () =>{
  if (coin == "0"){
    return
  }

  if (parseInt(coin) > parseInt(username.wallet)){
    Alert.alert('LOW BALANCE',`You have insufficient coins – Please recharge and then try.`,
                             [
                                 {text:"OK",
                                 },
                             ],
                             {cancelable:false}
                         )

return
}else{
  CreateVip({plan_id:plan})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
            // alert(JSON.stringify(data))

          navigation.navigate('Thankyou')





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
}

useEffect (() =>{
   console.log(selectedId?'tt':'ff')
setSelectedId(!selectedId)
},[data])


  const edit = (item,index) =>{
//setSelectedId(!selectedId)

for(var i  = 0 ; i <data.length ; i++){
  var a = data[i]
  if (a.selected != ""){
    a.selected  = ''
    data[i] = a
      setData(data)
  }

}
    var editr = data[index]
             if (item.selected == ''){
               editr.selected  = '1'
              // alert(item.id)
              setPlan(item.id)
               setCoin(item.price)
             }else{
               editr.selected  = ''
             }

             data[index] = editr
    setData(data)
    setSelectedId(!selectedId)
  }

  useEffect(() => {
setSelectedId(!selectedId)
    FetchVip({user_id:"1"})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {

            //   alert(JSON.stringify(data))

               var c = data.data

               var array = []

               for (var i = 0 ; i < c.length ; i++){
                 var dict = c[i]
                 dict["selected"] = ''
                 array.push(dict)

               }

               setData(array)
            ////  alert(JSON.stringify(array))






             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });




    const unsubscribew =   navigation.addListener('focus', () => {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                // alert(JSON.stringify(data))
                setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);

  return (
    <SafeAreaView style={styles.conatiner}>
      <ScrollView>
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={viewStyle.simpleHeaderView}>
          <Text style={textStyle.simpleHeaderTitle}>VIP</Text>
          <View style={styles.backView}>
            <TouchableOpacity
              style={styles.backTouch}
              onPress={() => navigation.goBack()}>
              <Image
                source={require('../resources/back.png')}
                style={styles.backImage}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>
        <ImageBackground
          source={require('../resources/bg-vip.png')}
          style={styles.bgVip}>
          <Image
            source={require('../resources/crown3.png')}
            style={styles.crownImage}
          />
          <Text style={styles.vipText}>VIP</Text>
        </ImageBackground>
        <View style={styles.view_1}>
          <Text style={styles.text_1}>Privileged Features</Text>
          <TouchableOpacity onPress={() => navigation.navigate('VipDetail')}>
            <Text style={styles.text_2}>View Details</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.view_2}>
          <FlatList
            data={flatListData}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
            numColumns={3}
            horizontal={false}
            scrollEnabled={false}
          />
        </View>
        <Text style={styles.text_3}>Select Time Period</Text>

        <View style={styles.view_3}>
          <FlatList
            data={data}
            numColumns={3}
            horizontal={false}
            extraData={selectedId}
            scrollEnabled={false}
            renderItem={({item,index}) => (
              <TouchableOpacity

                onPress={() => edit(item,index)}>

              <View  style = {{width:100}}
                style={{}}>
                {item.selected == "" && (
                  <View style = {{borderWidth:1,borderColor:'#00000033',padding:5,borderRadius:12,width:100,margin:5}}>
                  <Text style={[styles.text_41]}>
                    {`${item.no_of_days} ${item.duration_type == "day" ?item.duration_type == "1" ? "Day":"Days" :"Hours"}`}
                  </Text>
                  </View>

                )}
                {item.selected != "" && (
                  <View style = {{borderWidth:0,borderColor:'#E80A52',padding:5,borderRadius:12,width:100,margin:5,
                  backgroundColor:'#E80A52'}}>
                  <Text style={{    fontFamily: 'DMSans-Bold',
                      fontWeight: '700',
                      fontSize: 16,
                      color: 'white',
                      textAlign:'center'}}>
                    {`${item.no_of_days} ${item.duration_type == "day" ?item.duration_type == "1" ? "Day":"Days" :"Hours"}`}
                  </Text>
                  </View>

                )}

              </View>
              </TouchableOpacity>
            )}
             extraData={selectedId}
          />
        </View>
        <Text style={styles.priceText}>Price</Text>
        <View style={styles.view_5}>
          <Image
            style={styles.image_5}
            source={require('../resources/coin.png')}
          />
          <Text style={styles.text_5}>{coin} Coins</Text>
        </View>

        {username.is_vip_subscribe == 0 && (
          <LinearGradient
            colors={['#73005C', '#F00B51']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.buttonView}>
            <TouchableOpacity style={styles.buttonTouch} onPress={() => calcy()}>
              <Text style={styles.buttonText}>BUY NOW</Text>
            </TouchableOpacity>
          </LinearGradient>
        )}


      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  bgVip: {
    height: 120,
    alignItems: 'center',

  },
  crownImage: {
    width: 70,
    height: 70,
    resizeMode: 'cover',
    marginTop:30,

  },
  vipText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '900',
    fontSize: 38,
    fontStyle: 'normal',
    color: '#232323',
    marginVertical: 10,
  },
  view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: 36,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 16,
    fontStyle: 'normal',
    color: '#232323',
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 12,
    fontStyle: 'normal',
    color: '#E70A52',
    textDecorationLine: 'underline',
  },
  flatView: {
    marginVertical: 2,
    alignItems: 'center',
    paddingVertical: 2,
  },
  flatImage: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  flatTitle: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262628',
    width: 120,
    textAlign: 'center',
    marginVertical: 5,
  },
  view_2: {
    alignItems: 'center',
    marginTop: 10,
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 16,
    color: '#232323',
    margin: 20,
  },
  view_3: {
    alignItems: 'center',
  },
  view_4: {
    borderWidth: 2,
    borderRadius: 20,
    borderColor: '#EB0B52',
    marginHorizontal: 10,
    marginVertical: 20,
    padding:5,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  view_41: {
    borderColor: '#00000033',
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 16,
    color: '#E80A52',
    textAlign:'center',
  },
  text_41: {
    color: '#00000033',
    textAlign:'center',
  },
  priceText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 16,
    color: '#232323',
    marginHorizontal: 20,
    marginVertical: 12,
  },
  view_5: {
    flexDirection: 'row',
    marginHorizontal: 20,
  },
  image_5: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    marginRight: 9,
  },
  text_5: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#E40A52',
  },
  buttonView: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginVertical: 30,
  },
  buttonTouch: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  buttonText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export default Vip;
