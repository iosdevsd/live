import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Modal,
} from 'react-native';
import Backend from "./Backend.js";
import store from '../redux/store';
var randomString = require('random-string');
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {LoginOtpApi,SignUpApi,RegisterVerify,EditDetail,GetProfileApi,Favorite,Follow} from '../backend/Api';
import {shadowStyle} from '../style/Style';
const GLOBAL = require('./Global');
const ViewProfile1 = ({navigation,callback}) => {
    const [detail, setDetail] = useState('');
    const [detail1, setDetail1] = useState('');
    const [wallet, setWallet] = useState('0');
    const [count, setCount] = useState('0');
    const [follo, isfollow] = useState(false);
      const [check, ischeck] = useState(false);
    const [visible, setvisible] = useState(false);
const  capitalize = (string) => {
  //salert(string)ssss
  if (string == "" || typeof string == "undefined"){
    return ""
  }
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
checkexist = (item)=>{

  if (typeof store.getState().user.id != "undefined"){
    var k = ""

 k =   item


 var x = randomString({
     length: 20,
     numeric: true,
     letters: true,
     special: false,
     exclude: ['a', 'b']
 });

 var array = [];
 var users = {
     _id: store.getState().user.id,
     name: GLOBAL.mydata.name ,
 }
 var today = new Date();
 /* today.setDate(today.getDate() - 30);
 var timestamp = new Date(today).toISOString(); */
 var timestamp = today.toISOString();
 var dict = {
     text:k,
     user: users,
     createdAt: timestamp,
     vip:GLOBAL.mydata.is_vip_subscribe,
     _id: x,
     wallet:GLOBAL.wallet


     // etc.
 };
 array.push(dict)
 //Backend.load()

 Backend.sendMessage12(array)
  }
}
const follow = () =>{
  //alert(JSON.stringify(GLOBAL.host.id))

  Favorite({id:GLOBAL.host.id})
         .then((data) => {
            //   toggleLoading(false);
//alert(JSON.stringify(data))
           if (data.status) {
             isfollow(!follo)
             if (follo){
               checkexist("Has unfollowed you")
             }else {
               checkexist("Has followed you")
             }
             //alert(JSON.stringify(data))
        setCount(data.count_results)
            // alert(JSON.stringify(data))
            //setUsername(data.data)





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });

}

const hi = () =>{
  ischeck(true)
  alert(`Please wait until host ${GLOBAL.host.name} accept the call`)
  var b = parseInt(GLOBAL.host.price)
  var c = parseInt(wallet)

  if (c >= b){
    callback('hi')
  }else {
      setvisible(true)
  //  alert('You do not have sufficient balance. Please Recharge ')
  }


}

const getAge = (dateString) => {

    var today = new Date();
    var birthDate = new Date(dateString);

    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
const ok = () =>{

  setvisible(false)
}
useEffect(() => {


  Follow({host_id:GLOBAL.host.id})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {

      setCount(data.count_results)
      if (data.is_follow == "1"){
        isfollow(true)
      }
      else {
        isfollow(false)
      }

        //  this.setState({username:data.data})
        //  this.setState({wallet:data.data.wallet})
          //wallet





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });



  GetProfileApi({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
          // alert(JSON.stringify(data.data.is_vip_subscribe))
        //    GLOBAL.wallet = data.data.wallet
        setWallet(data.data.wallet)
        //  this.setState({username:data.data})
        //  this.setState({wallet:data.data.wallet})
          //wallet





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
  var k = GLOBAL.host.language.split(",")
  var es = ""
   for (var i = 0 ; i<k.length;i++){

     es = es + " " + capitalize(k[i]) + ","
   }
   es  = es.substring(1)
   setDetail(es == "" ? "" :es.substring(0, es.length - 1))

}, []);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <ImageBackground
          source={{uri:GLOBAL.host.imageUrl}}
          style={styles.bgimage}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
          <View style={styles.userView}>
              <View style={styles.userDot} />
              <Text style={styles.userName}>{GLOBAL.host.name}</Text>
              <View style={styles.ageView}>
                <Text style={styles.ageText}> Age {getAge(GLOBAL.host.dob)}</Text>
              </View>



            </View>
            <View style={styles.view_2}>
              <View style={styles.view_1}>
                <Text style={styles.text_1}>Id:{GLOBAL.host.id}</Text>
              </View>
              <View style={styles.view_1}>
                <Text style={styles.text_1}>Followers {count}</Text>
              </View>
  <TouchableOpacity onPress={() => follow()}>
              <LinearGradient
                colors={['#73005C', '#F00B51']}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 1}}
                style={styles.followView}>
                <Text style={styles.followText}> {follo == false ? "Follow" :"Unfollow"} </Text>
              </LinearGradient>
              </TouchableOpacity>
            </View>


        </ImageBackground>


        <View style={styles.view_3}>

        {GLOBAL.host.price == "0" && (
          <View style={styles.view_31}>

          </View>
        )}
          {GLOBAL.host.price != "0" && (
                 <View style={styles.view_31}>
                   <Image
                     style={styles.image31}
                     source={require('../resources/coin_.gif')}
                   />
                   <Text style={styles.text31}>{GLOBAL.host.price} / {GLOBAL.host.seconds} Seconds</Text>
                 </View>
               )}

               {check == false && (
                 <TouchableOpacity onPress={() => hi()}>
                   <LinearGradient
                     colors={['#73005C', '#F00B51']}
                     start={{x: 0, y: 0}}
                     end={{x: 1, y: 1}}
                     style={styles.view_32}>
                     <Image
                       style={styles.image32}
                       source={require('../resources/videocall.png')}
                     />
                     <Text style={styles.text32}>VIDEO CALL</Text>
                   </LinearGradient>
                 </TouchableOpacity>
               )}

                 </View>

        {GLOBAL.host.title != "" && (
          <View style={[styles.titleView, shadowStyle.s_1]}>
            <Text style={styles.title}>TITLE</Text>
            <Text style={styles.subTitle}>
            {GLOBAL.host.title}
            </Text>
          </View>
        )}





  {GLOBAL.host.description != "" && (
        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>DESCRIPTION</Text>
          <Text style={styles.subTitle}>
          {GLOBAL.host.description}
          </Text>
        </View>
      )}

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>TAG LINE</Text>
          <Text style={styles.subTitle}>
          {GLOBAL.host.tagline}
          </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>INTRO</Text>
          <Text style={styles.subTitle}>
          {GLOBAL.host.bio}
          </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>TIMINGS</Text>
          <Text style={styles.subTitle}>{GLOBAL.host.service_start_time} to {GLOBAL.host.service_end_time} (IST) </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>LANGUAGE KNOWN</Text>
          <Text style={styles.subTitle}>{detail}</Text>
        </View>
        {visible == true && (
          <View style = {{marginTop:200}}>

          <Modal
          animationType='slide'
       onRequestClose={() => setvisible(false)}
       presentationStyle="FormSheet"
       transparent
           style = {{height:300,width:300,backgroundColor:'transparent',marginTop:200}}
           isVisible={visible}>


               <View style = {{width:300,alignSelf:'center',backgroundColor:'transparent',borderRadius:20,marginTop:300}} >

               <Image
                   source={require('../resources/l-bg.png')}

                   style={{width: 280, height: 300,alignSelf:'center',marginLeft:0,marginTop:-26,borderRadius:32}}


               />


                   <Image
                       source={require('../resources/logo.png')}

                       style={{width: 80, height: 80,alignSelf:'center',resizeMode:'contain',marginLeft:0,marginTop:-260}}


                   />




             <Text style={{fontSize:22,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:34,marginTop:1}}>You do not have sufficient balance</Text>
             <Text style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:-26}}>Please recharge </Text>





            <TouchableOpacity style = {{backgroundColor:'black',width:100,borderRadius:12,height:40,alignSelf:'center'}}>
            <View >
              <Text onPress={() => ok()} style={{fontSize:16,fontFamily:'DMSans-Bold',fontWeight:'normal',textAlign:'center',color:'white',margin:14,marginTop:9}}>OK</Text>
            </View>
            </TouchableOpacity>





               </View>

          </Modal>
          </View>
        )}



        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>LOCATION</Text>
          <Text style={styles.subTitle}>{GLOBAL.host.city}</Text>
        </View>


      </ScrollView>



    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgimage: {
    height: 290,
    width: '100%',
  },
  backImage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginTop: 40,
    marginHorizontal: 20,
  },
  view_1: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#0000004d',
    marginRight: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
  text_1: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  view_2: {
    flexDirection: 'row',
    margin: 20,
    marginTop: 5,
  },
  followView: {
    borderRadius: 12.5,
    paddingHorizontal: 5,
    paddingVertical: 2,
    marginLeft: 'auto',
  },
  followText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 13,
    color: '#FFF',
  },
  coinImage: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  coinView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 1,
    borderRadius: 10,
    marginLeft: 10,
  },
  coinText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 10,
    color: '#FFFFFF',
    marginLeft: 5,
  },
  levelText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  ageText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  ageView: {
    backgroundColor: '#8B572A',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginLeft: 10,
    marginTop:2
  },
  levelView: {
    backgroundColor: '#9013FE',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginLeft: 10,
  },
  userName: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 20,
    color: '#FFFFFF',
    marginLeft: 6,
  },
  userDot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#43FA00',
  },
  userView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 'auto',
  },
  view_3: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    marginBottom: 15,
    backgroundColor: 'white',
  },
  view_31: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft:0
  },
  view_32: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 24,
    padding: 10,
    marginLeft:40
  },
  text31: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 18,
    color: '#EA0B52',
    marginLeft: 6,
  },
  text32: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 14,
    color: '#FFFFFF',
    marginLeft: 6,
  },
  image31: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  image32: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  title: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 14,
    color: '#D50853',
  },
  subTitle: {
    fontFamily: 'Nunito',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 23,
    color: '#262628',
  },
  titleView: {
    borderRadius: 8,
    marginHorizontal: 20,
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'white',
    borderRadius: 15,
  },
  videoImage: {
    height: 100,
    width: 88,
    borderRadius: 10,
    overflow: 'hidden',
  },
  overlayview: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e60a5273',
  },
  videoImage2: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },
  view_5: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 20,
  },
});
export default ViewProfile1;
