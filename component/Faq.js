import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ScrollView,
  Clipboard,
  Dimensions,
  Share
} from 'react-native';
const window = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi,FetchFaq} from '../backend/Api';
import {textStyle, viewStyle} from '../style/Style';
import * as actions from '../redux/actions';
import {settingList} from '../backend/Config';
import HTML from "react-native-render-html";



const Faq = ({navigation}) => {
  const [username, setUsername] = useState([]);
    const [selectedId, setSelectedId] = useState(true);
  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
      FetchFaq({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {


var array = []
                for (var i = 0 ; i < data.data.length ; i++){
                  var dict = data.data[i]
                  dict["is_selected"] = ""
                  array.push(dict)
                }


                 setUsername(array)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);

  useEffect (() =>{
     console.log(selectedId?'tt':'ff')
  setSelectedId(!selectedId)
},[username])

const detail = (item,index) =>{

  var a  = username[index]



  if (item.is_selected == ''){
     a.is_selected = 'Y'
     username[index] = a


  }else {
     a.is_selected = ''
        username[index] = a
  }


setUsername(username)

    setSelectedId(!selectedId)



}

  const renderItemProduct = ({item,index}) => {


       return (
   <TouchableOpacity onPress= {()=>detail(item,index)}>
           <View style={{backgroundColor:'white',color :'white', flex: 1,borderWidth:1,borderColor:'#f1f1f1' ,margin: 10,borderRadius :9,width : window.width - 30, shadowColor: '#000',
               shadowOffset: { width: 0, height: 1 },
               shadowOpacity: 0.2,
               shadowRadius: 2,
               elevation: 5 }}>

               <View style = {{flexDirection:'row',justifyContent:'space-between'}}>


               <Text style={{fontFamily:'DMSans-Bold',color:'#2c2e30',fontSize:19,marginLeft:6,marginTop:12,marginBottom:12,width:window.width - 80,lineHeight:23}}>
               {item.question}

               </Text>
               {item.is_selected == "" && (
                 <Image
                     source={require('./downarrow.png')}
                     style={{width: 18, height: 20,marginRight:20,marginTop:12,resizeMode:'contain'}}


                 />
               )}

               {item.is_selected != "" && (
                 <Image
                     source={require('./uparrow.png')}
                     style={{width: 18, height: 20,marginRight:20,marginTop:12,resizeMode:'contain'}}


                 />
               )}







</View>



 {item.is_selected != "" && (
<View>

   <Text style={{fontFamily:'DMSans-Regular',color:'#2c2e30',fontSize:19,marginLeft:6,marginTop:12,width:window.width - 50,textAlign:'justify',lineHeight:25,marginBottom:12}}>
   {item.answer}

   </Text>

   </View>
 )}








           </View>
           </TouchableOpacity>

       )
   }



  const copyToClipboard = () => {
   Clipboard.setString(username.referral_code)
 }

const  fancyShareMessage=()=>{

    var a = username.referral_code
    var commonHtml = `I want to recommend Cleo Live - mobile application.\n\nAt Cleo Live you can Play Live Games with Live Girls and Win lots of Money.\n\nLogin Every day and get Free Coins to Play.\n\nDownload Cleo Live from http://mepllive.com.\n\nWhile Registering use my referral code ${a} and get Free Coins`;

    Share.share({
            message:commonHtml
        },{
            tintColor:'green',
            dialogTitle:'Share this Referral Code via....'
        }
    );
}


  const onPressHandler = (key) => {
    switch (key) {
      case 'logout':
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Introduction'}],
                 });
        break;
      default:
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>FAQ</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>


      <FlatList
                        data={username}
                        extraData={selectedId}


                        renderItem={renderItemProduct}
                    />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  appVersion: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
    alignSelf: 'center',
    opacity: 0.7,
    marginTop: 47,
  },
  view: {
    flexDirection: 'row',
    borderBottomColor: '#F5F5F5',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  image: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 12,
  },
  text: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
  },
  linearGradient: {
    flex: 1,
  },
});

export default Faq;
