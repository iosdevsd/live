import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
var validator = require("email-validator");
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Forgotv} from '../backend/Api';


import DeviceInfo from 'react-native-device-info';
import {radioStyle, textStyle, viewStyle} from '../style/Style';
import * as actions from '../redux/actions';
const Reset = ({navigation, route}) => {
  const [state, setState] = useState({
  loading: false,



});
  const toggleLoading = (bol) => setState({...state, loading: bol});
  const [userState, setUserState] = useState({
    dp: '',
    name: '',
    email: '',
    mobile: '',
    gender: 0,
    password:'',
    confirm:'',
    referal:'',
  });
  const submitHandler = () => {
     navigation.navigate('Otp');

  };
  const onPressBack = () => navigation.goBack();
  const onRadioPress = (value) => setUserState({...userState, gender: value});
  var radio_props = [
    {label: 'Male', value: 0},
    {label: 'Female', value: 1},
  ];

  const onPress = () => {


    if (userState.password == ""){
        Toast.showWithGravity('Please enter password', Toast.LONG, Toast.CENTER);
        return
    }

    if (userState.confirm == ""){
      Toast.showWithGravity('Please enter Confirm Password ', Toast.LONG, Toast.CENTER);
      return
    }
    if (userState.confirm != userState.password ){
      Toast.showWithGravity('Password and Confirm Password not match ', Toast.LONG, Toast.CENTER);
      return
    }
    toggleLoading(true);

    const json = {
  id: route.params.item.id,
  otp: route.params.item.otp,
  "password":userState.password,
      "confirm_password":userState.confirm


};
console.log(JSON.stringify(json))

Forgotv(json)
       .then((data) => {
            toggleLoading(false);

         if (data.status) {
          //
      Toast.showWithGravity('Password reset sucessfully  ', Toast.LONG, Toast.CENTER);
          navigation.reset({
                          index: 0,
                          routes: [{name: 'Introduction'}],
                        });




         } else {
    Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };

  const onPresss = () => {
navigation.goBack()


  };
  useEffect(() => {
    console.log(userState);
  }, [userState]);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {state.loading && <Loader />}
        <StatusBar
          barStyle="light-content"
          backgroundColor="transparent"
          translucent={true}
        />
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={viewStyle.simpleHeaderView}>
          <Text style={textStyle.simpleHeaderTitle}>Reset Password</Text>
          <View style={styles.backView}>
            <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
              <Image
                source={require('../resources/back.png')}
                style={styles.backImage}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>

<View style = {{marginTop:50}}/>




        <Text style={styles.text_3}>Password </Text>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => setUserState({...userState, password: text})}
          value={userState.password}
          secureTextEntry = {true}

        />
        <Text style={styles.text_3}>Confirm Password </Text>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => setUserState({...userState, confirm: text})}
          value={userState.confirm}
          secureTextEntry = {true}

        />




        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={styles.linearGradient}>
          <TouchableOpacity style={styles.touchable} onPress={onPress}>
            <Text style={styles.submitText}>Submit</Text>
          </TouchableOpacity>
        </LinearGradient>



      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  linearGradient: {
    margin: 25,
    borderRadius: 25,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  submitText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },

  imageBg: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    marginTop: 46,
    marginBottom: 30,
  },
  image: {
    borderRadius: 75,
  },
  editImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    width: '82%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
    marginBottom: 15,
  },
  date: {},
});
export default Reset;
