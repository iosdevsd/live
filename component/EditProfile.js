import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import store from '../redux/store';
import Loader from '../utils/Loader';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
const options = {
  title: 'Select Image',
  customButtons: [],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi} from '../backend/Api';
import {radioStyle, textStyle, viewStyle} from '../style/Style';
const EditProfile = ({navigation}) => {
    const [username, setUsername] = useState({});
  const [booking, setBooking] = useState(0);
  const [state, setState] = useState({
  loading: false,



});
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const toggleLoading = (bol) => setState({...state, loading: bol});
    const [avtar, setAvtar] = useState('');
      const [flag, setFlag] = useState(0);
  const [date, setDate] = useState(new Date());

  const onPress = () => {
    if (flag == 0){
        toggleLoading(true);
  const url = 'http://51.79.250.86/roulette/api/edit_profile'
           const data = new FormData();
           // const data = new FormData();
                         data.append('user_id',store.getState().user.id);
                         data.append('name',name);
                         data.append('gender',"Male");
                               data.append('flag',0);



console.log(JSON.stringify(data))




           // you can append anyone.

           fetch(url, {
               method: 'post',
               body: data,
               headers: {
                 'Content-Type': 'multipart/form-data',
               }

           }).then((response) => response.json())
               .then((responseJson) => {
                // alert(JSON.stringify(responseJson))
  toggleLoading(false);
if (responseJson.status == true){



navigation.goBack()
}else{
alert(responseJson.message)
}




               }).catch((error)=>{
                 this.hideLoading()
  console.log("Api call error");
  alert(error.message);
})
}


if (flag == 1){
  toggleLoading(true);
const url = 'http://51.79.250.86/roulette/api/edit_profile'
       const data = new FormData();
       // const data = new FormData();
                     data.append('user_id',store.getState().user.id);
                     data.append('name',name);
                     data.append('gender',"Male");
                           data.append('flag',1);

                           data.append('image', {
                                              uri: avtar,
                                              type: 'image/jpg', // or photo.type
                                              name: 'image.jpg'
                                          });


console.log(JSON.stringify(data))




       // you can append anyone.

       fetch(url, {
           method: 'post',
           body: data,
           headers: {
             'Content-Type': 'multipart/form-data',
           }

       }).then((response) => response.json())
           .then((responseJson) => {
            // alert(JSON.stringify(responseJson))
  toggleLoading(false);
if (responseJson.status == true){



navigation.goBack()
}else{
alert(responseJson.message)
}




           }).catch((error)=>{
             this.hideLoading()
console.log("Api call error");
alert(error.message);
})
}
  };
  const onPressBack = () => navigation.goBack();
  const onRadioPress = (value) => setBooking(value);
  var radio_props = [
    {label: 'Male', value: 0},
    {label: 'Female', value: 1},
  ];
  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 setUsername(data.data)
                 setName(data.data.name)
                 setEmail(data.data.email)
                 setMobile(data.data.phone)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);

  const handlePressd = () => {


        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                setAvtar(response.uri)
                // this.setState({image:response.uri})
                //  this.setState({flag:1})
//"puja_id":this.props.navigation.state.params

setFlag(1)
            }

        });
    }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Edit Profile</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
  {state.loading && <Loader />}
{avtar == "" && (
  <ImageBackground
    style={styles.imageBg}
    imageStyle={styles.image}
    resizeMode="cover"
     source = {{uri:username.imageUrl}}>
    <TouchableOpacity
    onPress={()=> handlePressd()}>
      <Image
        style={styles.editImage}
        source={require('../resources/setting/edit.png')}
      />
    </TouchableOpacity>
  </ImageBackground>
)}

{avtar != "" && (
  <ImageBackground
    style={styles.imageBg}
    imageStyle={styles.image}
    resizeMode="cover"
     source = {{uri:avtar}}>
    <TouchableOpacity
    onPress={()=> handlePressd()}>
      <Image
        style={styles.editImage}
        source={require('../resources/setting/edit.png')}
      />
    </TouchableOpacity>
  </ImageBackground>
)}


      <Text style={styles.text_3}>Name</Text>
      <View style={styles.outerView}>
        <TextInput
          style={styles.textInput}
          keyboardType="default"
             onChangeText={(text) => setName(text)}
             value={name}
          maxLength={30}
        />
        <Image
          source={require('../resources/setting/edit1.png')}
          style={styles.editImage2}
        />
      </View>
      <Text style={styles.text_3}>Email</Text>
      <View style={styles.outerView}>
        <TextInput
          style={styles.textInput}
          keyboardType="email-address"
             onChangeText={(text) => setEmail(text)}
             value={email}
             editable = {false}
          maxLength={50}
        />

      </View>

      <Text style={styles.text_3}>Mobile No.</Text>
      <View style={styles.outerView}>
        <TextInput
          style={styles.textInput}
          keyboardType="numeric"
           onChangeText={(text) => setMobile(text)}
           value={mobile}
            editable = {false}
          //   onChangeText={(text) => setState({...state, mobile: text})}
          maxLength={10}
        />

      </View>


      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
          <Text style={styles.submitText}>Save</Text>
        </TouchableOpacity>
      </LinearGradient>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginVertical: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  submitText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },

  imageBg: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    marginTop: 46,
    marginBottom: 30,
  },
  image: {
    borderRadius: 75,
  },
  editImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    flex: 1,
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    alignSelf: 'center',
  },
  outerView: {
    flexDirection: 'row',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
    marginBottom: 20,
    width: '82%',
    alignSelf: 'center',
  },
  editImage2: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginHorizontal: 5,
  },
  view_1: {
    flexDirection: 'row',
  },
});
export default EditProfile;
