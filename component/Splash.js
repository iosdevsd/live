import React ,{ useEffect } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,PermissionsAndroid} from 'react-native';
import style from '../style/Style.js';
import { AsyncStorageGetUser,AsyncStorageGettoken } from '../backend/Api';
import * as actions from '../redux/actions';
import KeepAwake from 'react-native-keep-awake';
import Geolocation from '@react-native-community/geolocation';
import { getAppstoreAppMetadata } from "react-native-appstore-version-checker";
import VersionNumber from 'react-native-version-number';
import LinearGradient from 'react-native-linear-gradient';
const GLOBAL = require('./Global');
import store from '../redux/store'
const Splash = ({navigation}) => {
  const screenHandler = async () => {
     actions.Login(JSON.parse(await AsyncStorageGetUser()))
      actions.Token(await AsyncStorageGettoken())
    setTimeout(() => {
      const user = store.getState().user
  if (Object.keys(user).length === 0 && user.constructor === Object) {

    navigation.replace('Introduction1')
  } else {
    navigation.replace('TabNavigator')
  }
}, 1000);

  }
  const hello = (info) =>{
  GLOBAL.gllat = info.coords.latitude
  GLOBAL.gllong = info.coords.longitude

}
  useEffect(() => {

  const granted =   PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );

if (granted) {
  console.log( "You can use the ACCESS_FINE_LOCATION" )

  Geolocation.getCurrentPosition(
    position => {
      const initialPosition = JSON.stringify(position);
    hello(position)
    },
    error => console.log('Error', JSON.stringify(error)),
    {enableHighAccuracy: false, timeout: 20000, maximumAge: 10000},
  );
}
else {
//  alert('Please enable Location.')
  //console.log( "ACCESS_FINE_LOCATION permission denied" )
}



        getAppstoreAppMetadata("com.cleolive") //put any apps packageId here
 .then(metadata => {

     var currentVerNum = (VersionNumber.appVersion).split('-')[0];
   if (currentVerNum != metadata.version){
     console.log(currentVerNum)
     console.log(metadata.version)
     alert('Please update your app.')
     return
   }
   else {

   }
   console.log(
     "clashofclans android app version on playstore",
     metadata.version,
     "published on",
     metadata.currentVersionReleaseDate
   );
 })
 .catch(err => {
   console.log("error occurred", err);
 });
    KeepAwake.activate();
    screenHandler()
  }, []);
  return (
    <LinearGradient
      colors={['#73005C', '#F00B51']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}
      style={{flex:1}}>
      <Image style = {{width:200,height:200,resizeMode:'contain',alignSelf:'center',marginTop:'40%'}}
      source = {require('../resources/logo.png') }/>

      <Text style={{  fontFamily: 'DMSans-Bold',
        fontWeight: '800',
        fontSize: 45,
        textAlign:'center',
        marginTop:7,
        color: 'white'}}>Cleo Live</Text>
        <Text style={{  fontFamily: 'DMSans-Italic',
          fontWeight: '800',
          fontSize: 19,
          textAlign:'center',
          marginTop:-10,
          color: 'white'}}>Fun So Real, It's Unreal</Text>



    </LinearGradient>
  );
};

export default Splash;
