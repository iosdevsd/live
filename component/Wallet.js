import React ,{ useEffect ,useState,useRef} from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  Dimensions,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
//AppUser
const window = Dimensions.get('window');
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import User from './User';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import * as RNLocalize from "react-native-localize";
import RBSheet from "react-native-raw-bottom-sheet";
import RazorpayCheckout from 'react-native-razorpay';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend,GetProfileApi,RechargeWallet,Tranfer,FetchCoin} from '../backend/Api';
const Wallet = ({navigation}) => {
  const [username, setUsername] = useState({});
  const [account, setAccount] = useState('');
  const [coin, setCoin] = useState('');
  const [coinq, setCoinq] = useState([]);
  //setAccount
  //setCoin
    const [visible, setvisible] = useState(false);
      const [visible1, setvisible1] = useState(false);
  const ref = useRef()
  useEffect(() => {


    FetchCoin({user_id:"1"})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {
               console.log(JSON.stringify(data))
               setCoinq(data.data)
              // alert(JSON.stringify(data))
              //setUsername(data.data)





             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });

    const unsubscribew =   navigation.addListener('focus', () => {
      GetProfileApi({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 setUsername(data.data)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);
  const change = (item) =>{




  var amount = parseInt(item.amount) * 100
  var options = {
 description: 'Purchasing',

 currency: 'INR',
 key: 'rzp_live_KbP8LCqPVe4f1E', // Your api key
 amount: amount.toString(),
 name:  username.name,
 prefill: {
   email: username.email,
   contact: username.phone,
   name: username.name
 },
 theme: {color: '#F00B51'}
}
RazorpayCheckout.open(options).then((data) => {
 // handle success
 success(data.razorpay_payment_id,item)
// alert(`Success: ${data.razorpay_payment_id}`);
}).catch((error) => {
 // handle failure
 alert(`Error: ${error.code} | ${error.description}`);
});
}


const sendInput = () =>{

if (coin == ""){
 Toast.showWithGravity('Please Fill in the coins to be transferred', Toast.LONG, Toast.CENTER);
}else {
var a = parseInt(coin)
var b = parseInt(username.wallet)

if (b >= a){

  Tranfer({transfer_to_id:account,transfer_amount:coin})
         .then((data) => {
            //   toggleLoading(false);

           if (data.status) {
             console.log(JSON.stringify(data))
             callback()
             setvisible(false)
             setvisible1(true)

            // alert(JSON.stringify(data))
            //setUsername(data.data)





           } else {
             setvisible(false)
               Toast.showWithGravity('The Account Id entered is incorrect – Please check', Toast.LONG, Toast.CENTER);
  //alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
  //callback()
}else {
Toast.showWithGravity('The amount exceeds your wallet balance', Toast.LONG, Toast.CENTER);
}

}
}

const success = (id,item)=>{
var c = {txn_id:id,recharge_amount:item.benefit}

console.log(JSON.stringify(c))
  RechargeWallet({txn_id:id,recharge_amount:item.benefit})
         .then((data) => {
            //   toggleLoading(false);
 console.log(JSON.stringify(data))
           if (data.status) {
             console.log(JSON.stringify(data))

             GetProfileApi({user_id:"1"})
                    .then((data) => {
                       //   toggleLoading(false);

                      if (data.status) {
                        console.log(JSON.stringify(data))
                        setUsername(data.data)
                       // alert(JSON.stringify(data))
                       //setUsername(data.data)





                      } else {
             alert(JSON.stringify(data.message))
                      }
                    })
                    .catch((error) => {
                      //toggleLoading(false);
                      console.log('error', error);
                    });




           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
  const itemData = [
    {
      id: 1,
      coin: 100,
      price: 100,
    },
    {
      id: 2,
      coin: 200,
      price: 200,
    },
    {
      id: 3,
      coin: 300,
      price: 300,
    },
    {
      id: 4,
      coin: 500,
      price: 500,
    },
    {
      id: 5,
      coin: 1000,
      price: 1000,
    },
  ];

  const callback = () =>{
    ref.current.close()
    GetProfileApi({user_id:"1"})
           .then((data) => {
              //   toggleLoading(false);

             if (data.status) {
               console.log(JSON.stringify(data))
               setUsername(data.data)
              // alert(JSON.stringify(data))
              //setUsername(data.data)





             } else {
    alert(JSON.stringify(data.message))
             }
           })
           .catch((error) => {
             //toggleLoading(false);
             console.log('error', error);
           });
  }
  return (
    <SafeAreaView style={styles.container}>
      {/* <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      /> */}
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Wallet</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.view_1}>
        <View style={styles.view_2}>
          <Image
            style={styles.coinImage}
            source={require('../resources/coin.png')}
          />
          <Text style={styles.coinText}>{username.wallet}</Text>
        </View>
        <Text style={styles.coinText_2}>Current Coins</Text>
      </LinearGradient>
      <View style={styles.view_3}>
        <Text onPress={() => navigation.navigate('Top')} style={styles.text_3}>View topup history</Text>
        <Text onPress={() => navigation.navigate('Wallets')} style={styles.text_3}>Transactions</Text>
        <Text onPress={() => setvisible(true)} style={styles.text_3}>Transfer Coins</Text>
      </View>
      <Text onPress={() => navigation.navigate('Offline')} style={{    fontFamily: 'DMSans-Bold',
          fontWeight: '600',
          fontSize: 13,
          fontStyle: 'normal',
          color: '#DD0952',
          textDecorationLine: 'underline',
        alignSelf:'center',marginTop:16}}>Offline Recharge</Text>
      <Text style={styles.text_1}>Choose Coins Pack</Text>

      {coinq.map((item) => (
        <TouchableOpacity

          onPress={() => change(item)}>
        <View style={styles.view_4} key={item.id.toString()}>
          <View style={styles.view_5}>
            <Image
              style={styles.image_1}
              source={require('../resources/coin.png')}
            />
            <Text style={styles.text_4}>{item.benefit}</Text>
          </View>
          <View>
          {RNLocalize.getTimeZone() == "Asia/Kolkata" && (
            <LinearGradient
              colors={['#73005C', '#F00B51']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              style={styles.view_6}>
                <Text style={{color:'white',fontFamily: 'DMSans-Bold',
                fontWeight: '700',
                fontSize: 18,
                fontStyle: 'normal',
                color: '#FFFFFF',marginLeft:20}}>{`₹`}</Text>
              <Text style={styles.text_5}>{`${item.amount}`}</Text>
            </LinearGradient>
          )}
    {RNLocalize.getTimeZone() != "Asia/Kolkata" && (
          <LinearGradient
            colors={['#73005C', '#F00B51']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.view_6}>
            <Text style={{color:'white',fontFamily: 'DMSans-Bold',
            fontWeight: '700',
            fontSize: 18,
            fontStyle: 'normal',
            color: '#FFFFFF',marginLeft:20}}>{`$`}</Text>
            <Text style={styles.text_5}>{`${item.amount_usd}`}</Text>
          </LinearGradient>
        )}

          </View>
        </View>
        </TouchableOpacity>
      ))}
      <RBSheet
ref={ref}
closeOnDragDown={true}
height={window.height - 90}
openDuration={250}
customStyles={{
container: {
justifyContent: "center",
alignItems: "center"
}
}}
>
<User usernamew = {username} callback = {callback}/>
</RBSheet>


<Dialog dialogStyle = {{width:300,borderRadius: 22}}
   visible={visible1}
   style = {{backgroundColor:'red'}}


   onTouchOutside={() => {
     setvisible1(false)

   }}
 >
   <DialogContent  >


<Text style={{  marginLeft: 10,marginTop:20,
  fontFamily: 'DMSans-Bold',
  fontWeight: '800',
  fontSize: 20,
  fontStyle: 'normal',
  color: 'black',}}>Transfer Successful</Text>

  <Text style={styles.coinText_2t}>You have successfully transferred {coin} coins to user {account} on {moment().format("DD,MMMM")} at {moment().format("hh:mm: a")}</Text>


  <View style = {{flexDirection:'row',marginTop:20}}>



    <TouchableOpacity

      onPress={() => setvisible1(false)}>
  <LinearGradient
    colors={['#73005C', '#F00B51']}
    start={{x: 0, y: 0}}
    end={{x: 1, y: 1}}
    style={{height:40,width:220,borderRadius:22,marginLeft:12,alignSelf:'center'}}>
<Text style={{color:'white',fontFamily:'DMSans-Bold',fontSize:14,textAlign:'center',marginTop:9}}>DONE</Text>

    </LinearGradient>
    </TouchableOpacity>
  </View>

   </DialogContent>
 </Dialog>

<Dialog dialogStyle = {{borderRadius: 30,width:window.width-70,height:260}}
   visible={visible}
   rounded = {true}
   style = {{backgroundColor:'red',borderRadius: 200}}


   onTouchOutside={() => {
     setvisible(false)

   }}
 >
   <DialogContent dialogStyle = {{width:window.width-30,height:220,borderRadius: 200}} >


<Text style={{  marginLeft: 0,marginTop:12,
  fontFamily: 'DMSans-Bold',
  fontWeight: '800',
  fontSize: 17,
  marginTop:20,
  fontStyle: 'normal',
  color: 'black',}}>Transfer Coins</Text>

  <TextField
      label='Account Id'
      baseColor = '#acb1c0'
      tintColor = '#acb1c0'
      value = {account}
      onChangeText={(text) => setAccount(text)}


  />

  <TextField
      label='Enter Coin Amount'
      baseColor = '#acb1c0'
      tintColor = '#acb1c0'
      value = {coin}
      onChangeText={(text) => setCoin(text)}


  />





  <View style = {{flexDirection:'row',marginTop:20,alignSelf:'center',width:'90%',justifyContent:'space-between'}}>

  <TouchableOpacity

    onPress={() => setvisible(false)}>
  <View

    style={{height:35,width:120,borderRadius:50,borderWidth:1,borderColor:'#F00B51'}}>
<Text style={{color:'#F00B51',fontFamily:'DMSans-Bold',fontSize:14,textAlign:'center',marginTop:7}}>Cancel</Text>

    </View>
    </TouchableOpacity>

    <TouchableOpacity

      onPress={() => sendInput()}>
  <LinearGradient
    colors={['#73005C', '#F00B51']}
    start={{x: 0, y: 0}}
    end={{x: 1, y: 1}}
    style={{height:35,width:120,borderRadius:50,marginLeft:12}}>
<Text style={{color:'white',fontFamily:'DMSans-Bold',fontSize:14,textAlign:'center',marginTop:7}}>Confirm</Text>

    </LinearGradient>
    </TouchableOpacity>
  </View>

   </DialogContent>
 </Dialog>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  view_1: {
    marginTop: 23,
    marginHorizontal: 23,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  view_2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    height: 44,
    width: 44,
  },
  coinText: {
    marginLeft: 7,
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 40,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  coinText_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '500',
    fontSize: 16,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  coinText_2t: {
    fontFamily: 'DMSans-Regular',
    fontWeight: '500',
    fontSize: 16,
    fontStyle: 'normal',
    color: 'grey',
    marginTop:10,
    marginLeft:12
  },
  view_3: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 7,
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 13,
    fontStyle: 'normal',
    color: '#DD0952',
    textDecorationLine: 'underline',
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#262628',
    marginTop: 34,
    alignSelf: 'center',
  },
  view_4: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#EFEFEF',
    paddingVertical: 10,
  },
  view_5: {
    flexDirection: 'row',
    marginLeft: 20,
    alignItems: 'center',
    width:"60%"
  },
  image_1: {
    height: 25,
    width: 25,
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 20,
    fontStyle: 'normal',
    color: '#262628',
    marginLeft: 7,
    width:80,
    textAlign:'right'
  },
  text_5: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#FFFFFF',
  width:50,
    textAlign:'right'
  },
  textInput: {
    height: 40,
    width: '90%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 15,
    color: '#FFFFFF',
  },
  view_6: {
    borderRadius: 24,
    width: 100,
    paddingVertical: 7,
    marginLeft: 'auto',
    marginRight: 20,
    alignItems: 'center',
    marginTop:5,
    flexDirection:'row'
  },
});
export default Wallet;
