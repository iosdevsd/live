import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  NativeModules,
  TextInput,
  Dimensions,
  StatusBar,
} from 'react-native';
  var location = {}
import DatePicker from 'react-native-datepicker';
import requestCameraAndAudioPermission from './permission';
import DeviceInfo from 'react-native-device-info';
const {Agora} = NativeModules;
import Toast from 'react-native-simple-toast';
import Geolocation from '@react-native-community/geolocation';
const window = Dimensions.get('window');
const {
 FPS30,
 AudioProfileDefault,
 AudioScenarioDefault,
 Host,
 Audience,
 Adaptative
} = Agora;
import SegmentedControl from '@react-native-segmented-control/segmented-control';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

const GLOBAL = require('./Global');
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin,AddkMember} from '../backend/Api';
import * as actions from '../redux/actions';
const Homes = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [location, setLocation] = useState({});
  const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [selectedIndex,setselectedIndex] = useState(0);
    const [selectedIndexs,setselectedIndexs] = useState(0);
  const [keyTypeNum, setKeyTypeNum] = useState(false);
  const onPress = () => {
    if (username == ""){
      Toast.showWithGravity('Please enter Description', Toast.LONG, Toast.CENTER);
      return
    }
    if (password == ""){
        Toast.showWithGravity('Please enter Short Description', Toast.LONG, Toast.CENTER);
    }
console.log('hi')
  //  console.log(location)

    const json = {
  title: username,
  description: password,
  is_vip: selectedIndex,
  "location":"",
  latitude: '',
  longitude: '',
  start_time: date + time,

};
console.log(JSON.stringify(json))

AddkMember(json)
       .then((data) => {
          //   toggleLoading(false);

         if (data.status) {
           console.log(JSON.stringify(data))
          // alert(JSON.stringify(data))
          GLOBAL.bookingid = data.data.bridge_id
                          navigation.navigate("LiveVideoCall", {
                                                      uid: Math.floor(Math.random() * 100),
                                                      clientRole: Host,
                                                      channelName: data.data.bridge_id,
                                                      onCancel: (message) => {

                        }})




         } else {
alert(JSON.stringify(data.message))
         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };
  useEffect (() =>{

    Geolocation.getCurrentPosition(
          position => {
            const initialPosition = JSON.stringify(position);
            setLocation(initialPosition)
            console.log(initialPosition);
          },
          error => Alert.alert('Error', JSON.stringify(error)),
          {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
  })
  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Create Game</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
              source = {require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <KeyboardAwareScrollView>
      <Text style={styles.text_1}> Hi , Welcome back!</Text>
      <Text style={{color: '#8E9198',fontFamily:'DMSans-Regular',fontSize: 18,marginLeft:28,marginTop:-40,width:window.width - 90,lineHeight:23}}>
                      Please write Detailed Description

                   </Text>


                   <TextInput
                                                       style={{height: 50,width:'90%',margin:2,marginLeft:0,backgroundColor:'white',borderWidth:0.4,alignSelf:'center',marginTop:10,borderColor:'grey',borderRadius:6}}
                                                       placeholder ={''}
                                                        multiline={true}

                                                       textAlignVertical={'top'}
                                                       placeholderTextColor = {'#bebebe'}
                                                       placeholderStyle = {{marginLeft:50}}
                                                       onChangeText={(text) => {

                                                         setUsername(text);
                                                       }}

                                                                                         value={username}

                                                       />

                                                       <Text style={{color: '#8E9198',fontFamily:'DMSans-Regular',fontSize: 18,marginLeft:28,marginTop:10,width:window.width - 90,lineHeight:23}}>
                                                                        Please write Short Description (max 50 characters)

                                                                    </Text>

                                                                    <TextInput
                                                                                                        style={{height: 50,width:'90%',margin:2,marginLeft:0,backgroundColor:'white',borderWidth:0.4,alignSelf:'center',marginTop:10,borderColor:'grey',borderRadius:6}}
                                                                                                        placeholder ={''}
                                                                                                         multiline={true}

                                                                                                        textAlignVertical={'top'}
                                                                                                        placeholderTextColor = {'#bebebe'}
                                                                                                        placeholderStyle = {{marginLeft:50}}
                                                                                                        onChangeText={(text) => {

                                                                                                          setPassword(text);
                                                                                                        }}

                                                                                                                                          value={password}

                                                                                                        />

                                                                                                                  <Text style={{color: '#8E9198',fontFamily:'DMSans-Regular',fontSize: 18,marginLeft:28,marginTop:30,width:window.width - 90,lineHeight:23}}>
                                                                                                                                  Is this Broadcast only for VIP?

                                                                                                                               </Text>
                                                                                                                                 <View style = {{width:window.width - 200 ,marginTop:15,marginLeft:15}}>

                                                                                                                               <SegmentedControl
   values={['No', 'Yes']}
   selectedIndex={selectedIndex}
   onChange={(event) => {
     setselectedIndex(event.nativeEvent.selectedSegmentIndex);
   }}
 />
 </View>
 <Text style={{color: '#8E9198',fontFamily:'DMSans-Regular',fontSize: 18,marginLeft:28,marginTop:30,width:window.width - 90,lineHeight:23}}>
                 If you want disable your Location?

              </Text>
              <View style = {{width:window.width - 200 ,marginTop:15,marginLeft:15}}>

              <SegmentedControl
values={['No', 'Yes']}
selectedIndex={selectedIndexs}
onChange={(event) => {
setselectedIndexs(event.nativeEvent.selectedSegmentIndex);
}}
/>
</View>


      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
          <Text style={styles.text_3}>Next</Text>
        </TouchableOpacity>
      </LinearGradient>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 55,
    marginBottom: 42,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export default Homes;
