import React, {useEffect} from 'react';
import {Image, ImageBackground, StyleSheet, View,Text,TouchableOpacity} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import { AsyncStorageGetUser,AsyncStorageGettoken } from '../backend/Api';
import * as actions from '../redux/actions';
import store from '../redux/store';
import LinearGradient from 'react-native-linear-gradient';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
// import {useDispatch} from 'react-redux';
// import {Api, AS, Auth} from '../backend/Api';
// import * as Actions from '../redux/actions';

const Thankyou = ({navigation}) => {
  // const dispatch = useDispatch();
  const change = async () => {
       //  stringsoflanguages.setLanguage('hi')

       navigation.reset({
                       index: 0,
                       routes: [{name: 'TabNavigator'}],
                     });
     //  SoundPlayer.playSoundFile('ring', 'mp3')
   //  navigation.navigate('AstrologerFilter')
   //  navigation.replace('TabNavigator')
   // navigation.navigate('PujaFilter')
    //navigation.replace('Home')
   //navigation.replace('Horoscope')
     // navigation.replace('Introduction')
   //  navigation.replace('Home')


   }


  useEffect(() => {
  //  screenHandler();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBarLight />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={{flex:1}}>


          <Text style={{alignSelf:'center',color:'white',marginTop:300,fontSize:18,fontFamily:"DMSans-Bold"}}>VIP USER</Text>
                    <Text style={{alignSelf:'center',color:'white',marginTop:12,fontSize:18,fontFamily:"DMSans-Bold",width:'70%',alignSelf:'center',textAlign:'center'}}>We are delighted to inform you that you have become a VIP User</Text>
  <TouchableOpacity  onPress={() => change()}>
<View style = {{width:160,height:40,alignSelf:'center',borderWidth:1,borderRadius:13,borderColor:'white',marginTop:30}}>
          <Text style={{alignSelf:'center',color:'white',marginTop:9,fontSize:12,fontFamily:"DMSans-Bold"}}>Go To Home</Text>
</View>
</TouchableOpacity>
      </LinearGradient>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#73005C'
  },
  iconImage: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});

export default Thankyou;
