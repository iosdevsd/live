import React, {useRef} from 'react';
import Splash from './Splash.js';
import Introduction from './Introduction.js';
import Main from './Main.js';
import Login from './Login.js';
import Rule from './Rule.js';
import Bet1 from './Bet1.js';
import Offline from './Offline.js';
import LeaderBoard from './LeaderBoard.js';
import VideoCall from './VideoCall.js';
import AudioCall from './AudioCall.js';
import VideoViewProfile1 from './VideoViewProfile1.js';
import MyChat from './MyChat.js';
import Chat1 from './Chat1.js';
import AudioBroadcast from './AudioBroadcast.js';
import Introduction1 from './Introduction1.js';
import ViewProfile2 from './ViewProfile2.js';
import AudioViewProfile2 from './AudioViewProfile2.js';
import Schedule from './Schedule.js';
import ViewProfile from './ViewProfile.js';
import Thankyou from './Thankyou.js';
import Wallets from './Wallets.js';
import Walletss from './Walletss.js';
import Bet from './Bet.js';
import Top from './Top.js';
import Refer from './Refer.js';
import Games from './Games.js';
import LiveBroadcast from './LiveBroadcast.js';
import LiveBroadcast1 from './LiveBroadcast1.js';
import EditProfile from './EditProfile.js';
import Wallet from './Wallet.js';
import Otp from './Otp.js';
import My from './My.js';
import Support from './Support.js';
import Vip from './Vip.js';
import VipDetail from './VipDetail.js';
import MyEarning from './MyEarning.js';
import LiveMain  from './LiveMain.js';
import Home from './Home.js';
import Forgot from './Forgot.js';
import ForgotOtp from './ForgotOtp.js';
import Reset from './Reset.js';
import Homes from './Homes.js';
import Register from './Register.js';
import LiveVideoCall from './LiveVideoCall.js';
import LiveVideoCall1 from './LiveVideoCall1.js';
import Signup from './Signup.js';
import Faq from './Faq.js';
import About from './About.js';
import Privacy from './Privacy.js';
import Terms from './Terms.js';
import {NavigationContainer} from '@react-navigation/native';
const iconPath = {
  h: require('../resources/Icons/home.png'),
  ha: require('../resources/Icons/home2.png'),
  f: require('../resources/Icons/following.png'),
  fa: require('../resources/Icons/following2.png'),
  m: require('../resources/Icons/message.png'),
  ma: require('../resources/Icons/message2.png'),
  p: require('../resources/Icons/profile.png'),
  pa: require('../resources/Icons/profile2.png'),
};
const TabIcon = (source) => (
  <Image source={source} style={imageStyle.tabIcon} />
);
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';

import {TouchableOpacity} from 'react-native-gesture-handler';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, View} from 'react-native';
import {imageStyle, viewStyle} from '../style/Style';

const Tab = createBottomTabNavigator();
const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        showLabel: false,
      }}>
      <Tab.Screen
        name="Main"
        component={Main}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ha : iconPath.h),
        }}
      />


      <Tab.Screen
        name="Login"
        component={Login}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.pa : iconPath.p),
        }}
      />
    </Tab.Navigator>
  );
};

const Stack = createStackNavigator();
const Navigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        // initialRouteName={'Store'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        {/* <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        /> */}
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ViewProfile2"
          component={ViewProfile2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LeaderBoard"
          component={LeaderBoard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AudioViewProfile2"
          component={AudioViewProfile2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoCall"
          component={VideoCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Homes"
          component={Homes}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Introduction1"
          component={Introduction1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Games"
          component={Games}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Introduction"
          component={Introduction}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallets"
          component={Wallets}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Offline"
          component={Offline}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AudioBroadcast"
          component={AudioBroadcast}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveVideoCall1"
          component={LiveVideoCall1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoViewProfile1"
          component={VideoViewProfile1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyChat"
          component={MyChat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="About"
          component={About}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Terms"
          component={Terms}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Bet1"
          component={Bet1}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="AudioCall"
          component={AudioCall}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Privacy"
          component={Privacy}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ViewProfile"
          component={ViewProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Faq"
          component={Faq}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Top"
          component={Top}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveMain"
          component={LiveMain}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Rule"
          component={Rule}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Support"
          component={Support}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Thankyou"
          component={Thankyou}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Refer"
          component={Refer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveBroadcast"
          component={LiveBroadcast}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveBroadcast1"
          component={LiveBroadcast1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Schedule"
          component={Schedule}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Main"
          component={Main}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="My"
          component={My}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveVideoCall"
          component={LiveVideoCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Walletss"
          component={Walletss}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyEarning"
          component={MyEarning}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Vip"
          component={Vip}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VipDetail"
          component={VipDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Forgot"
          component={Forgot}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotOtp"
          component={ForgotOtp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Reset"
          component={Reset}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Bet"
          component={Bet}
          options={{headerShown: false}}
        />
        <Stack.Screen
        name="TabNavigator"
        component={TabNavigator}
        options={{headerShown: false}}
      />


        {/* <Stack.Screen
          name="SelectLanguage"
          component={SelectLanguage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login2"
          component={Login2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LeaderBoard"
          component={LeaderBoard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveGames"
          component={LiveGames}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Vip"
          component={Vip}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VipDetail"
          component={VipDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Store"
          component={Store}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyLevel"
          component={MyLevel}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyEarning"
          component={MyEarning}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Support"
          component={Support}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Setting"
          component={Setting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveUser"
          component={LiveUser}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoDetails"
          component={VideoDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AudioDetails"
          component={AudioDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PartyCall"
          component={PartyCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OneCall"
          component={OneCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveHouse1"
          component={LiveHouse1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Viewers"
          component={Viewers}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoPlay"
          component={VideoPlay}
          options={{headerShown: false}}
        />*/}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
