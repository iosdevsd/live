import React, {Component} from 'react';
import { StyleSheet, Text, View, Button,SafeAreaView,StatusBarLight,Dimensions,Image,Animated,TouchableOpacity,Alert,ImageBackground } from 'react-native';
import Backend from "./Backend.js";
import { GiftedChat,Time,Send,InputToolbar } from "react-native-gifted-chat";
import ImagePicker from 'react-native-image-picker';
import Orientation from 'react-native-orientation';
import Bubble from "react-native-gifted-chat/lib/Bubble";
import {LoginOtpApi,SignInApi,Explore,FetchHomeWallet,PujaStart,GetProfileApi} from '../backend/Api';
import { EventRegister } from 'react-native-event-listeners';
import CountDown from 'react-native-countdown-component';
import store from '../redux/store';
import io from 'socket.io-client';

const socket = io('http://139.59.25.187:3355', {
  transports: ['websocket']
})
var randomString = require('random-string');
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
type Props = {};
const options = {
    title: 'Select Document',
    maxWidth:300,
    maxHeight:500,

    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
export default class LiveChat extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Chat Consulation',
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  constructor(props) {
    super(props);

    this.state = {
        modalVisible: false,
        recognized: '',
        started: '',
        text :'',
        mystatus:false,
        results: [],
        messages: [],
        texts:'',

    };
  }

  // renderBubble(props) {
  //
  //     return (
  //         <View>
  //             <Text style={{color:'black'}}>{props.currentMessage.user.name}</Text>
  //         </View>
  //     );
  // }




  componentWillMount() {

    // const url = GLOBAL.BASE_URL + 'chat_read';

    // fetch(url, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     user_id: GLOBAL.user_id,
    //     chat_group_id: GLOBAL.bookingid,
    //   }),
    // })
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     if (responseJson.status == true) {
    //     } else {
    //     }
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  }
  renderBubble = (props,index) => {
         var a = false;
         if (props.currentMessage.status == true){
         a = true;
         }else{
             a = false;
         }
         //
         // if (props.currentMessage.user_id != GLOBAL.user_id ){
         //
         // }
         return (

                 <View style={{paddingRight: 12}}>




                     <Bubble {...props}

                     textProps={{
       style: {

         fontFamily:'Nunito-Regular',
         color:'#333333'

       },
     }}
                     wrapperStyle={{
                                             left: {
                                                 backgroundColor: 'white',
                                                 color:'#333333'
                                             },
                                             right: {
                                                 backgroundColor: '#90EE90',
                                                 color:'#333333'
                                             }
                                         }} />
                     {props.currentMessage.user_id != GLOBAL.user_id  &&  (
                         <View>

                         </View>
                     )}

                     {props.currentMessage.user_id == GLOBAL.user_id  &&  (
                         <View>


                         </View>
                     )}






                 </View>

         )
     }




  login = () => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Landing',
            params: {someParams: 'parameters goes here...'},
          }),
        ],
      }),
    );
  };

  renderChatFooter = () => {
    if (this.state.texts != '') {
      return (
        <Text style={{fontSize: 14, margin: 10}}> {this.state.texts}</Text>
      );
    }

    // if (this.state.isTyping) {
    //   if (this.typingTimeoutTimer == null) {
    //     this.startTimer();
    //   }
    //   return <TypingIndicator />;
    // }
    return null;
  };
  fieldView = (title, value) => (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        paddingVertical: 5,
      }}>
      <View style={{flex: 0.5, paddingHorizontal: 5}}>
        <Text>{title}</Text>
      </View>
      <View style={{flex: 0.5, paddingHorizontal: 5}}>
        <Text>{value}</Text>
      </View>
    </View>
  );

  renderInputToolbar (props) {
          //Add the extra styles via containerStyle
          return <InputToolbar {...props}

                               textInputStyle={{ color: "black" }}
                               containerStyle={{backgroundColor:'white',marginLeft:10,borderRadius:20,borderWidth:0,color:'black',marginBottom:0,marginRight:10}} />
      }
  renderSend(props) {
       return (
           <Send
               {...props}
           >
               <View style={{backgroundColor:'transparent'}}>
                   <Image source={require('../resources/send.png')}
                   style = {{height:38,width:38,resizeMode:'contain',backgroundColor:'transparent',marginRight:2,marginBottom:2}}/>
               </View>
           </Send>
       );
   }
   renderTime = (props) => {
 return (
   <Time
   {...props}
     timeTextStyle={{
       left: {
         color: 'grey',
       },
       right: {
         color: 'grey',
       },
     }}
   />
 );
};
  render() {


    return (
      <View style = {{flex:1}}>







             <GiftedChat
                    
                     extraData={this.state}
                    showUserAvatar = {false}
                     messages={this.state.messages}
             renderChatFooter={this.renderChatFooter}
             renderSend = {this.renderSend}
             renderInputToolbar={this.renderInputToolbar}
                     onSend={message => {


     Backend.sendMessage(message);
    setTimeout(()=>{
                EventRegister.emit('hide', '')
              },3000);

    //this.props.route.params.call()



                     }}
                     renderBubble={this.renderBubble}
                     renderTime = {this.renderTime}
                     renderAvatar = {null}
                     onInputTextChanged = {text =>{


                         // alert(text)

                     }

                     }
                     user={{
                         _id: store.getState().user.id,
                          name: GLOBAL.mydata.name
                     }}
                 />

             </View>


    );
  }
  componentDidMount() {
         Orientation.lockToLandscape();
  //  alert('hi')


    //  GLOBAL.mystatus = "Online";



      // Backend.updateMessage(message => {
      //     alert(JSON.stringify(message))
      //
      //
      // })


      Backend.loadMessages(message => {
        //  alert(JSON.stringify(message))


if (message.user_id == store.getState().user.id){
  if (message.length == 0){


  }else {
    this.setState(previousState => {


        return {
            messages: GiftedChat.append(previousState.messages, message)
        };
    });


  }
}







      });


     ;


      // Backend.updateMessage(message => {
      //     alert(JSON.stringify(message))
      //
      //
      // })





  }




  componentWillUnmount() {
    Backend.closeChat();
  }
}
const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#001739',
  },
  slide1: {
    marginLeft: 50,

    width: window.width - 50,
    height: 300,
    resizeMode: 'contain',
    marginTop: window.height / 2 - 200,
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});
