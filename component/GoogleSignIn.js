import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {webClientId} from '../backend/Config';
const GoogleSignIn = ({}) => {
  const onPressHandler = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const isSignedIn = await GoogleSignin.isSignedIn();
      console.log({isSignedIn});
      const userInfo = await GoogleSignin.signIn();
      const {idToken, user} = userInfo;
      consolejson(user);
      return;
      console.log(statusCodes.SIGN_IN_CANCELLED);
      console.log(statusCodes.IN_PROGRESS);
      console.log(statusCodes.PLAY_SERVICES_NOT_AVAILABLE);
      console.log(JSON.stringify(userInfo, null, 2));
    } catch (error) {
      console.log('error');
      console.log(JSON.stringify(error, null, 2));
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };
  useEffect(() => {
    GoogleSignin.configure({webClientId});
  }, []);

  return (
    <TouchableOpacity onPress={onPressHandler} style={styles.touch}>
      <Text style={styles.textTitle}>{`CONTINUE WITH GOOGLE`}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touch: {
    backgroundColor: '#ffffff',
    padding: 15,
    marginHorizontal: 25,
    borderRadius: 10,
    marginTop: 20,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#000000',
    alignSelf: 'center',
  },
});

export default GoogleSignIn;
