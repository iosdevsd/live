import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {textStyle, viewStyle} from '../style/Style';
import moment from 'moment';
import RazorpayCheckout from 'react-native-razorpay';
import LinearGradient from 'react-native-linear-gradient';

import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend,GetProfileApi,RechargeWallet,WalletHist,Daily} from '../backend/Api';
const Walletss = ({navigation}) => {
  const [username, setUsername] = useState([]);
    const [usernames, setUsernames] = useState({});
  useEffect(() => {



    const unsubscribew =   navigation.addListener('focus', () => {
      Daily({limit:"100",offset:"0"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 setUsername(data.data)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);
  const change = (amounte) =>{




  var amount = parseInt(amounte) * 100
  var options = {
 description: 'Coin purchasing',

 currency: 'INR',
 key: 'rzp_test_yLzqxWLttKfDSu', // Your api key
 amount: amount.toString(),
 name:  username.name,
 prefill: {
   email: username.email,
   contact:username.phone ,
   name: username.name
 },
 theme: {color: '#F00B51'}
}
RazorpayCheckout.open(options).then((data) => {
 // handle success
 success(data.razorpay_payment_id,amounte)
// alert(`Success: ${data.razorpay_payment_id}`);
}).catch((error) => {
 // handle failure
 alert(`Error: ${error.code} | ${error.description}`);
});
}


const success = (id,amount)=>{
var c = {txn_id:id,recharge_amount:amount}

console.log(JSON.stringify(c))
  RechargeWallet({txn_id:id,recharge_amount:amount})
         .then((data) => {
            //   toggleLoading(false);
 console.log(JSON.stringify(data))
           if (data.status) {
             console.log(JSON.stringify(data))

             GetProfileApi({user_id:"1"})
                    .then((data) => {
                       //   toggleLoading(false);

                      if (data.status) {
                        console.log(JSON.stringify(data))
                        setUsername(data.data)
                       // alert(JSON.stringify(data))
                       //setUsername(data.data)





                      } else {
             alert(JSON.stringify(data.message))
                      }
                    })
                    .catch((error) => {
                      //toggleLoading(false);
                      console.log('error', error);
                    });




           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
  const itemData = [
    {
      id: 1,
      coin: 100,
      price: 100,
    },
    {
      id: 2,
      coin: 200,
      price: 200,
    },
    {
      id: 3,
      coin: 300,
      price: 300,
    },
    {
      id: 4,
      coin: 500,
      price: 500,
    },
    {
      id: 5,
      coin: 1000,
      price: 1000,
    },
  ];
  return (
    <SafeAreaView style={styles.container}>
      {/* <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      /> */}
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Daily Reward</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>




      {username.map((item) => (

        <View style={styles.view_4} key={item.id.toString()}>
        <View style={styles.view_5}>
          <Image
            style={styles.image_1}
            source={require('../resources/coin.png')}
          />
          <View>
          <Text style = {{color:'#000000',fontFamily:"DMSans-Regular",fontSize:15,marginLeft:20}}>
          {item.txn_name}
          </Text>
          <Text style = {{color:'#EB0A51',fontFamily:"DMSans-Bold",fontSize:20,marginLeft:20}}>
          ₹{item.txn_amount}
          </Text>
          <Text style = {{color:'#000000',fontFamily:"DMSans-Regular",fontSize:15,marginLeft:20}}>
          {moment(item.created_at).format("DD MMM YYYY , hh:mm: a")}
          </Text>


          </View>




          </View>
        </View>
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  view_1: {
    marginTop: 23,
    marginHorizontal: 23,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  view_2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    height: 44,
    width: 44,
  },
  coinText: {
    marginLeft: 7,
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 40,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  coinText_2: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  view_3: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 7,
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 13,
    fontStyle: 'normal',
    color: '#DD0952',
    textDecorationLine: 'underline',
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#262628',
    marginTop: 34,
    alignSelf: 'center',
  },
  view_4: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#EFEFEF',
    paddingVertical: 10,
  },
  view_5: {
    flexDirection: 'row',
    marginLeft: 20,
    alignItems: 'center',
  },
  image_1: {
    height: 25,
    width: 25,
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 20,
    fontStyle: 'normal',
    color: '#262628',
    marginLeft: 7,
  },
  text_5: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  view_6: {
    borderRadius: 24,
    width: 100,
    paddingVertical: 7,
    marginLeft: 'auto',
    marginRight: 20,
    alignItems: 'center',
  },
});
export default Walletss;
