import React ,{ useEffect ,useState} from 'react';
import {SafeAreaView,StatusBar,Dimensions, Text,ImageBackground,Image,View,StyleSheet,TouchableOpacity,NativeModules,FlatList} from 'react-native';
import style from '../style/Style.js';
import { AsyncStorageGetUser } from '../backend/Api';
import * as actions from '../redux/actions';
const window = Dimensions.get('window');
import store from '../redux/store';
const {Agora} = NativeModules;
const GLOBAL = require('./Global');
var array = [
  {
    title:"Launch Your Game"
  },
  {
    title:"Schedule Your Game"
  },
  {
    title:"Schedule List"
  },

]
import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
import {LoginOtpApi,SignInApi,SocialLogin,Recently} from '../backend/Api';
import requestCameraAndAudioPermission from './permission';
import {textStyle, viewStyle} from '../style/Style';
const {
 FPS30,
 AudioProfileDefault,
 AudioScenarioDefault,
 Host,
 Audience,
 Adaptative
} = Agora
import LinearGradient from 'react-native-linear-gradient';
const LiveMain = ({navigation}) => {
    const [username, setUsername] = useState({});
  const screenHandler = async () => {
     actions.Login(JSON.parse(await AsyncStorageGetUser()))
    setTimeout(() => {
      const user = store.getState().user
  if (Object.keys(user).length === 0 && user.constructor === Object) {
    navigation.replace('Introduction')
  } else {
    navigation.replace('Home')
  }
}, 1000);

  }

  const pujaopen = (item,index) =>{
if (index == 0){
  onPressd()
} else if (index == 1){
  onPress()
}else {
  onPressa()
}

  }

  const renderItemTv=({item, index}) => {

var color = 'white'

      // alert(JSON.stringify(item))
  return(

<TouchableOpacity onPress={()=>pujaopen(item,index)} >

<LinearGradient
  colors={['#73005C', '#F00B51']}
  start={{x: 0, y: 0}}
  end={{x: 1, y: 1}}
  style={{height:100,borderRadius:12,margin:8,width:window.width/2 -20}}>
  <Text style={{textAlign:'center',fontSize:16,fontFamily:"DMSans-Bold",color:'white',alignSelf:'center',marginTop:30}}>{item.title}</Text>

</LinearGradient>

</TouchableOpacity>


)
}

    const onPress = () => {
        navigation.navigate('Home')
    }
    const onPressd = () => {
        navigation.navigate('Homes')
    }

    const onPressa = () => {
        navigation.navigate('Schedule')
    }
    const onPressBack = () => navigation.goBack();
  useEffect(() => {
    const socket = io('http://51.79.250.86:3000', {
     transports: ['websocket']
   })
       socket.on('connect', () => {

         console.log("socket connected")




       })
    const unsubscribew =   navigation.addListener('focus', () => {
      Recently({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                // alert(JSON.stringify(data))
GLOBAL.bookingid = data.data.bridge_id
                navigation.navigate("LiveVideoCall", {
                                            uid: Math.floor(Math.random() * 100),
                                            clientRole: Host,
                                            channelName: data.data.bridge_id,
                                            onCancel: (message) => {

              }})



               } else {
    //  alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)
    requestCameraAndAudioPermission().then(_ => {
                console.log('requested!');
            })
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />



      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Live Game Roulette</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
              source = {require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
    <FlatList  style={{width:'100%',marginTop:20,alignSelf:'center'}}
data={array}
numColumns={2}
showsHorizontalScrollIndicator={false}
renderItem={renderItemTv}
/>






  

    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 55,
    marginBottom: 42,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export default LiveMain;
