import firebase from "firebase";
const GLOBAL = require('./Global');
import store from '../redux/store';
import {ChatHistory,GetProfileApi,Unsseen} from '../backend/Api';
class Backend {
    uid = GLOBAL.user_id;
    messagesRef = null;
    messagesRefs = null;
    messagesRefss = null;
    chatmessagesRef = null;
    // initialize Firebase Backend
    constructor() {
        firebase.initializeApp({
          apiKey: "AIzaSyBD2vHLhXm4MzN0A79AQLlHe8XBAzowSm4",
    authDomain: "docall-a8194.firebaseapp.com",
        databaseURL: "https://docall-a8194-default-rtdb.firebaseio.com",
    projectId: "docall-a8194",
        storageBucket: "docall-a8194.appspot.com",
        messagingSenderId: "117868681027",
        appId: "1:117868681027:web:11391d4c38b691743441f6",
        measurementId: "G-7T4WQ6DE04"
        });

    }
    setUid(value) {
        this.uid = value;
    }
    getUid() {
        return this.uid;
    }





    // retrieve the messages from the Backend

    loadMessagess(callbacks) {
       // alert(JSON.stringify(callbacks))

          this.messagesRefs =  firebase.database().ref().child("chat/" + GLOBAL.bookingid + 'type');
        if (this.messagesRefs.length == 0) {
            this.messagesRefs.push({
                typinguser: false,
                typinganother: false,
                name :'harshit',

            });


        }


        const onReceives = data => {
           // alert(JSON.stringify(data))
          this.messagesRefs.once("value").then(function(snapshot) {
              const message = snapshot.val();
             // alert(message.typinganother)

          //    alert(JSON.stringify(message))

              // snapshot.forEach(function(childSnapshot) {
              //     var key = childSnapshot.name;
              //     var childData = childSnapshot.val();
              //   //  alert(JSON.stringify(childSnapshot))
              //
              // });



             if (message.userid == GLOBAL.user_id){
                 callbacks({
                     name: message.name,
                     typinganother: message.typinganother,

                 })
             }else{
                 callbacks({
                     name: message.name,
                     typinganother: message.typinguser,

                 })
             }

          //    alert(JSON.stringify(callbacks))
            //  this.loadMessagess(callbacks)

            })

         //   alert(data.typinganother)
         //    this.messagesRefs.then(snapshot => {
         //        alert(JSON.stringify(snapshot.value))
         //    })
           // alert(JSON.stringify(data))


            // const message = data.val();
            // callbacks({
            //     _id: '',
            //     text: '',
            //     //createdAt: new Date(message.createdAt),
            //     createdAt: '',
            //     user: {
            //         _id: '',
            //         name: ''
            //     }
            // });
        };

        this.messagesRefs
            .orderByChild("createdAt")
            //.startAt(d)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_changed", onReceives);

    }



    updateMessage(callbacks) {

        this.messagesRefss =  firebase.database().ref().child("chat/" + GLOBAL.bookingid);

       this.messagesRefss.off(); //Detaches a callback previously attached with on()


        // this.messagesRef.upd
        var query =  this.messagesRefss.orderByChild('anotherid').equalTo(GLOBAL.user_id)
        query.on('value', function(snapshot) {
            snapshot.forEach(function(weekSnapshot) {
                console.log(weekSnapshot.val());
                weekSnapshot.ref.update({ status: true });
            });
        });

        const onReceives = data => {
          //  alert(JSON.stringify(data))
            const message = data.val();





           //  alert(JSON.stringify(message))

            callbacks({
                _id: data.key,
                text: message.text,
                //createdAt: new Date(message.createdAt),
                createdAt: message.createdAt,
                status:message.status,
                user_id:message.user_id,
                anotherid:message.another,
                image:message.image,
                vip:message.vip,
                wallet:message.wallet,

                user: {
                    _id: message.user._id,
                    name: message.user.name
                }
            });
        };



        var d = this.getLimit();
        //  console.log(d);
        //   Generates a new Query object limited to the last specific number of children.
        //    this.messagesRef.limitToLast(10).on("child_added", onReceive);
        // this.messagesRef
        //     .orderByChild("createdAt")
        //     //.startAt(d)
        //     //.endAt("2017-11-27T06:51:47.851Z")
        //     .on("child_added", onReceive);



        this.messagesRefss
            .orderByChild("createdAt")
            //.startAt(d)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_changed", onReceives);


    }



    loadMessagesvideo(callback) {
        this.messagesRef =  firebase.database().ref().child("chat/" + GLOBAL.bookingid);
        this.messagesRef.off();



        const onReceive = data => {
            // alert(JSON.stringify(data))
            const message = data.val();





            // alert(JSON.stringify(message))

            callback({
                _id: data.key,
                text: message.text,
                //createdAt: new Date(message.createdAt),
                createdAt: message.createdAt,
                status:message.status,
                user_id:message.user_id,
                anotherid:message.another,
                image:message.image,
                vip:message.vip,
                wallet:message.wallet,

                user: {
                    _id: message.user._id,
                    name: message.user.name
                }
            });
        };

        var today = new Date();

           var timestamp = new Date(today).toISOString();
           var timestamp = today.toISOString();

        var d = this.getLimit();
        //  console.log(d);
        //   Generates a new Query object limited to the last specific number of children.
        //    this.messagesRef.limitToLast(10).on("child_added", onReceive);
        this.messagesRef
            .orderByChild("createdAt")
            .startAt(timestamp)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_added", onReceive);






    }

    loadMessages(callback) {
        this.messagesRef =  firebase.database().ref().child("chat/" + GLOBAL.bookingid);
        this.messagesRef.off();




        var query =  this.messagesRef.orderByChild('anotherid').equalTo(GLOBAL.user_id)
        query.on('value', function(snapshot) {
            snapshot.forEach(function(weekSnapshot) {
                console.log(weekSnapshot.val());
               // alert(JSON.stringify(weekSnapshot))
                weekSnapshot.ref.update({ status: true });
            });
        });

        const onReceives = data => {

            const message = data.val();





            // alert(JSON.stringify(message))

            callback({
                 _id: data.key,
                 text: '',
                // //createdAt: new Date(message.createdAt),
                // createdAt: message.createdAt,
                status:message.status,
                // user_id:message.user_id,
                // anotherid:message.anotherid,
                //
                // user: {
                //     _id: message.user._id,
                //     name: message.user.name
                // }
            });
        };

        const onReceive = data => {
            // alert(JSON.stringify(data))
            const message = data.val();





            // alert(JSON.stringify(message))

            callback({
                _id: data.key,
                text: message.text,
                //createdAt: new Date(message.createdAt),
                createdAt: message.createdAt,
                status:message.status,
                user_id:message.user_id,
                anotherid:message.another,
                image:message.image,
                vip:message.vip,
                wallet:message.wallet,

                user: {
                    _id: message.user._id,
                    name: message.user.name
                }
            });
        };



        var d = this.getLimit();
        //  console.log(d);
        //   Generates a new Query object limited to the last specific number of children.
        //    this.messagesRef.limitToLast(10).on("child_added", onReceive);
        this.messagesRef
            .orderByChild("createdAt")
            //.startAt(d)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_added", onReceive);



        this.messagesRef
            .orderByChild("createdAt")
            //.startAt(d)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_changed", onReceives);


    }

    loadMessages1(callback) {
        this.chatmessagesRef =  firebase.database().ref().child("chat1/" + GLOBAL.chatid);
        this.chatmessagesRef.off();










        const onReceive = data => {

          Unsseen({user_id:GLOBAL.user_id,chatgid:GLOBAL.chatid})
                 .then((data) => {
                   if (data.status) {
                     console.log(JSON.stringify(data))

                   } else {
          alert(JSON.stringify(data.message))
                   }
                 })
                 .catch((error) => {
                   //toggleLoading(false);
                   console.log('error', error);
                 });
            // alert(JSON.stringify(data))
            const message = data.val();

            callback({
                _id: data.key,
                text: message.text,
                //createdAt: new Date(message.createdAt),
                createdAt: message.createdAt,
                status:message.status,
                user_id:message.user_id,
                anotherid:message.another,
                image:message.image,
                vip:message.vip,
                wallet:message.wallet,
                userdelete:message.userdelete,
                broaddelete:message.broaddelete,
                video:message.video,
                image1:message.image1,

                user: {
                    _id: message.user._id,
                    name: message.user.name
                }
            });
        };



        var d = this.getLimit();

        this.chatmessagesRef
            .orderByChild("createdAt")
            //.startAt(d)
            //.endAt("2017-11-27T06:51:47.851Z")
            .on("child_added", onReceive);



        // this.chatmessagesRef
        //     .orderByChild("createdAt")
        //     //.startAt(d)
        //     //.endAt("2017-11-27T06:51:47.851Z")
        //     .on("child_changed", onReceives);


    }



    // send the message to the Backend


    sendMessage12(message) {
        this.messagesRef =  firebase.database().ref().child("chat/" + GLOBAL.bookingid);
      //  this.messagesRef.update({ status: true});

    //         if (GLOBAL.chatstatus == true) {
            //console.log(new Date(firebase.database.ServerValue.TIMESTAMP));
            var today = new Date();
            /* today.setDate(today.getDate() - 30);
            var timestamp = new Date(today).toISOString(); */
            var timestamp = today.toISOString();
            //this.messagesRef = [];
            for (let i = 0; i < message.length; i++) {
    var z = "";
              var k = message[i].hasOwnProperty('image')
                   if (k == true){
                     z = message[i].image;


                   } else {
                     z = "";

                   }
                   if (message[i].text != ""){
                this.messagesRef.push({

                    text: message[i].text,
                    user: message[i].user,
                    createdAt: timestamp,
                    user_id:'0',
                    anotherid:GLOBAL.another,
                    status: false,
                    image:z,
                    vip:GLOBAL.mydata.is_vip_subscribe,
                    wallet:GLOBAL.wallet
                });
              }
              //  this.messagesRefs.update({ typinguser: false, typinganother: false ,name :GLOBAL.myname,userid:GLOBAL.user_id});
            }
    //    }
    }
    sendMessage(message) {
        this.messagesRef =  firebase.database().ref().child("chat/" + GLOBAL.bookingid);
      //  this.messagesRef.update({ status: true});

//         if (GLOBAL.chatstatus == true) {
            //console.log(new Date(firebase.database.ServerValue.TIMESTAMP));
            var today = new Date();
            /* today.setDate(today.getDate() - 30);
            var timestamp = new Date(today).toISOString(); */
            var timestamp = today.toISOString();
            //this.messagesRef = [];
            for (let i = 0; i < message.length; i++) {
var z = "";
              var k = message[i].hasOwnProperty('image')
                   if (k == true){
                     z = message[i].image;


                   } else {
                     z = "";

                   }
                   if (message[i].text != ""){
                this.messagesRef.push({

                    text: message[i].text,
                    user: message[i].user,
                    createdAt: timestamp,
                    user_id:store.getState().user.id,
                    anotherid:GLOBAL.another,
                    status: false,
                    image:z,
                    vip:GLOBAL.mydata.is_vip_subscribe,
                    wallet:GLOBAL.wallet
                });
              }
              //  this.messagesRefs.update({ typinguser: false, typinganother: false ,name :GLOBAL.myname,userid:GLOBAL.user_id});
            }
    //    }
    }




//one to one chat


updateStatus = (item,data) => {


        this.chatmessagesRef =  firebase.database().ref().child("chat1/" + GLOBAL.chatid);

        this.chatmessagesRef.off(); //Detaches a callback previously attached with on()


         // this.messagesRef.upd
         var query =  this.chatmessagesRef.orderByChild('createdAt').equalTo(data.createdAt)
         query.on('value', function(snapshot) {
             snapshot.forEach(function(weekSnapshot) {
                 console.log(weekSnapshot.val());
                 weekSnapshot.ref.update({ userdelete: "1" });
             });
         });


}


updateDeleteStatus = () => {

        this.chatmessagesRef =  firebase.database().ref().child("chat1/" + GLOBAL.chatid);

  var query = this.chatmessagesRef.orderByChild("createdAt")
  query.on('value', function (snapshot) {
    snapshot.forEach(function (weekSnapshot) {
      console.log(weekSnapshot.val());

     weekSnapshot.ref.update({userdelete: "1"});
    });
  });
}

    chatendMessage(message) {
        this.chatmessagesRef =  firebase.database().ref().child("chat1/" + GLOBAL.chatid);

            var today = new Date();
            var timestamp = today.toISOString();
            //this.messagesRef = [];
            for (let i = 0; i < message.length; i++) {
    var z = "";
              var k = message[i].hasOwnProperty('image')
                   if (k == true){
                     z = message[i].image;


                   } else {
                     z = "";

                   }


                   var z3 = "";
                           var k3 = message[i].hasOwnProperty('image1')
                                if (k3 == true){
                                  z3 = message[i].image1;


                                } else {
                                  z3 = "";

                                }


             var z2 = "";
                           var k2 = message[i].hasOwnProperty('video')
                                if (k2 == true){
                                  z2 = message[i].video;


                                } else {
                                  z2 = "";

                                }
                   if (message[i].text != ""){
                this.chatmessagesRef.push({

                    text: message[i].text,
                    user: message[i].user,
                    createdAt: timestamp,
                    user_id:store.getState().user.id,
                    anotherid:GLOBAL.another,
                    status: false,
                    image:z,
                    vip:GLOBAL.mydata.is_vip_subscribe,
                    wallet:GLOBAL.wallet,
                    userdelete:'',
                    broaddelete:'',
                    video:z2,
                    image1:z3

                });
              }
              if (message[i].text == "" && z2 != "" && z3 == "" ){
           this.chatmessagesRef.push({

               text: message[i].text,
               user: message[i].user,
               createdAt: timestamp,
               user_id:store.getState().user.id,
               anotherid:GLOBAL.another,
               status: false,
               image:z,
               vip:GLOBAL.mydata.is_vip_subscribe,
               wallet:GLOBAL.wallet,
               userdelete:'',
               broaddelete:'',
               video:z2,
               image1:z3

           });
         }



              //  this.messagesRefs.update({ typinguser: false, typinganother: false ,name :GLOBAL.myname,userid:GLOBAL.user_id});
            }
    //    }
    }

    // close the connection to the Backend
    closeChat() {
        if (this.messagesRef) {
            this.messagesRef.off();
        }
    }

    getLimit() {
        var today = new Date();
        //var milliseconds = Date.parse(today);
        //var changed = milliseconds - 86400000; //10 minutes (- 900000) -  86400000 1 day
        today.setDate(today.getDate() - 31); // last 30 Days
        //console.log(today);
        var changedISODate = new Date(today).toISOString();
        //var changedISODate = today.toISOString();
        console.log(changedISODate);
        return changedISODate;
    }
}

export default new Backend();
