import React from 'react';
import {ImageBackground, StyleSheet, Text, View,TouchableOpacity,Dimensions,Image} from 'react-native';
const window = Dimensions.get('window');
const Game = ({show,callback,callback1,callback2}) => {
  console.log(show);
  return (
    <>
      <View
        style={{
          paddingVertical: 8,
          paddingHorizontal: 16,
          backgroundColor: '#ADC3D2',
          justifyContent: 'center',
          marginBottom: 8,
        }}>
        <Text
          style={{
            fontFamily: 'DMSans-Bold',
            fontWeight: '600',
            fontSize: 13,
            color: '#FFFFFF',
            textAlign: 'justify',
          }}>
          These games are intended for amusement and entertainment purposes
          only.
        </Text>
      </View>


        <TouchableOpacity

          onPress={() => callback()}>
          <View style = {{height:186,shadowColor: "black",width:window.width - 20,alignSelf:'center',
    shadowOffset: { height: 13},
    shadowOpacity: 1.9,elevation:5}}>





    <ImageBackground
                  source={require('../resources/roulette.png')}
                  style={{width:window.width - 20,height:181,marginTop:10}}
                  imageStyle = {{  shadowColor: "black",
        shadowOffset: { height: 2},
        shadowOpacity: 0.3,}}
                  borderRadius = {22}>
    <TouchableOpacity  style={{position:'absolute',top:10,right:3,width:34,height:34,zIndex:2}}

      onPress={() => callback1('r')}>
    <Image
                  source={require('../resources/information-button.png')}
            style={{width:20,height:20}}
              />
</TouchableOpacity>



    </ImageBackground>


          </View>
          </TouchableOpacity>

          <TouchableOpacity

            onPress={() => callback2()}>
            <View style = {{height:186,shadowColor: "black",width:window.width - 20,alignSelf:'center',
        shadowOffset: { height: 13},
        shadowOpacity: 1.9,elevation:5}}>





        <ImageBackground
                      source={require('../resources/dice.png')}
                      style={{width:window.width - 20,height:181,marginTop:10}}
                      imageStyle = {{  shadowColor: "black",
            shadowOffset: { height: 2},
            shadowOpacity: 0.3,}}
                      borderRadius = {22}>
        <TouchableOpacity  style={{position:'absolute',top:10,right:3,width:34,height:34,zIndex:2}}

        onPress={() => callback1('d')}>
        <Image
                          source={require('../resources/information-button.png')}
                    style={{width:20,height:20}}
                      />
        </TouchableOpacity>



        </ImageBackground>


            </View>
            </TouchableOpacity>



    </>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
  textView: {
    backgroundColor: '#3D1B12',
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius:22,
    borderBottomRightRadius:22,

  },
  gameView: {
    elevation:5,

    height: 190,
    shadowColor: "black",
  shadowOffset: { height: 2},
  shadowOpacity: 0.3,
    margin: 4,

    overflow: 'hidden',
    flexDirection: 'column-reverse',
  },
  mainView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginHorizontal: 8,
  },
});
export default Game;
