import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Clipboard,
  Share
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {LoginOtpApi,SignInApi,SocialLogin,Recently,GetProfileApi,Terq} from '../backend/Api';
import {textStyle, viewStyle} from '../style/Style';
import * as actions from '../redux/actions';
import {settingList} from '../backend/Config';
import HTML from "react-native-render-html";


const Terms = ({navigation}) => {
  const [username, setUsername] = useState('');
  useEffect(() => {
    const unsubscribew =   navigation.addListener('focus', () => {
      Terq({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {

                //alert(JSON.stringify(data.data.terms))
                 setUsername(data.data.terms)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);

  const copyToClipboard = () => {
   Clipboard.setString(username.referral_code)
 }

const  fancyShareMessage=()=>{

    var a = username.referral_code
    var commonHtml = `I want to recommend Cleo Live - mobile application.\n\nAt Cleo Live you can Play Live Games with Live Girls and Win lots of Money.\n\nLogin Every day and get Free Coins to Play.\n\nDownload Cleo Live from http://mepllive.com.\n\nWhile Registering use my referral code ${a} and get Free Coins`;

    Share.share({
            message:commonHtml
        },{
            tintColor:'green',
            dialogTitle:'Share this Referral Code via....'
        }
    );
}


  const onPressHandler = (key) => {
    switch (key) {
      case 'logout':
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Introduction'}],
                 });
        break;
      default:
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Terms & Conditions</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>

      <ScrollView style={{ flex: 1 }}>
      <View style = {{margin:10}}>
          <HTML source={{ html: username }}  />
          </View>
        </ScrollView>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  appVersion: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
    alignSelf: 'center',
    opacity: 0.7,
    marginTop: 47,
  },
  view: {
    flexDirection: 'row',
    borderBottomColor: '#F5F5F5',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  image: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 12,
  },
  text: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
  },
  linearGradient: {
    flex: 1,
  },
});

export default Terms;
