import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import { CallingCodePicker } from '../src/components/CallingCodePicker';
import { spacing } from '../src/theme';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
const window = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
var validator = require("email-validator");
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps} from '../backend/Api';
const GLOBAL = require('./Global');

import DeviceInfo from 'react-native-device-info';
import {radioStyle, textStyle, viewStyle} from '../style/Style';
import * as actions from '../redux/actions';
const Register = ({navigation, route}) => {
   const [selectedCode, setSelectedCode] = useState('');
  const [state, setState] = useState({
  loading: false,



});

const [term, setTerm] = useState(false);
  const toggleLoading = (bol) => setState({...state, loading: bol});
  const [userState, setUserState] = useState({
    dp: '',
    name: '',
    email: '',
    mobile: '',
    gender: 0,
    password:'',
    confirm:'',
    referal:'',
  });
  const submitHandler = () => {
     navigation.navigate('Otp');

  };
  const onPressBack = () => navigation.goBack();
  const onRadioPress = (value) => setUserState({...userState, gender: value});
  var radio_props = [
    {label: 'Male', value: 0},
    {label: 'Female', value: 1},
  ];

  const my = (text) =>{
    GLOBAL.email = text
     setUserState({...userState, email: text})
  }
  const my1 = (text) =>{
    GLOBAL.mobile = text
     setUserState({...userState, mobile: text})
  }

  const onPress = () => {


    if (userState.name == ""){
      Toast.showWithGravity('Please enter username', Toast.LONG, Toast.CENTER);
      return
    }
    if (userState.email == ""){
      Toast.showWithGravity('Please enter Email ', Toast.LONG, Toast.CENTER);
      return
    }

    if (validator.validate(userState.email) == false){
      Toast.showWithGravity('Please enter Valid Email ', Toast.LONG, Toast.CENTER);
      return
    }



    if (userState.password == ""){
        Toast.showWithGravity('Please enter password', Toast.LONG, Toast.CENTER);
        return
    }

    if (userState.confirm == ""){
      Toast.showWithGravity('Please enter Confirm Password ', Toast.LONG, Toast.CENTER);
      return
    }
    if (userState.confirm != userState.password ){
      Toast.showWithGravity('Password and Confirm Password do not match. ', Toast.LONG, Toast.CENTER);
      return
    }
    if (term == false ){
      Toast.showWithGravity('Please accept our Terms & Conditions ', Toast.LONG, Toast.CENTER);
      return
    }
    toggleLoading(true);

    const json = {
  name: userState.name,
  email: GLOBAL.email,
  phone: GLOBAL.mobile,
  password:userState.password,
  referral_code: userState.referal,
  device_id: DeviceInfo.getDeviceId()


};
console.log(JSON.stringify(json))

RegisterOtps(json)
       .then((data) => {
            toggleLoading(false);

         if (data.status) {
           //alert(JSON.stringify(data))

//            actions.Login(data.data);
// actions.Token(data.token.toString());
// AsyncStorageSetUser(data.data);
// AsyncStorageSettoken(data.token)
//
 navigation.navigate('Otp',{item:data.data})



         } else {
    Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };

  const onPresss = () => {
navigation.goBack()


  };
  useEffect(() => {
    GLOBAL.email = ""
    GLOBAL.mobile = ""
  }, []);
  useEffect(() => {
    console.log(userState);
  }, [userState]);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView keyboardShouldPersistTaps="always">
        {state.loading && <Loader />}
        <StatusBar
          barStyle="light-content"
          backgroundColor="transparent"
          translucent={true}
        />
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={viewStyle.simpleHeaderView}>
          <Text style={textStyle.simpleHeaderTitle}>Register</Text>
          <View style={styles.backView}>
            <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
              <Image
                source={require('../resources/back.png')}
                style={styles.backImage}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>

  <View style = {{marginLeft:28,width:window.width -56,marginTop:0}}>


<TextField
    label='Name'
    baseColor = '#acb1c0'
    tintColor = '#acb1c0'
    onChangeText={(text) => setUserState({...userState, name: text})}
    value={userState.name}


/>
<View style = {{marginBottom:10,marginTop:0}}>
<TextField  style = {{marginTop:0}}
    label='Email'
    baseColor = '#acb1c0'
    tintColor = '#acb1c0'
      inputContainerStyle = {{marginTop:-15}}
    onChangeText={(text) => my(text)}
    value={userState.email}


/>
</View>

<CallingCodePicker

onValueChange={setSelectedCode} />



<View style = {{marginLeft:130,marginTop:-50}}>
<TextField
    label='Mobile'
    baseColor = '#acb1c0'

    tintColor = '#acb1c0'
      onChangeText={(text) => my1(text)}
    value={userState.mobile}


/>
</View>
<TextField
    label='Password'
    baseColor = '#acb1c0'
    tintColor = '#acb1c0'
    inputContainerStyle = {{marginTop:-15}}
    onChangeText={(text) => setUserState({...userState, password: text})}
    value={userState.password}
    secureTextEntry = {true}


/>

<TextField
    label='Confirm Password'
    baseColor = '#acb1c0'
    tintColor = '#acb1c0'
    inputContainerStyle = {{marginTop:-15}}
    onChangeText={(text) => setUserState({...userState, confirm: text})}
    value={userState.confirm}
    secureTextEntry = {true}


/>

<TextField
    label='Referral Code'
    baseColor = '#acb1c0'
    tintColor = '#acb1c0'
    inputContainerStyle = {{marginTop:-15}}
    onChangeText={(text) => setUserState({...userState, referal: text})}
    value={userState.referal}


/>

</View>





        <TouchableOpacity onPress= {()=>setTerm(!term)}>

                            <View style = {{flexDirection:'row'}}>
                            {term == false && (
                              <Image
                                  source={require('../resources/box.png')}
                                  style={{width: 15, height: 15,marginLeft:24,marginTop:12,resizeMode:'contain'}}


                              />
                            )}

                            {term == true && (
                              <Image
                                  source={require('../resources/boxa.png')}
                                  style={{width: 15, height: 15,marginLeft:24,marginTop:12,resizeMode:'contain'}}


                              />
                            )}


                            <Text style={{color: 'black',fontFamily:'DMSans-Regular',fontSize: 13,marginLeft:10,marginTop:8,width:window.width - 90,lineHeight:23}}>
                               By Clicking Login You confirm  you are above 18 years and you agree to &nbsp;
                                <Text onPress={()=>navigation.navigate('Terms')}  style={{color: '#73005C',fontFamily:'DMSans-Bold',fontSize: 13}}>
                                    Our Terms & Conditions &nbsp;
                                </Text>
                                and &nbsp;
                                <Text onPress={()=>navigation.navigate('Privacy')} style={{color: '#73005C',fontFamily:'DMSans-Bold',fontSize: 13}}>
                                  Privacy Policy &nbsp;
                                </Text>
                            </Text>




                            </View>
                            </TouchableOpacity>
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={styles.linearGradient}>
          <TouchableOpacity style={styles.touchable} onPress={onPress}>
            <Text style={styles.submitText}>Submit</Text>
          </TouchableOpacity>
        </LinearGradient>


        <Text onPress={onPresss}  style={{color:'black',fontFamily:'DMSans-Bold',marginTop:20,alignSelf:'center'}}>Already have an Account?
<Text onPress={onPresss}  style={{color:'#73005C',fontFamily:'DMSans-Bold',marginTop:20,textAlign:'center'}}> Login
</Text>
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  linearGradient: {
    margin: 25,
    borderRadius: 25,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  submitText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },

  imageBg: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    marginTop: 46,
    marginBottom: 30,
  },
  image: {
    borderRadius: 75,
  },
  editImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 25,
  },
  textInput: {
    width: '90%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
    marginBottom: 15,
    height:40
  },
  date: {},
});
export default Register;
