import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ScrollView,
  Platform,
  TextInput,
  Dimensions,
  StatusBar,
} from 'react-native';

import GoogleSignIn from './GoogleSignIn';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import DeviceInfo from 'react-native-device-info';
const GLOBAL = require('./Global');
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin} from '../backend/Api';
const window = Dimensions.get('window');
import * as actions from '../redux/actions';
const Introduction = ({navigation}) => {
  const [state, setState] = useState({
  loading: false,



});
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const toggleLoading = (bol) => setState({...state, loading: bol});
  const [keyTypeNum, setKeyTypeNum] = useState(false);
  const onPress = () => {

    if (username == ""){
      Toast.showWithGravity('Please enter username', Toast.LONG, Toast.CENTER);
      return
    }
    if (password == ""){
        Toast.showWithGravity('Please enter password', Toast.LONG, Toast.CENTER);
        return
    }
    toggleLoading(true);

    const json = {
  username: username,
  password: password,
  device_id: DeviceInfo.getDeviceId(),
  device_type: Platform.OS,
device_token: GLOBAL.firebaseToken,
  model_name: DeviceInfo.getModel(),

};
console.log(JSON.stringify(json))

SignInApi(json)
       .then((data) => {
            toggleLoading(false);

         if (data.status) {
          //alert(JSON.stringify(data))

           actions.Login(data.data);
actions.Token(data.token.toString());
AsyncStorageSetUser(data.data);
AsyncStorageSettoken(data.token)


navigation.reset({
                index: 0,
                routes: [{name: 'TabNavigator'}],
              });





         } else {
           console.log(JSON.stringify(data.message))
           if (data.message == `The email address is not registered at Do Call, Kindly Create a new account from Sign Up` || data.message == `The mobile number is not registered at Do Call, Kindly Create a new account from Sign Up` ){
                        Alert.alert('USER NOT FOUND',`This ${username} is not registered at Cleo Live,  Kindly create a new account from Sign Up.`,
                                                 [
                                                     {text:"OK",
                                                     },
                                                 ],
                                                 {cancelable:false}
                                             )
                      }
                      else if (data.message == `Invalid Password!`){
                           Alert.alert('OPPS!','Oops, the password entered in incorrect. Please enter the password again.',
                                                    [
                                                        {text:"OK",
                                                        },
                                                    ],
                                                    {cancelable:false}
                                                )
                         }
                         else if (data.message == `Your account is not active, Please contact to Admin`){
                              Alert.alert('DEACTIVATED','Your account is not active and has been deactivated. Kindly contact support for Help!',
                                                       [
                                                           {text:"OK",
                                                           },
                                                       ],
                                                       {cancelable:false}
                                                   )
                            }
                         else {
                           Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
                         }
         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };

  useEffect(() => {
//     try {
//    GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
//   // google services are available
// } catch (err) {
//   console.error('play services are not available');
// }
//AIzaSyCcRlpCWs8QBklhcYKZ9wyIBIU_9yZAOW4
//117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com
//117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com
//222874891986-d3ak6g6cmt4ell8rpqhfufnl8q31i0r5.apps.googleusercontent.com
   //  GoogleSignin.configure({
   //   webClientId: '222874891986-d3ak6g6cmt4ell8rpqhfufnl8q31i0r5.apps.googleusercontent.com',
   //   offlineAccess: true,
   //   hostedDomain: '',
   //   forceConsentPrompt: true,
   // })

  // GoogleSignin.configure();
//117868681027-vgs1osij1gus6l92jojrfc8dn1k6bdvf.apps.googleusercontent.com
//   GoogleSignin.configure({
//   scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
//   webClientId: "117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com",
//   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//   hostedDomain: '', // specifies a hosted domain restriction
//   loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//   forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
//   accountName: '', // [Android] specifies an account name on the device that should be used
//   iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
//   googleServicePlistPath: '', // [iOS] optional, if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
// });
  //  alert(store.getState().token)

  }, []);

//   const _signIn = async () => {
//   try {
//     await GoogleSignin.hasPlayServices();
//     const userInfo = await GoogleSignin.signIn();
//   alert(JSON.stringify(userInfo))
//     //this.setState({ userInfo });
//   } catch (error) {
//     alert(JSON.stringify(error))
//     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//       // user cancelled the login flow
//     } else if (error.code === statusCodes.IN_PROGRESS) {
//       // operation (e.g. sign in) is in progress already
//     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//       // play services not available or outdated
//     } else {
//       // some other error happened
//     }
//   }
// };

  const onPresss = () => {
navigation.navigate('Register')
/*
      <LinearGradient
        colors={['#4267B2', '#4267B2']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradientr}>

        <Image
          source = {require('../resources/facebook.png')}
          style={{width:20,height:20,margin:12,resizeMode:'contain'}}
        />

          <Text style={{color:'white',textAlign:'center',alignSelf:'center',fontFamily:"DMSans-Bold",fontSize:17,marginTop:-35,marginBottom:12}}>Login With Facebook</Text>


      </LinearGradient>



      <LinearGradient
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradientr}>
        <Image
          source = {require('../resources/google.png')}
          style={{width:20,height:20,margin:12,resizeMode:'contain'}}
        />
        <Text style={{color:'black',textAlign:'center',alignSelf:'center',fontFamily:"DMSans-Bold",fontSize:17,marginTop:-35,marginBottom:12}}>Login With Google</Text>
      </LinearGradient>

*/

  };
  const onForgot = () => {
navigation.navigate('Forgot')


  };
  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container}>
    <View>
      <ScrollView style = {{width:window.width,height:window.height}}>
      {state.loading && <Loader />}
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Welcome Back</Text>

      </LinearGradient>

      <Text style={styles.text_1}>Please login to continue using Cleo Live</Text>


      <View style = {{marginLeft:28,width:window.width -56,marginTop:-20}}>
    <TextField
        label='Email/Mobile'
        baseColor = '#acb1c0'
        tintColor = '#acb1c0'
        value = {username}
        onChangeText={(text) => setUsername(text)}


    />

    <TextField
                           label='Password'
                           baseColor = '#acb1c0'
                           tintColor = '#acb1c0'
                           secureTextEntry = {true}
                           value = {password}
                           onChangeText={(text) => setPassword(text)}


                       />


</View>



      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
          <Text style={styles.text_3}>Next</Text>
        </TouchableOpacity>
      </LinearGradient>

      <GoogleSignIn/>

      <Text onPress={onForgot} style={{color:'black',fontFamily:'DMSans-Bold',marginTop:20,textAlign:'center'}}>Forgot your Password?</Text>

</ScrollView>
<Text onPress={onPresss}  style={{color:'black',fontFamily:'DMSans-Bold',position:'absolute',bottom:20,alignSelf:'center'}}>Don't have an account yet ?
<Text onPress={onPresss}  style={{color:'#73005C',fontFamily:'DMSans-Bold',marginTop:20}}>  Sign Up</Text>
</Text>
</View>

    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 18,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 20,
    marginBottom: 20,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    height: 50,
    width: '90%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  linearGradientr: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 23,

  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
    textAlign:'center'
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
  text_31: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: 'black',
    textAlign:'center',
    alignSelf:'center'
  },
});
export default Introduction;
