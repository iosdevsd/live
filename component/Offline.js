import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  FlatList,
  Text,
  TouchableOpacity,
  Dimensions,
  Linking,
  View,
} from 'react-native';
import RazorpayCheckout from 'react-native-razorpay';
import LinearGradient from 'react-native-linear-gradient';
import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert';
import {textStyle, viewStyle} from '../style/Style';
import moment from 'moment';
import * as RNLocalize from "react-native-localize";
const window = Dimensions.get('window');
import {LoginOtpApi,SignInApi,SocialLogin,RegisterOtps,Verify,Resend,GetProfileApi,RechargeWallet,WalletHist,Clear,Affliate} from '../backend/Api';
const Offline = ({navigation}) => {
  const [username, setUsername] = useState([]);
    const [usernames, setUsernames] = useState({});
      const [show, setShow] = useState(false);
  useEffect(() => {
//RNLocalize.getTimeZone() == "Asia/Kolkata" &&v


    const unsubscribew =   navigation.addListener('focus', () => {
      Affliate({limit:"100",offset:"0",country:RNLocalize.getCountry()})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 setUsername(data.data)
                // alert(JSON.stringify(data))
                //setUsername(data.data)





               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)

  }, []);


  const change = (amounte) =>{




  var amount = parseInt(amounte) * 100
  var options = {
 description: 'Coin purchasing',

 currency: 'INR',
 key: 'rzp_test_yLzqxWLttKfDSu', // Your api key
 amount: amount.toString(),
 name:  username.name,
 prefill: {
   email: username.email,
   contact:username.phone ,
   name: username.name
 },
 theme: {color: '#F00B51'}
}
RazorpayCheckout.open(options).then((data) => {
 // handle success
 success(data.razorpay_payment_id,amounte)
// alert(`Success: ${data.razorpay_payment_id}`);
}).catch((error) => {
 // handle failure
 alert(`Error: ${error.code} | ${error.description}`);
});
}


const success = (id,amount)=>{
var c = {txn_id:id,recharge_amount:amount}

console.log(JSON.stringify(c))
  RechargeWallet({txn_id:id,recharge_amount:amount})
         .then((data) => {
            //   toggleLoading(false);
 console.log(JSON.stringify(data))
           if (data.status) {
             console.log(JSON.stringify(data))

             GetProfileApi({user_id:"1"})
                    .then((data) => {
                       //   toggleLoading(false);

                      if (data.status) {
                        console.log(JSON.stringify(data))
                        setUsername(data.data)
                       // alert(JSON.stringify(data))
                       //setUsername(data.data)





                      } else {
             alert(JSON.stringify(data.message))
                      }
                    })
                    .catch((error) => {
                      //toggleLoading(false);
                      console.log('error', error);
                    });




           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}


const handleClose = () =>{
  Clear({user_id:"1"})
         .then((data) => {
            //   toggleLoading(false);
//alert(JSON.stringify(data))
           if (data.status) {
            navigation.goBack()
            //setUsername(data.data)





           } else {
  alert(JSON.stringify(data.message))
           }
         })
         .catch((error) => {
           //toggleLoading(false);
           console.log('error', error);
         });
}
const  clear = () =>{
  setShow(true)



}

const  call = (item) =>{

  var c = `91${item.phone}`
  let url = 'whatsapp://send?text=hello&phone=' + c;

  Linking.openURL(url)
  //BET16218365781675311


}
const renderItem = ({item, index}) => (
    <TouchableOpacity  onPress ={() =>  call(item)}>


    <View style = {{flexDirection:'row'}}>

    <Image style = {{width:60,height:60,borderRadius:30,marginTop:15}} source = {{uri:item.imageUrl}}/>
  <View style = {{borderBottomWidth:0.5,marginTop:7}}  key={item.id.toString()}>



    <Text style = {{color:'#000000',fontFamily:"DMSans-Bold",fontSize:15,marginLeft:20}}>
    {item.country}
    </Text>
    <Text style = {{color:'#000000',fontFamily:"DMSans-Bold",fontSize:15,marginLeft:20}}>
    {item.name}
    </Text>














    <Text style = {{color:'#000000',fontFamily:"DMSans-Regular",fontSize:15,marginLeft:20}}>
    Email: {item.email}
    </Text>
    <Text style = {{color:'#000000',fontFamily:"DMSans-Regular",fontSize:15,marginLeft:20}}>
    Whatsapp: {item.phone}
    </Text>
    <Text style = {{color:'#000000',fontFamily:"DMSans-Bold",fontSize:15,marginLeft:20}}>
    {item.discount_text}
    </Text>

  </View>

  </View>

  </TouchableOpacity>
);

const handleClose1 = () =>{
  navigation.goBack()
}
  const itemData = [
    {
      id: 1,
      coin: 100,
      price: 100,
    },
    {
      id: 2,
      coin: 200,
      price: 200,
    },
    {
      id: 3,
      coin: 300,
      price: 300,
    },
    {
      id: 4,
      coin: 500,
      price: 500,
    },
    {
      id: 5,
      coin: 1000,
      price: 1000,
    },
  ];
  return (
    <SafeAreaView style={styles.container}>
      {/* <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      /> */}
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={{  flex: 0,

          alignItems: 'center',
          height: 90,
          padding: 11,
        flexDirection:'row',
        justifyContent:'space-between',

      }}>


        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
          <Text style={{
            fontFamily: 'DMSans-Bold',
            fontWeight: '600',
            fontSize: 18,
            marginLeft:40,
            marginTop:40,
            color: '#FFFFFF',
          }}>Offline Recharge</Text>
          <Text  style={{
            fontFamily: 'DMSans-Bold',
            fontWeight: '600',
            fontSize: 18,
            marginLeft:40,
            marginTop:40,
            color: '#FFFFFF',
          }}></Text>
      </LinearGradient>
      <Text style = {{color:'#000000',fontFamily:"DMSans-Regular",fontSize:15,textAlign:'center',margin:10}}>
      Kindly contact any of the agents below to get coins added instantly.
      </Text>

      <FlatList



        contentContainerStyle={{
          paddingBottom: 20,
          margin: 8,
        }}
        data={username}


        renderItem={renderItem}


      />

      <SCLAlert
       theme="warning"
       show={show}
       title="Mepl"
       subtitle= "Are you sure to clear all transactions. Data cleared can not come back. "
       subtitleStyle = {{fontSize:14,fontFamily:'DMSans-Bold'}}
     >
     <SCLAlertButton theme="info" onPress={handleClose1}>Go Back</SCLAlertButton>
       <SCLAlertButton theme="info" onPress={handleClose}>Clear all</SCLAlertButton>
     </SCLAlert>


    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  view_1: {
    marginTop: 23,
    marginHorizontal: 23,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  view_2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    height: 44,
    width: 44,
  },
  coinText: {
    marginLeft: 7,
    fontFamily: 'DMSans-Bold',
    fontWeight: '800',
    fontSize: 40,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  coinText_2: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  view_3: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 7,
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 13,
    fontStyle: 'normal',
    color: '#DD0952',
    textDecorationLine: 'underline',
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#262628',
    marginTop: 34,
    alignSelf: 'center',
  },
  view_4: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#EFEFEF',
    paddingVertical: 10,
  },
  view_5: {
    flexDirection: 'row',
    marginLeft: 20,
    alignItems: 'center',
  },
  image_1: {
    height: 25,
    width: 25,
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 20,
    fontStyle: 'normal',
    color: '#262628',
    marginLeft: 7,
  },
  text_5: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#FFFFFF',
  },
  view_6: {
    borderRadius: 24,
    width: 100,
    paddingVertical: 7,
    marginLeft: 'auto',
    marginRight: 20,
    alignItems: 'center',
  },
});
export default Offline;
