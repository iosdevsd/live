import React, {useState} from 'react';
import {
  FlatList,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import RadialGradient from 'react-native-radial-gradient';

const PKMode = () => {
  const [state, setstate] = useState(0);
  const flatListData_2 = [
    {id: 1, house: 'House 1'},
    {id: 2, house: 'House 2'},
    {id: 3, house: 'House 3'},
  ];
  const renderItem_2 = ({item}) => (
    <TouchableOpacity key={item.id} style={styles.renderView}>
      <ImageBackground
        source={require('../assets/setting/pk.png')}
        style={{flex: 1}}
      />
    </TouchableOpacity>
  );
  return (
    <>
      <View style={styles.selectView}>
        <TouchableOpacity onPress={() => setstate(0)}>
          <RadialGradient
            colors={
              state === 0 ? ['#3499FF', '#3A3985'] : ['#6ee2f580', '#6454f080']
            }
            stops={[0, 1]}
            center={[57.5, 17.5]}
            radius={180}
            style={styles.radialGradient}>
            <Text style={styles.selectText}>Ongoing</Text>
          </RadialGradient>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setstate(1)}>
          <RadialGradient
            colors={
              state === 1 ? ['#3499FF', '#3A3985'] : ['#6ee2f580', '#6454f080']
            }
            stops={[0, 1]}
            center={[57.5, 17.5]}
            radius={180}
            style={styles.radialGradient}>
            <Text style={styles.selectText}>Upcoming</Text>
          </RadialGradient>
        </TouchableOpacity>
      </View>
      <FlatList
        data={flatListData_2}
        renderItem={renderItem_2}
        keyExtractor={(item) => item.id.toString()}
      />
    </>
  );
};

const styles = StyleSheet.create({
  selectView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginHorizontal: 10,
    marginVertical: 20,
  },
  radialGradient: {
    width: 120,
    height: 35,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  selectText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 13,
    color: '#FFFFFF',
  },
  renderView: {
    flex: 1,
    flexDirection: 'column-reverse',
    overflow: 'hidden',
    height: 206,
    margin: 10,
    marginBottom: 0,
    backgroundColor: '#000000cc',
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderColor: '#E90A51',
    borderWidth: 2,
  },
  house: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 16,
    color: '#FFFFFF',
  },
});
export default PKMode;
