import React ,{ useEffect ,useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {shadowStyle} from '../style/Style';

const ViewProfile = ({navigation,route}) => {
    const [detail, setDetail] = useState('');
    const [detail1, setDetail1] = useState('');
const  capitalize = (string) => {
  //salert(string)ssss
  if (string == "" || typeof string == "undefined"){
    return ""
  }
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
useEffect(() => {
  var k = route.params.item.language.split(",")
  var es = ""
   for (var i = 0 ; i<k.length;i++){

     es = es + " " + capitalize(k[i]) + ","
   }
   es  = es.substring(1)
   setDetail(es == "" ? "" :es.substring(0, es.length - 1))

}, []);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <ImageBackground
          source={{uri:route.params.item.imageUrl}}
          style={styles.bgimage}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
          <View style={styles.userView}>

            <Text style={styles.userName}>{route.params.item.name}</Text>

            {route.params.item.is_premium == "1" && (
              <Image
                  source={require('../resources/vip.png')}
                style={{width:20,height:20,resizeMode:'contain',position:'absolute',bottom:2,right:10}}
              />
            )}


          </View>

        </ImageBackground>

        {route.params.item.title != "" && (
          <View style={[styles.titleView, shadowStyle.s_1]}>
            <Text style={styles.title}>TITLE</Text>
            <Text style={styles.subTitle}>
            {route.params.item.title}
            </Text>
          </View>
        )}


  {route.params.item.description != "" && (
        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>DESCRIPTION</Text>
          <Text style={styles.subTitle}>
          {route.params.item.description}
          </Text>
        </View>
      )}

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>TAG LINE</Text>
          <Text style={styles.subTitle}>
          {route.params.item.tagline}
          </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>INTRO</Text>
          <Text style={styles.subTitle}>
          {route.params.item.bio}
          </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>TIMINGS</Text>
          <Text style={styles.subTitle}>{route.params.item.service_start_time} to {route.params.item.service_end_time} (IST) </Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>LANGUAGE KNOWN</Text>
          <Text style={styles.subTitle}>{detail}</Text>
        </View>

        <View style={[styles.titleView, shadowStyle.s_1]}>
          <Text style={styles.title}>LOCATION</Text>
          <Text style={styles.subTitle}>{route.params.item.city}</Text>
        </View>


      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgimage: {
    height: 280,
    width: '100%',
  },
  backImage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginTop: 40,
    marginHorizontal: 20,
  },
  view_1: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#0000004d',
    marginRight: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
  text_1: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  view_2: {
    flexDirection: 'row',
    margin: 20,
    marginTop: 5,
  },
  followView: {
    borderRadius: 12.5,
    paddingHorizontal: 5,
    paddingVertical: 2,
    marginLeft: 'auto',
  },
  followText: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 13,
    color: '#FFF',
  },
  coinImage: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  coinView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 1,
    borderRadius: 10,
    marginLeft: 10,
  },
  coinText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 10,
    color: '#FFFFFF',
    marginLeft: 5,
  },
  levelText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  ageText: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 11,
    color: '#FFFFFF',
  },
  ageView: {
    backgroundColor: '#8B572A',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginLeft: 10,
  },
  levelView: {
    backgroundColor: '#9013FE',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginLeft: 10,
  },
  userName: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 20,
    color: '#FFFFFF',
    marginLeft: 6,
  },
  userDot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#43FA00',
  },
  userView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 'auto',
  },
  view_3: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    marginBottom: 15,
    backgroundColor: 'white',
  },
  view_31: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  view_32: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 24,
    padding: 10,
  },
  text31: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 18,
    color: '#EA0B52',
    marginLeft: 6,
  },
  text32: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 14,
    color: '#FFFFFF',
    marginLeft: 6,
  },
  image31: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  image32: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  title: {
    fontFamily: 'Nunito',
    fontWeight: '700',
    fontSize: 14,
    color: '#D50853',
  },
  subTitle: {
    fontFamily: 'Nunito',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 23,
    color: '#262628',
  },
  titleView: {
    borderRadius: 8,
    marginHorizontal: 20,
    padding: 15,
    marginBottom: 15,
    backgroundColor: 'white',
    borderRadius: 15,
  },
  videoImage: {
    height: 100,
    width: 88,
    borderRadius: 10,
    overflow: 'hidden',
  },
  overlayview: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e60a5273',
  },
  videoImage2: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },
  view_5: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 20,
  },
});
export default ViewProfile;
