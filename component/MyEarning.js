import React from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';

const MyEarning = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Earning Report</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.calView}>
          <TouchableOpacity
            style={styles.calTouch}
            // onPress={() => navigation.goBack()}
          >
            <Image
              source={require('../resources/cal.png')}
              style={styles.calImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <ImageBackground source={require('../resources/er1.png')} style={styles.bg1}>
        <Text style={styles.text_1}>Total Coins</Text>
        <View style={styles.view_1}>
          <Image
            style={styles.image_1}
            source={require('../resources/coin.png')}></Image>
          <Text style={styles.coin_text_1}>20,000</Text>
        </View>
      </ImageBackground>
      <ImageBackground source={require('../resources/er2.png')} style={styles.bg1}>
        <Text style={styles.text_1}>Current Month Imcome</Text>
        <View style={styles.view_1}>
          <Image
            style={styles.image_1}
            source={require('../resources/coin.png')}></Image>
          <Text style={styles.coin_text_1}>20,000</Text>
        </View>
      </ImageBackground>
      <View style={styles.view_2}>
        <ImageBackground
          source={require('../resources/er3.png')}
          style={styles.bg2}>
          <Text style={styles.text_2}>10</Text>
          <Text style={styles.text_3}>Total Hours</Text>
        </ImageBackground>
        <ImageBackground
          source={require('../resources/er4.png')}
          style={styles.bg2}>
          <Text style={styles.text_2}>10</Text>
          <Text style={styles.text_3}>Valid days</Text>
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  calImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  calView: {
    position: 'absolute',
    right: 18,
    bottom: 8,
  },
  calTouch: {
    padding: 5,
  },
  bg1: {
    height: 120,
    width: 350,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 8,
    overflow: 'hidden',
  },
  bg2: {
    height: 120,
    width: 160,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 8,
    overflow: 'hidden',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
  view_1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image_1: {
    height: 32,
    width: 32,
    resizeMode: 'contain',
  },
  coin_text_1: {
    marginLeft: 5,
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 35,
    color: '#FFFFFF',
  },
  view_2: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 35,
    color: '#FFFFFF',
  },
  text_3: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
});

export default MyEarning;
