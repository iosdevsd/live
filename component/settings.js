import React from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/style';
import * as actions from '../redux/actions';
import {settingList} from '../backend/Config';

const Setting = ({navigation}) => {
  const onPressHandler = (key) => {
    switch (key) {
      case 'logout':
        actions.logout();
        navigation.reset({
          index: 0,
          routes: [{name: 'SelectLanguage'}],
        });
        break;
      default:
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Settings</Text>
        <View style={styles.backView}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
      {settingList.map((item) => (
        <TouchableOpacity
          style={styles.view}
          key={item.key}
          onPress={() => onPressHandler(item.key)}>
          <Image style={styles.image} source={item.source} />
          <Text style={styles.text}>{item.name}</Text>
        </TouchableOpacity>
      ))}
      <Text style={styles.appVersion}>App version 1.0</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  appVersion: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
    alignSelf: 'center',
    opacity: 0.7,
    marginTop: 47,
  },
  view: {
    flexDirection: 'row',
    borderBottomColor: '#F5F5F5',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  image: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 12,
  },
  text: {
    fontFamily: 'Nunito',
    fontWeight: '600',
    fontSize: 14,
    color: '#262626',
  },
});

export default Setting;
