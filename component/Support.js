import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  View,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import LinearGradient from 'react-native-linear-gradient';
//Create
import {LoginOtpApi,SignInApi,SocialLogin,Create} from '../backend/Api';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import {textStyle, viewStyle} from '../style/Style';
const window = Dimensions.get('window');
const Support = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('')
    const [message, setMessage] = useState('');

  const onPress = () => {

    if (username == ""){
      Toast.showWithGravity('Please enter username', Toast.LONG, Toast.CENTER);
      return
    }
    if (email == ""){
        Toast.showWithGravity('Please enter Email', Toast.LONG, Toast.CENTER);
        return
    }
    if (message == ""){
        Toast.showWithGravity('Please enter Message', Toast.LONG, Toast.CENTER);
        return
    }
  //  toggleLoading(true);

    const json = {
  message: message,
  email: email,
  name: username,



};
console.log(JSON.stringify(json))

Create(json)
       .then((data) => {
          //  toggleLoading(false);

         if (data.status) {
          //alert(JSON.stringify(data))
//our message has been sent to Support, our executives will review and get back to you within 24 working hours.
Toast.showWithGravity('Your message has been sent to Support, our executives will review and get back to you within 24 working hours.', Toast.LONG, Toast.CENTER);

setUsername('')
setEmail('')
setMessage('')
navigation.goBack()
         } else {
    Toast.showWithGravity(data.message, Toast.LONG, Toast.CENTER);
         }
       })
       .catch((error) => {
         //toggleLoading(false);
         console.log('error', error);
       });

  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView keyboardShouldPersistTaps="always">
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={viewStyle.simpleHeaderView}>
          <Text style={textStyle.simpleHeaderTitle}>Support</Text>
          <View style={styles.backView}>
            <TouchableOpacity
              style={styles.backTouch}
              onPress={() => navigation.goBack()}>
              <Image
                source={require('../resources/back.png')}
                style={styles.backImage}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>
        <Image
          source={require('../resources/support-bg.png')}
          style={styles.bgImage}
        />
        <Text style={styles.title}>Need Some Help?</Text>
        <View style={styles.view_1}>
          <Image
            style={styles.image_1}
            source={require('../resources/envelopes.png')}
          />
          <View style={styles.view_11}>
            <Text style={styles.text_2}>Email</Text>
            <Text style={styles.text_3}>cleolivegames@gmail.com</Text>
          </View>
        </View>

        <View  style = {{elevation:5,backgroundColor:'white',borderRadius:22,marginHorizontal: 15,marginTop:20,marginBottom:20}}>
        <View style={styles.view_2}>
          <Image
            style={{width:30,height:30 ,resizeMode:'contain',marginLeft:8,marginRight:15}}
            source={require('../resources/help.png')}
          />
          <Text style={styles.text_4}>Get in Touch</Text>
        </View>
        <Text style={styles.text_1}>
          Please give us in between 12 to 24 working hours to address your
          issues
        </Text>
        <View style = {{marginLeft:28,width:window.width -56,marginTop:-16}}>
      <TextField
          label='Name'
          baseColor = '#acb1c0'
          tintColor = '#acb1c0'
          value = {username}
          onChangeText={(text) => setUsername(text)}


      />

      <TextField
          label='Email'
          baseColor = '#acb1c0'
          tintColor = '#acb1c0'
          value = {email}
          onChangeText={(text) => setEmail(text)}


      />
      <TextField
          label='Message'
          baseColor = '#acb1c0'
          multiline = {true}
          tintColor = '#acb1c0'
          value = {message}
          onChangeText={(text) => setMessage(text)}


      />
</View>






        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={styles.buttonView}>
          <TouchableOpacity style={styles.buttonTouch} onPress={onPress}>
            <Text style={styles.buttonText}>SEND</Text>
          </TouchableOpacity>
        </LinearGradient>

        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  bgImage: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 20,
  },
  title: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 17,
    color: '#1E2432',
    alignSelf: 'center',
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 12,
    color: '#7D7D7E',
    marginHorizontal: 29,
  },
  image_1: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginHorizontal: 20,
  },
  view_1: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginTop: 15,
    elevation:5,
    borderRadius:12,
    height:70,
    backgroundColor:'white'
  },
  view_11: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 13,
    color: '#00000080',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
  },
  image_2: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginHorizontal: 20,
    alignSelf:'center'
  },
  view_2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    marginTop: 5,
    marginBottom: 15,
  },
  text_4: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 20,
    color: '#1E2432',
  },
  inputText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 15,
    color: '#ACB1C0',
    marginHorizontal: 33,
    marginTop: 5,
  },
  textInput: {
    width: '82%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
    height:40,
    marginBottom:3
  },
  buttonView: {
    marginHorizontal: 109,
    borderRadius: 25,
    marginVertical: 5,
  },
  buttonTouch: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  buttonText: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});

export default Support;
