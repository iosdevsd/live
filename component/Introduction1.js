import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ScrollView,
  Platform,
  TextInput,
  Dimensions,
  StatusBar,
} from 'react-native';
//import { GoogleSignin,GoogleSigninButton } from '@react-native-community/google-signin';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import LottieView from 'lottie-react-native';
import DeviceInfo from 'react-native-device-info';
const GLOBAL = require('./Global');
import Toast from 'react-native-simple-toast';
import Loader from '../utils/Loader';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/Style';
import { AsyncStorageSetUser,AsyncStorageSettoken} from '../backend/Api';
import {LoginOtpApi,SignInApi,SocialLogin} from '../backend/Api';
const window = Dimensions.get('window');
import * as actions from '../redux/actions';
const Introduction1 = ({navigation}) => {
  const [state, setState] = useState({
  loading: false,



});
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const toggleLoading = (bol) => setState({...state, loading: bol});
  const [keyTypeNum, setKeyTypeNum] = useState(false);
  const onPress = () => {

    navigation.reset({
                    index: 0,
                    routes: [{name: 'Introduction'}],
                  });

  };

  useEffect(() => {
//     try {
//    GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
//   // google services are available
// } catch (err) {
//   console.error('play services are not available');
// }
//AIzaSyCcRlpCWs8QBklhcYKZ9wyIBIU_9yZAOW4
//117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com
//117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com
   //  GoogleSignin.configure({
   //   webClientId: '117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com',
   //   offlineAccess: true,
   //   hostedDomain: '',
   //   forceConsentPrompt: true,
   // })

  // GoogleSignin.configure();
//117868681027-vgs1osij1gus6l92jojrfc8dn1k6bdvf.apps.googleusercontent.com
//   GoogleSignin.configure({
//   scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
//   webClientId: "117868681027-n6dfgvn5vb3aap1kgmfsg5qc3asbtl2t.apps.googleusercontent.com",
//   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//   hostedDomain: '', // specifies a hosted domain restriction
//   loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//   forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
//   accountName: '', // [Android] specifies an account name on the device that should be used
//   iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
//   googleServicePlistPath: '', // [iOS] optional, if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
// });
  //  alert(store.getState().token)

  }, []);

//   const _signIn = async () => {
//   try {
//     await GoogleSignin.hasPlayServices();
//     const userInfo = await GoogleSignin.signIn();
//
//     //this.setState({ userInfo });
//   } catch (error) {
//     alert(JSON.stringify(error))
//     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//       // user cancelled the login flow
//     } else if (error.code === statusCodes.IN_PROGRESS) {
//       // operation (e.g. sign in) is in progress already
//     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//       // play services not available or outdated
//     } else {
//       // some other error happened
//     }
//   }
// };

  const onPresss = () => {
navigation.navigate('Register')
/*
      <LinearGradient
        colors={['#4267B2', '#4267B2']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradientr}>

        <Image
          source = {require('../resources/facebook.png')}
          style={{width:20,height:20,margin:12,resizeMode:'contain'}}
        />

          <Text style={{color:'white',textAlign:'center',alignSelf:'center',fontFamily:"DMSans-Bold",fontSize:17,marginTop:-35,marginBottom:12}}>Login With Facebook</Text>


      </LinearGradient>



      <LinearGradient
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradientr}>
        <Image
          source = {require('../resources/google.png')}
          style={{width:20,height:20,margin:12,resizeMode:'contain'}}
        />
        <Text style={{color:'black',textAlign:'center',alignSelf:'center',fontFamily:"DMSans-Bold",fontSize:17,marginTop:-35,marginBottom:12}}>Login With Google</Text>
      </LinearGradient>

*/

  };
  const onForgot = () => {
navigation.navigate('Forgot')


  };
  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container}>

    <View>

    <View style = {{width:300,alignSelf:'center',height:window.height - 300}}>

<LottieView   source={require('../resources/aq.json')}  />
</View>

      <Text style={styles.text_1}>Find your partner with us</Text>
<Text style={styles.text_31w}>Join us and socialize with million of people </Text>





      <LinearGradient
        colors={['#ff578a', '#fade6a']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.linearGradient}>
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
          <Text style={styles.text_3}>Continue</Text>
        </TouchableOpacity>
      </LinearGradient>





</View>

    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white'
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 22,
    color: '#000000',
    alignSelf:'center',
    marginHorizontal: 30,
    width:180,
    textAlign:'center',
    marginTop: 0,
    marginBottom: 7,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  text_31w: {
    fontFamily: 'DMSans-Regular',
    fontSize: 12,
    lineHeight: 20,
    alignSelf:'center',
    color: 'grey',
    width:150,
    textAlign:'center'

  },
  textInput: {
    height: 50,
    width: '90%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  linearGradientr: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 23,

  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
    textAlign:'center'
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
  text_31: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: 'black',
    textAlign:'center',
    alignSelf:'center'
  },
});
export default Introduction1;
