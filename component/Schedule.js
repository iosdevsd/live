import React ,{ useEffect ,useState} from 'react';
import {SafeAreaView,StatusBar, Text,ImageBackground,Image,View,StyleSheet,TouchableOpacity,NativeModules,FlatList} from 'react-native';
import style from '../style/Style.js';
import { AsyncStorageGetUser } from '../backend/Api';
import * as actions from '../redux/actions';
import store from '../redux/store';
const {Agora} = NativeModules;
import Toast from 'react-native-simple-toast';
const GLOBAL = require('./Global');
import io from 'socket.io-client';
const socket = io('http://51.79.250.86:3000', {
  transports: ['websocket']
})
import {LoginOtpApi,SignInApi,SocialLogin,Recently,FetchSchedule,CheckSchedule} from '../backend/Api';
import requestCameraAndAudioPermission from './permission';
import {textStyle, viewStyle} from '../style/Style';
const {
 FPS30,
 AudioProfileDefault,
 AudioScenarioDefault,
 Host,
 Audience,
 Adaptative
} = Agora
import LinearGradient from 'react-native-linear-gradient';
const Schedule = ({navigation}) => {
    const [username, setUsername] = useState({});
  const screenHandler = async () => {
     actions.Login(JSON.parse(await AsyncStorageGetUser()))
    setTimeout(() => {
      const user = store.getState().user
  if (Object.keys(user).length === 0 && user.constructor === Object) {
    navigation.replace('Introduction')
  } else {
    navigation.replace('Home')
  }
}, 1000);

  }
const onPressBack = () => navigation.goBack();
    const onPress = () => {
        navigation.navigate('Home')
    }
    const onPressd = () => {
        navigation.navigate('Homes')
    }

    const onPressa = () => {
        navigation.navigate('Homes')
    }

    const pujaopen = (item,index) =>{

      CheckSchedule({id:item.id})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 GLOBAL.bookingid = data.data.bridge_id
                                 navigation.navigate("LiveVideoCall", {
                                                             uid: Math.floor(Math.random() * 100),
                                                             clientRole: Host,
                                                             channelName: data.data.bridge_id,
                                                             onCancel: (message) => {

                               }})


              //   navigation.navigate("LiveVideoCall", {
              //                               uid: Math.floor(Math.random() * 100),
              //                               clientRole: Host,
              //                               channelName: data.data.bridge_id,
              //                               onCancel: (message) => {
              //
              // }})



               } else {
                 Toast.showWithGravity('Please Wait Schedule time to Start', Toast.LONG, Toast.CENTER);
    //  alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });

    }

    const renderItemTv=({item, index}) => {

  var color = 'white'

        // alert(JSON.stringify(item))
    return(

  <TouchableOpacity onPress={()=>pujaopen(item,index)} >

  <View style={{flexDirection:'column',borderWidth:0,borderColor:'#949494',borderRadius:6,margin:2,backgroundColor:color,borderBottomWidth:0.4}}>



<View style = {{}}>
               <Text style={{marginLeft:3,fontSize:18,fontFamily:'DMSans-Regular',color:'#505050',marginTop:5}}>{item.title}</Text>
               <Text style={{marginLeft:3,fontSize:16,fontFamily:'DMSans-Regular',color:'#FF9445',marginTop:5}}>{item.description}</Text>
                  <Text style={{marginLeft:3,fontSize:16,fontFamily:'DMSans-Regular',color:'#505050',marginTop:5,marginBottom:12}}>Schedule Date :{item.start_time}</Text>
               </View>

     </View>

</TouchableOpacity>


)
}

  useEffect(() => {
    const socket = io('http://51.79.250.86:3000', {
     transports: ['websocket']
   })
       socket.on('connect', () => {

         console.log("socket connected")




       })
    const unsubscribew =   navigation.addListener('focus', () => {
      FetchSchedule({user_id:"1"})
             .then((data) => {
                //   toggleLoading(false);

               if (data.status) {
                 console.log(JSON.stringify(data))
                 //alert(JSON.stringify(data))
                 setUsername(data.data)

              //   navigation.navigate("LiveVideoCall", {
              //                               uid: Math.floor(Math.random() * 100),
              //                               clientRole: Host,
              //                               channelName: data.data.bridge_id,
              //                               onCancel: (message) => {
              //
              // }})



               } else {
      alert(JSON.stringify(data.message))
               }
             })
             .catch((error) => {
               //toggleLoading(false);
               console.log('error', error);
             });
    })
  //  alert(store.getState().token)
    requestCameraAndAudioPermission().then(_ => {
                console.log('requested!');
            })
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <LinearGradient
        colors={['#73005C', '#F00B51']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={viewStyle.simpleHeaderView}>
        <Text style={textStyle.simpleHeaderTitle}>Schedule List</Text>
        <View style={styles.backView}>
          <TouchableOpacity style={styles.backTouch} onPress={onPressBack}>
            <Image
              source = {require('../resources/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>


      <FlatList  style={{width:'90%',marginTop:20,alignSelf:'center'}}
  data={username}
  showsHorizontalScrollIndicator={false}
  renderItem={renderItemTv}
  />




    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  text_1: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
    marginTop: 55,
    marginBottom: 42,
  },
  text_2: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'DMSans-Bold',
    fontSize: 16,
    fontWeight: '400',
    color: '#1E1F20',
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#1e1f2033',
    borderBottomWidth: 1,
  },
  linearGradient: {
    marginHorizontal: 25,
    borderRadius: 25,
    marginTop: 46,
  },
  touchable: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    justifyContent: 'center',
  },
  text_3: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '700',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export default Schedule;
