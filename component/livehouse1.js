import React from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {textStyle, viewStyle} from '../style/style';

const LiveHouse1 = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <LinearGradient
          colors={['#73005C', '#F00B51']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          style={viewStyle.simpleHeaderView}>
          <Text style={textStyle.simpleHeaderTitle}>Live House 1</Text>
          <View style={styles.backView}>
            <TouchableOpacity
              style={styles.backTouch}
              onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/back.png')}
                style={styles.backImage}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>
        <ImageBackground
          source={require('../assets/hl1.png')}
          style={styles.imageBg}>
          <LinearGradient
            colors={['#73005C', '#F00B51']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.view_1}>
            <Text style={styles.text_1}>Recorded Highlights</Text>
          </LinearGradient>
        </ImageBackground>
        <ImageBackground
          source={require('../assets/hl1.png')}
          style={styles.imageBg}>
          <LinearGradient
            colors={['#73005C', '#F00B51']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.view_1}>
            <Text style={styles.text_1}>Pool Area</Text>
            <Text style={[styles.text_1, styles.text_2]}>Free</Text>
          </LinearGradient>
        </ImageBackground>
        <ImageBackground
          source={require('../assets/hl1.png')}
          style={styles.imageBg}>
          <LinearGradient
            colors={['#73005C', '#F00B51']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            style={styles.view_1}>
            <Text style={styles.text_1}>Pool Area</Text>
            <View style={styles.coinView}>
              <Image
                source={require('../assets/coin.png')}
                style={styles.coinImage}
              />
              <Text style={[styles.text_1, styles.text_2]}>1000/min</Text>
            </View>
          </LinearGradient>
        </ImageBackground>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
  backTouch: {
    padding: 5,
  },
  imageBg: {
    height: 280,
    width: 380,
    alignSelf: 'center',
    margin: 12,
    borderRadius: 25,
    overflow: 'hidden',
    flexDirection: 'column-reverse',
  },
  view_1: {
    borderRadius: 25,
    paddingHorizontal: 20,
    paddingVertical: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text_1: {
    fontFamily: 'Nunito',
    fontWeight: '800',
    fontSize: 22,
    color: '#FFFFFF',
  },
  text_2: {
    fontWeight: '700',
    fontSize: 18,
  },
  coinView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 5,
  },
});
export default LiveHouse1;
