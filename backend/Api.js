import AsyncStorage from '@react-native-community/async-storage';
import {PATH_URL} from './Config';
import ApiSauce from './ApiSauce';
import ApiSauceMu from './ApiSauce';
import store from '../redux/store';
import {Dimensions} from 'react-native';

import {latitudeDelta} from '../backend/Config';
const request = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then((response) => {
      //alert(JSON.stringify(json))
      if (response.ok) {
        resolve(response.data);
      } else {
        alert(JSON.stringify(response));
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};
const requestMultipart = (path, formdata) => {

  return new Promise((resolve, reject) => {
        ApiSauce.setHeader('Authorization', `${store.getState().token}`)
    ApiSauceMu.post(path, formdata).then((response) => {
      if (response.ok) {
        resolve(response.data);
      } else {
        alert(JSON.stringify(response));
        reject(response.err);
      }
    });
  });
};

const requesth = (path, json) => {
  //alert(store.getState().token)
  console.log("Token", store.getState().token)
var headers =  {
 Accept: 'application/json',
 'Content-Type': 'application/json'
  }

 //alert(store.getState().token)
  return new Promise((resolve, reject) => {
  //  alert(GLOBAL.language)

    //  console.log(json)
      ApiSauce.setHeader('Authorization', `${store.getState().token}`)


    ApiSauce.post(path, json).then((response) => {
    //  alert(JSON.stringify(path))
      console.log(JSON.stringify(response))
      if (response.ok) {
        resolve(response.data);
      } else {
      //  alert(JSON.stringify(response))
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};
export const VerifyRegisterLogin = (form) => requestMultipart(PATH_URL.verify_register_login, form);
export const SignUpApi = (form) => requestMultipart(PATH_URL.SignUp, form);
export const SignInApi = (json) => request(PATH_URL.login_with_password, json);
export const RegisterOtps = (json) => request(PATH_URL.user_signup_otp, json);
export const Resend = (json) => request(PATH_URL.resend, json);
export const RegisterOtp = (json) => request(PATH_URL.RegisterOtp, json);
export const LoginOtpApi = (json) => request(PATH_URL.LoginOtp, json);
export const GetProfileApi = (json) => requesth(PATH_URL.GetProfile, json);
export const AdddSupport = (json) => requesth(PATH_URL.add_support, json);
export const RegisterVerify = (json) => request(PATH_URL.Verify, json);
export const Favorite = (json) => requesth(PATH_URL.toggle_favourite, json);
export const Broadcasts = (json) => request(PATH_URL.broadcast, json);
export const PayableWallet = (json) => request(PATH_URL.fetch_payable_amount_wallet, json);
export const EditPasseord = (json) => requesth(PATH_URL.edit_password, json);
export const FetchFaq   = (json) => requesth(PATH_URL.fetch_faqs, json);
export const EditDetail = (json) => requestMultipart(PATH_URL.edit_profile_details, json);
export const Explore = (json) => requesth(PATH_URL.fetch_explore, json);
export const Fetch = (json) => requesth(PATH_URL.fetch_pujas, json);
export const Member = (json) => requesth(PATH_URL.add_member, json);
export const Clear = (json) => requesth(PATH_URL.clear_txns, json);
export const Venue = (json) => requesth(PATH_URL.fetch_puja_venues, json);
export const Add = (json) => requesth(PATH_URL.add_enquiry, json);
export const Pro = (json) => requesth(PATH_URL.get_userdetails, json);
export const Proe = (json) => requesth(PATH_URL.fetch_astrologer_speciality_horoscope, json);
export const RechargeWallet = (json) => requesth(PATH_URL.recharge_user_wallet, json);
export const FetchAstroService = (json) => requesth(PATH_URL.fetch_astro_services, json);
export const FetchFavourite = (json) => requesth(PATH_URL.fetch_favourite_astrologers, json);
export const WalletHist = (json) => requesth(PATH_URL.wallet_history, json);
export const CheckWallet = (json) => requesth(PATH_URL.user_wallet_balance, json);
export const FetchHoroscope = (json) => requesth(PATH_URL.fetch_horoscopes, json);
export const FetchTime = (json) => requesth(PATH_URL.fetch_puja_time_slots, json);
export const Referal = (json) => requesth(PATH_URL.referral_code_history, json);
export const FetchAstrologer = (json) => requesth(PATH_URL.fetch_astrologers_on_home, json);
export const FetchRopeway = (json) => requesth(PATH_URL.fetch_ropeways, json);
export const FetchTv= (json) => requesth(PATH_URL.fetch_tv_posts, json);
export const TogleFollow= (json) => requesth(PATH_URL.toggle_follow_astrologer, json);
export const CheckFollow= (json) => requesth(PATH_URL.checkIfastrologerfollowed, json);
export const FetchRopewayEnquiry = (json) => requesth(PATH_URL.add_ropeway_enquiry, json);
export const FetchPoojaCoupan = (json) => requesth(PATH_URL.fetch_puja_coupons, json);
export const ApplyCoupan = (json) => requesth(PATH_URL.apply_coupon, json);
export const BookingPooja = (json) => requesth(PATH_URL.booking_puja, json);
export const RemoveCoupan = (json) => requesth(PATH_URL.remove_coupon, json);
export const PoojaBookingHistory = (json) => requesth(PATH_URL.puja_booking_history, json);
export const FetchRopewayDetail = (json) => requesth(PATH_URL.fetch_ropeway_details, json);
export const FetchDetail = (json) => requesth(PATH_URL.fetch_puja_review_locations_details, json);
export const Payable = (json) => requesth(PATH_URL.fetch_payable_amount_puja, json);
export const Changepassword = (json) => requesth(PATH_URL.edit_passwords, json);
export const Bet = (json) => requesth(PATH_URL.submit_bet_numbers, json);
export const Bet1 = (json) => requesth(PATH_URL.submit_sicbo_bets, json);
export const FetchAstrologerList = (json) => requesth(PATH_URL.fetch_astrologers, json);
export const AddkMember = (json) => requesth(PATH_URL.add_kundalimember, json);
export const FetchkMember = (json) => requesth(PATH_URL.fetch_kundalimembers, json);
export const DeletekMember = (json) => requesth(PATH_URL.delete_kundalimember, json);
export const HorBooking = (json) => requesth(PATH_URL.horoscope_book, json);
export const AstroTiming = (json) => requesth(PATH_URL.fetch_astrologer_time_slots, json);
export const FetchHoroscopeCoupan = (json) => requesth(PATH_URL.fetch_horoscope_coupons, json);
export const HoroscopeBookings = (json) => requesth(PATH_URL.fetch_payable_amount_horoscope, json);
export const FetchAstrologerDetail = (json) => requesth(PATH_URL.fetch_astrologer_details, json);
export const AstroCoupans = (json) => requesth(PATH_URL.fetch_astrologer_coupons, json);
export const AstroBooks = (json) => requesth(PATH_URL.astrologer_book, json);
export const SetDefault = (json) => requesth(PATH_URL.set_default_kundali_member, json);
export const SeaechAstro = (json) => requesth(PATH_URL.search_astrologers, json);
export const SeaechPuja = (json) => requesth(PATH_URL.search_pujas, json);
export const SendOtp = (json) => requesth(PATH_URL.send_otp_phone, json);
export const ReschedulePooja = (json) => requesth(PATH_URL.reschedule_puja_booking, json);
export const RescheduleAstrologers = (json) => requesth(PATH_URL.reschedule_astrologer_booking, json);
export const CancelPooja = (json) => requesth(PATH_URL.cancel_puja_booking, json);
export const CancelAstrologer = (json) => requesth(PATH_URL.cancel_astrologer_booking, json);
export const FetchPayableAstrologer = (json) => requesth(PATH_URL.fetch_payable_amount_astrologer, json);
export const FetchHomeWallet = (json) => requesth(PATH_URL.fetch_wallet_and_notification_count, json);
export const FetchNotification = (json) => requesth(PATH_URL.fetch_notifications, json);
export const FetchPujaFilter = (json) => requesth(PATH_URL.fetch_puja_filters, json);
export const SearchPujaFilter = (json) => requesth(PATH_URL.search_puja_by_filters, json);
export const SocialLogin = (json) => requesth(PATH_URL.social_login, json);
export const FetchAstrologerFilter = (json) => requesth(PATH_URL.fetch_astrologer_filters, json);
export const SearchAstrologerFilter = (json) => requesth(PATH_URL.search_astrologer_by_filters, json);
export const EndLive = (json) => requesth(PATH_URL.end_live_session, json);
export const Create = (json) => requesth(PATH_URL.create_support, json);
export const Sort = (json) => requesth(PATH_URL.sortDataAstro, json);
export const PujaStart = (json) => requesth(PATH_URL.get_puja_booking_status, json);
export const Reals = (json) => requesth(PATH_URL.realtime_bookinghistory, json);
export const FetchSchedule = (json) => requesth(PATH_URL.fetch_scheduled_broadcasts, json);
export const CheckSchedule = (json) => requesth(PATH_URL.check_if_can_start_broadcast, json);
export const Verify = (json) => request(PATH_URL.user_signup_verify, json);
export const Addreview = (json) => requesth(PATH_URL.add_review, json);
export const Recently = (json) => requesth(PATH_URL.get_recently_added_member, json);
export const Forgotq = (json) => request(PATH_URL.forgot_otp, json);
export const Forgotv = (json) => request(PATH_URL.forgot_verify, json);
export const FetchHome = (json) => requesth(PATH_URL.fetch_home_live_broadcasts, json);
export const AppUser = (json) => requesth(PATH_URL.fetch_app_users, json);
export const Tranfer = (json) => requesth(PATH_URL.wallet_transfer, json);
export const Today = (json) => requesth(PATH_URL.today_coins_checkout, json);
export const Claim = (json) => requesth(PATH_URL.claim_today_checkout, json);
export const FetchVip = (json) => requesth(PATH_URL.fetch_vip_subscriptions, json);
export const CreateVip = (json) => requesth(PATH_URL.create_vip_subscription, json);
export const Daily = (json) => requesth(PATH_URL.daily_earn_claim_history, json);
export const TopHistory = (json) => requesth(PATH_URL.wallet_recharge_history, json);
export const BetFull = (json) => requesth(PATH_URL.bet_full_transactions, json);
export const Terq = (json) => requesth(PATH_URL.fetch_settings, json);
export const Affliate = (json) => requesth(PATH_URL.fetch_recharge_affiliates, json);
export const FetchCoin = (json) => requesth(PATH_URL.fetch_coins_plans, json);
export const Device = (json) => requesth(PATH_URL.device_id_can_access, json);
export const Place = (json) => requesth(PATH_URL.place_call_request, json);
export const Follow = (json) => requesth(PATH_URL.total_followers_host, json);
export const Addq = (json) => requesth(PATH_URL.fetch_adds, json);
export const GetChat = (json) => requesth(PATH_URL.get_chat_gid, json);
export const SendMessage = (json) => requesth(PATH_URL.send_message, json);
export const ChatHistory = (json) => requesth(PATH_URL.user_chat_history, json);
export const Unsseen = (json) => requesth(PATH_URL.user_seen_message, json);
export const Unread = (json) => requesth(PATH_URL.total_unread_messages, json);
export const GameImage = (json) => requesth(PATH_URL.fetch_games_images, json);
//bet_full_transactions
export const AsyncStorageSetUser = (user) =>
  AsyncStorage.setItem('user', JSON.stringify(user));
  export const AsyncStorageSettoken = (user) =>
    AsyncStorage.setItem('token', user);
export const AsyncStorageGetUser = () => AsyncStorage.getItem('user');
export const AsyncStorageGettoken = (user) =>
AsyncStorage.getItem('token')
export const AsyncStorageClear = () => AsyncStorage.clear();





export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;


export const formatAmount = (amount) =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = (str) =>
  str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = (price) => `\u20B9 ${price}`;
