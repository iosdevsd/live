import { create } from 'apisauce';
import { ApiSauceJson ,ApiSauceMultipart} from './Config';
const ApiSauce = create(ApiSauceJson);
export const ApiSauceMu = create(ApiSauceMultipart);
export const _SetAuthToken = (jwt) => {
  ApiSauce.setHeader('Authorization', jwt);
  ApiSauceMu.setHeader('Authorization', jwt);
};
export const _RemoveAuthToken = () => {
  ApiSauce.deleteHeader('Authorization');
  ApiSauceMu.deleteHeader('Authorization');
};
export default ApiSauce;
