import {PixelRatio, StyleSheet} from 'react-native';
import {shadow} from 'react-native-paper';
export const viewStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  tabButton: {
    bottom: 26,
  },
  simpleHeaderView: {
    flex: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: 90,
    padding: 11,
  },
  backView: {
    position: 'absolute',
    left: 18,
    bottom: 8,
  },
});
export const textStyle = StyleSheet.create({
  simpleHeaderTitle: {
    fontFamily: 'DMSans-Bold',
    fontWeight: '600',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
export const imageStyle = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  tabButton: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  backImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  backTouch: {
    padding: 5,
  },
  bannerImage: {
    flex: 1,
    width: '96.6%',
    height: PixelRatio.getPixelSizeForLayoutSize(30),
    resizeMode: 'cover',
    borderRadius:16,
    alignSelf: 'center',
    // marginVertical: 3,
  },
});

export const buttonStyle = StyleSheet.create({});

export const radioStyle = StyleSheet.create({
  buttonWrapStyle: {
    marginLeft: 40,
    marginTop: 11,
    marginBottom: 30,
  },
  labelStyle: {
    fontSize: 16,
    color: '#1E1F20',
    fontFamily: 'DMSans-Bold',
    fontWeight: '400',
    lineHeight: 20,
  },
  labelWrapStyle: {
    marginTop: 11,
    marginLeft: 5,
    marginBottom: 30,
  },
});

export const bgStyle = StyleSheet.create({
  splash: {
    flex: 1,
    resizeMode: 'contain',
  },
});

export const shadowStyle = StyleSheet.create({
  s_1: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1,
  },
  s_2: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});
