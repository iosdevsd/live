import React, {useEffect, useState} from 'react';
const GLOBAL = require('./component/Global');
import Navigator from './component/Navigator.js';
import { Provider } from 'react-redux';
import { EventRegister } from 'react-native-event-listeners'
import {
    SafeAreaView,
    Platform,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    AsyncStorage,
    PermissionsAndroid,
    NativeModules,
    BackHandler,
    Modal,



} from 'react-native';
import PushNotification  from 'react-native-push-notification';
import  store  from './redux/store';


const configure = () => {
 PushNotification.configure({

   onRegister: function(token) {
    // alert(token.token)
    GLOBAL.firebaseToken = token.token
    //alert(token.token)
      // actions.Firebase(token.token);
     //alert(store.getState().ftoken)
     console.log("tokens2" +token)
     //process token
   },

   onNotification: function(notification) {
       EventRegister.emit('notification', notification)
     //alert(JSON.stringify(notification))
     // Alert.alert(`${notification.data.title}`,`${notification.data.body}`,
     //                          [
     //                              {text:"OK",
     //                              },
     //                          ],
     //                          {cancelable:false}
     //                      )
     // process the notification
     // required on iOS only
    // notification.finish(PushNotificationIOS.FetchResult.NoData);
   },

   permissions: {
     alert: true,
     badge: true,
     sound: true
   },
  senderID: '222874891986',
   popInitialNotification: true,
   requestPermissions: true,

 });
};

// import Test from './src/screens/aatest';
export default App = () => {


  useEffect (() =>{
    Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
    configure()

//  alert(JSON.stringify(route.params.item.cart_data.qty))
  },[])

  return (
    // Redux: Global Store
    <Provider store={store}>

        <Navigator/>


    </Provider>
  );
};
